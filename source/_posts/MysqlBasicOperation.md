---
title: mysql basic operation
date: 2022-07-26 18:20:00
tags: 
- mysql 
- basic
categories:
- [Mysql, Basic]
---

# Mysql 基本操作

## 库操作

### 连接 mysql 数据库

```sql
mysql -uroot -p -P3306
```

> `-u` ：指代用户
> 
> `-p` ：指代密码
> 
> `-P` ：指代端口号

### 查看所有数据库

```sql
show databases;
```

### 创建数据库

> 以创建名为 `melodydb` 的数据库为例

```sql
create database melodydb charset=utf8;
```

### 使用数据库

> 以名为 `melodydb` 的数据库为例

```sql
use melodydb;
```

### 删除数据库

```sql
drop database melodydb;
```

---

## 表操作

> 以表名为 `melody` 为例

### 查看当前数据库中的所有表

```sql
show tables;
```

### 创建表

```sql
create table melody(
	id int unsigned primary key not null auto_increment,
	name varchar(255)
);
```

### 查看表结构

```sql
desc melody;
```

### 查看表的创建语句

```sql
show create table melody;
```

### 为表添加字段

```sql
alter table melody add content varchar(255) default"空";
```

### 为表修改字段

> 可重命名版本

```sql
alter table melody change content new_content varchar(255) default"nice";
```

> 不可重命名版本

```sql
alter table melody modify content varchar(200) default"neo";
```

### 删除字段

```sql
alter table melody drop content;
```

### 删除表

```sql
drop table melody;
```

---

## CRUD

例表：`melody`

| Field | Type | Null | Key | Default | Extra | Comment |
|---|---|---|---|---|---|---|
| id | int(10) | no | PRI | NULL | auto_increment |ID|
|name|varchar(255)|no||NULL||名称|
|content|varchar(255)|no||NULL||内容|

### 插入

#### 全列插入

> 规则： `insert [into] table_name values(...)`

```sql
insert into melody values(1, "testName", "testContent");
```

得到：

|id|name|content|
|---|---|---|
|1|testName|testContent|

#### 部分插入

> 规则： `insert into table_name(column1,column2,...) values(value1, value2,...)`

```sql
insert into melody(content) values("newContent");
```

得到：

|id|name|content|
|---|---|---|
|1||newContent|

`insert into melody(name, content) values("neoName", "neoContent");`

得到:

|id|name|content|
|---|---|---|
|1||newContent|
|2|neoName|neoContent|

#### 多行插入

> 规则： `insert into table_name values(key11, key12,...), (key21, key22,...)`

```sql
insert into melody values("name1", "content1"), ("name2", "content2");
```

得到：

|id|name|content|
|---|---|---|
|1|name1|content1|
|2|name2|conten2|

---

### 修改

#### 修改全部

> 规则： `update table_name set column1=key1, column2=key2, ...`

```sql
update melody set name="allName"
```

得到：

|id|name|content|
|---|---|---|
|1|allName|newContent|
|2|allName|neoContent|

#### 按条件修改

```sql
update melody set content="test" where id=1;
```

得到：

|id|name|content|
|---|---|---|
|1|allName|test|
|2|allName|neoContent|

#### 按条件修改多个值

```sql
update melody set name="testName", content="testContent" where id=2;
```

得到：

|id|name|content|
|---|---|---|
|1|allName|test|
|2|testName|testContent|

---

### 查询

#### 查询所有列

> 规则： `select * from table_name`

```sql
select * from melody;
```

得出：

|id|name|content|
|---|---|---|
|1|allName|test|
|2|testName|testContent|

#### 按条件查询

```sql
select * from melody where id=1;
```

得出：

|id|name|content|
|---|---|---|
|1|allName|test|

#### 查询指定列

> 规则： `select column1, column2, ... from table_name`

```sql
select name, content from melody;
```

得出：

| name | content |
|---|---|
|allName|test|
|testName|testContent|

#### 为列起别名

> 规则： `select fieldName1 [as aliasName1], fieldName2 [as aliasName2] from table_name`

```sql
select name as "名称", content as "内容" from melody;
```

得出：

|名称|内容|
|---|---|
|allName|test|
|testName|testContent|

#### 字段顺序

```sql
select content, name from melody;
```

得出：

|content|name|
|---|---|
|test|allName|
|testContent|testName|

### 删除

#### 物理删除

> 规则： `delete from table_name where conditions`

```sql
delete from melody where id=1;
```

得到：

|id|name|content|
|---|---|---|
|2|testName|testContent|

