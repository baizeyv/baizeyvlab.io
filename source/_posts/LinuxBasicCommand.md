---
title: linux basic command
date: 2022-08-28 20:01:00
tags: 
- linux
categories:
- Linux
---

# Linux

## Linux Command

### cd

#### Role

-   cd命令用来切换目录。

#### Example

-   `cd foldername` 切换至 `foldername` 目录下。
-   `cd /` 切换至根目录。
-   `cd -` 切换至最近访问的目录。
-   `cd /etc` 访问根目录的 `etc` 目录。

------



### ls

#### Role

-   查看当前目录下的子文件或子目录。

#### Example

-   `ls -l` 详细列出当前目录的信息，该命令可以简写为 `ll` 。

------



### clear

#### Role

-   清屏命令，清空控制台的所有信息。

------



### pwd

#### Role

-   获取当前目录的绝对路径。

------



### touch

#### Role

-   创建新的（空）文件。

#### Example

-   `touch filename` 创建名为`filename`的新文件。

------



### cat

#### Role

-   查看文件的内容。

#### Example

-   `cat filename` 查看名为 `filename` 的文件的内容。

------



### mkdir

#### Role

-   创建目录。

#### Example

-   `mkdir foldername` 创建一个名为 `foldername` 的文件夹。

------

### rm

#### Role

-   用于删除文件或文件夹。

#### Example

-   `rm filename` <u>**交互式删除**</u>[^1]当前目录下名为 `filename` 的文件。

-   `rm -i filename` 即rm的**默认模式**，参数 `-i` 代表的意思即为交互式删除。

-   `rm -f filename` <u>**强制删除**</u>[^2]文件名称为 `filename` 的文件。

-   `rm -r foldername` **递归删除**名称为 `foldername` 的文件夹。

-   `rm -rf ./*` **强制递归删除**当前目录下的所有内容。

[^1]: 即在输入完该条命令后，系统会询问用户是否确认删除，删除输入y，取消输入n。
[^2]: 即在输入完命令后直接删除掉文件，操作系统与用户不进行交互。

>   1.  `rm` 命令可以通过通配符 `*` 进行批量删除或者模糊删除。
>   2.  类似 `rm` 的命令可以跨目录进行操作。

------

### rmdir

#### Role

-   删除==空==文件夹，仅限于删除==空==文件夹

------

### cp

#### Role

-   用于复制文件或文件夹。

#### Example

-   `cp oldfile newfile` 复制 `oldfile` 文件一份，并命名为 `newfile` 。
-   `cp -r` 递归复制。**（用于复制文件夹）**

>   1.  `cp` 命令可跨目录进行复制文件或者文件夹。
>   2.  **跨目录复制**时，新文件可以只写路径不写文件名，此时默认文件名为原文件的名称。

------

### find

#### Role

-   用于查找文件。

#### Example

-   `find / -name demo` 在根目录下查找文件名为`demo`的文件，并返回文件所在路径。

>   `find` 后的 `/` 为查询的**目录范围**，`name` （参数）是指以文件名称查找，`demo` 是要查询的文件名称。
>
>   >   可通过通配符 `*` 进行模糊查找。

------

### mv

#### Role

-   移动文件或文件夹、剪切文件、修改文件名称、覆盖文件。

#### Example

-   `mv oldfile newfile` 将文件 `oldfile` 重命名为 `newfile` 。

>   1.  `mv` 命令可跨目录进行移动操作。
>   2.  **跨目录复制**时，新文件可以只写路径不写文件名，此时默认文件名为原文件的名称。

------

### tail

#### Role

-   用于查看<u>日志</u>[^3]文件。

#### Example

-   `-f` 参数进行循环读取日志，执行后命令行处于阻塞状态，以 `CTRL+C` 强制退出。
-   `tail -f catalina.out` 循环读取 `catalina.out` 日志文件。

[^3]: 机器运行期间的记录。

------

### zip | unzip

#### Role

-   `zip` 压缩文件或 `unzip` 解压文件。

#### Example

-   `zip -r filename.zip filesdir` 递归压缩 `filesdir` 目录为 `filename.zip`。
-   `-r` 参数代表**递归压缩**。
-   `unzip filename.zip` 解压缩名为 `filename.zip` 的压缩文件。

>   若在CentOS中，没有找到 `zip` 和 `unzip` 命令。
>
>   使用该命令进行安装：`sudo yum -y install zip`。
>
>   其中 `sudo` 是提权命令，即以超级管理员身份运行命令。

------

### tar

#### Role

-   解压或压缩后缀为 `tar.gz` 的压缩文件。

#### Example

-   `tar -czvf demo demo.tar.gz` 压缩 `demo` 文件夹为 `demo.tar.gz` 。
-   `tar -xzvf demo.tar.gz` 解压缩该压缩文件。

------

### rz | sz

#### Role

-   上传 `rz` 、下载 `sz` 文件。

>   安装 `lrzsz` ，执行以下命令：
>
>   `yum -y install lrzsz`

------

### ps

#### Role

-   查看应用或服务的进程信息。

#### Example

-   `ps -ef | grep mysql` 查看 `mysql` 服务的进程信息。

------



### kill

#### Role

-   结束进程，即杀进程。

#### Example

-   `ps -ef | grep mysql` 查询 `mysql` 服务的 `PID` 。
-   `kill -9 17369` 强制杀 `PID` 为17369的进程或服务。

>   `kill` 和 `kill -9` 的区别：
>
>   `kill` 命令执行后，程序或服务需要将进程执行结束（准备工作→释放资源）后杀死进程。
>
>   `kill` 命令有可能被忽略。（在准备工作时被阻塞，则 `kill` 命令被忽略）
>
>   `kill -9` 命令执行后，即强制杀进程，当下进程即结束。
>
>   `kill -9` 不会被忽略。

------

## Vim Command

### Base

>   `i` ：进入插入模式（insert mode）。
>
>   `esc` ：从其他模式退出至命令模式（command mode）。
>
>   `:w` ：保存文件（仅限于在command mode）。
>
>   `:q` ：退出（仅限于在command mode）。
>
>   `:wq` ：保存并退出（仅限于在command mode）。

## Additional

### Points

>   1.  进程：最小的资源分配单位。
>   2.  线程：最小的执行单位。
>
>   Linux中 `/` （斜杠）代表目录分割；Windows中 `\` （反斜杠）代表目录分割。

------



### CentOS换源

[**阿里云 服务器 CentOS 换源**](https://developer.aliyun.com/mirror/centos?spm=a2c6h.13651102.0.0.3e221b11K8js6M)



