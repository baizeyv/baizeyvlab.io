---
title: redis basic knowledge
date: 2022-08-02 15:50:00
tags: 
 - redis
categories:
 - Redis
 - Java
---

:::tip
该笔记跟随[bilibili狂神说视频](https://www.bilibili.com/video/BV1S54y1R7SB)讲解进行整理
:::

# NoSQL 概述

## 为什么用 NoSQL

### 发展史

大数据即一般的数据库无法进行分析处理了。

> 单机 `MySQL` 年代
> 
> ![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801001631.png)
> 
> 90年代，一个基本网站访问量一般不会太大，单个数据库足够用。
> 
> 当时，更多会使用静态网页 `HTML` ，服务器根本没有太大的压力
> 
> 这种情况下：整个网站的瓶颈为：
> 
>> 1. 数据量如果太大，一个机器放不下了
>> 2. 数据超过300w，就一定要建立索引 (B+ Tree) ，一个机器内存也放不下
>> 3. 访问量（读写混合），一个服务器承受不了
>
> 只要发生以上三种情况之一，一定需要升级


> `Memcached` 缓存 + `MySQL` + 读写分离 (垂直拆分)
> 
> 多台服务器中 `MySQL` 数据应尽可能保持一致
> 
> 网站80%情况都在读数据，总是访问数据库很不好，很麻烦。所以希望减轻数据库压力，可以使用缓存来保证效率
> 
> 发展过程：
>> 1. 优化数据结构和索引 -> （文件缓存IO） -> `Memcached` 
> 
> ![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801002406.png)

> 分库分表 + 水平拆分(`MySQL` 集群 (主[M]从[S]))
> 
>> 每个集群中存 1/n 的数据
>> 
>> *分库分表* 解决写的压力
> 
> ![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801003009.png)

**数据库本质：读、写**

> 如今年代，
> `MySQL` 等关系型数据库不够用了，因为数据量大，变化快
> 
> 有可能存图形、JSON
> 
> `MySQL` 有的时候使用他来存储一些比较大的文件，博客，图片！数据表很大，效率就低了！如果有一种数据库来专门处理这种数据，`MySQL` 压了就变得十分小
> 
>> 目前一个基本的互联网项目
>> 
>> ![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801004356.png)

### 正题


为什么要用 `NoSQL` ：

> 用户的个人信息，社交网络，地理位置，用户自己产生的数据，用户的日志等等爆发式增长
> 
> 此时，就需要使用 `NoSQL` 数据库， `NoSQL` 可以很好的处理以上的情况


## 什么是 NoSQL

`NoSQL` = `Not Only SQL` 不仅仅是sql

泛指非关系型数据库

传统关系型数据库很淡对付 `web2.0` 的数据，尤其是超大规模的高并发社区

> 关系型数据库：
>> 行、列、表格
>
> 很多数据类型的存储不需要固定的格式，如个人信息、社交网络等。不需要多余操作就可以横向扩展
> `Map<String,Object>` 使用键值对来控制

---

`NoSQL` 特点：

1. 方便扩展（数据之间没有关系，很好扩展）
2. 大数据量高性能（`Redis` 1s写8w次，读11w次，`NoSQL` 的缓存是一种细粒度缓存，性能会比较高）
3. 数据类型多样（不需要事先设计数据库，随取随用。）

---

传统 `RDBMS` 和 `NoSQL` ：

> 传统关系型数据库
> - 结构化组织
> - `SQL`
> - 数据和关系都存在单独的表中
> - 操作，数据定义语言
> - 严格的一致性
> - 基础的事务
> - ...... 


> `NoSQL`：
> - 不仅仅是数据
> - 没有固定的查询语言
> - 键值对存储，列存储，文档存储，图形数据库（社交关系）
> - 最终一致性
> - 高性能、高可用、高可扩展
> - ......

---

## NoSQL四大分类

**K-V键值对：**

- 新浪：Redis
- 美团：Redis + Tair
- 阿里、百度：Redis + memcache

**文档型数据库(bson格式)：**

- MongoDB
	- `MongoDB` 是一个基于分布式文件存储的数据库 cpp，主要用来处理大量的文档
	- `MongoDB` 是一个介于关系型数据库和非关系型数据库中间的产品。`MongoDB` 是非关系型数据库中功能最丰富，最像关系型数据库的
- CouchDB

**列存储数据库：**

- HBase
- 分布式文件系统

**图关系数据库：**

- 不是存图形，放的是关系，比如：朋友圈社交网络

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801102413.png)

# Redis入门

## 概述

`Redis` 是什么

> ==Remote Dictionary Server== 远程字典服务

`Redis` 能干什么

> 1. 内存存储、持久化（内存中是断电即失，所以持久化很重要[RDB、AOF]）
> 2. 效率高，可以用于高速缓存
> 3. 发布订阅系统
> 4. 地图信息分析
> 5. 计时器、计数器（浏览量）
> 6. ......


特性：

> 1. 多样的数据类型
> 2. 持久化
> 3. 集群
> 4. 事务
> 5. ......

## 安装

### Windows安装

1. 下载安装包 [Redis Github](https://github.com/dmajkic/redis)
2. 下载完毕得到压缩包
3. 解压到环境目录下
4. 开启 `Redis` ，运行服务即可
5. 使用`Redis`客户端连接服务端

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801104015.png)

> `ping` 测试连接
> 
> set get 设置基本值，获取基本值

### Linux安装

1. 下载安装包
2. 解压包 `tar -zxvf redis.tar.gz` 推荐 `/opt` 目录
3. `yum install gcc-c++`
4. `make`
5. `sudo make clean install`
6. 将redis配置文件复制到当前目录下
7. redis不是默认后台启动的
8. `daemonize` 改为 `yes`
9. 启动 `Redis` 服务
10. 查看redis进程是否开启 `ps -ef | grep redis`
11. 关闭Redis服务 `shutdown` | `exit`

通过指定配置文件启动服务: `redis-server /path/to/redis.conf`

---

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801110200.png)

---

## redis-benchmark 性能测试

`redis-benchmark` 是一个压力测试工具，官方自带的性能测试工具

### 简单测试

100个并发连接  100000请求

`redis-benchmark -h localhost -p 6379 -c 100 -n 100000`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801110604.png)

> ====== PING_INLINE ======
> 100000并发请求进行写入测试
> 100个并发客户端
> 每次写入3个字节
> 只有一台服务器来处理这些请求，单机性能
> 每秒处理59276.82个请求


## 基础知识

Redis可以作为数据库、缓存和消息中间件MQ

`Redis` 默认有16个数据库（在 `Redis.conf` 中有 `databases 16`），默认使用第0个

可以使用 `select` 进行切换数据库 `select 3`

`dbsize` 查看当前数据库大小

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801111107.png)

`keys *` 查看当前数据库所有的key

`flushdb` 清空当前数据库所有key

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801111350.png)

`FLUSHALL` 清空全部数据库的内容

---

***Redis是单线程的***

> `Redis` 是很快的，`Redis` 是基于内存操作，`CPU` 不是性能瓶颈，`Redis` 的瓶颈是根据机器的内存和网络带宽。
> 
> 既然可以使用单线程实现，就使用单线程

`Redis` 单线程为什么还真么快：

> 1. 误区1：高性能的服务器一定是多线程的 (X)
> 2. 误区2：多线程一定比单线程效率高 (X)
> 3. 核心：`Redis` 是将所有数据全部放在内存中的，所以使用单线程去操作效率就是最高的。
> 4. 多线程是CPU上下文切换，耗时的操作
> 5. 对于内存系统来说，没有上下文切换效率就是最高的
> 6. 多次读写都是在同一个CPU上，在内存情况下，效率就是最高的

## 五大数据类型

### Redis-Key

查看所有key `keys *`

判断某个键是否存在 `Exists name` -> 1存在 0不存在

移除当前数据库中的key `move name 1` 移除当前数据库中key为name的 (1代表当前数据库)

设置过期时间，单位为秒 `EXPIRE name 10` 10s后name过期

查看当前key的剩余时间 `ttl name` 

查看当前key的类型 `type name`

#### String（字符串类型）

设置值 `set name test`

获取值 `get name`

判断是否存在 `exists name`

字符串追加 `append name "hello"`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801113627.png)

获取字符串长度 `strlen name`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801113728.png)

---

i++自增操作 `incr name`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801114204.png)

i--自减操作 `decr name`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801114255.png)

自增自减步长设置操作 `incrby view 10` | `decrby view 5`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801114429.png)

---

字符串范围截取 `getrange name 0 3`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801114733.png)

指定位置开始的字符串范围替换 `setrange name beginIndex replaceContent`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801115014.png)

---

`setex` -> set with expire -> 设置过期时间

`setnx` -> set if not exist -> 不存在再设置，若存在则创建失败 -> 在分布式锁中常用

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801115409.png)

批量获取、设置值 `mget` | `mset`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801115717.png)

`msetnx` 是原子性操作，一起成功或一起失败

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801115803.png)

存储对象类型的：*这里的key是一个巧妙的设计* **user:{id}:{field}**

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801120048.png)

`getset` 先get再set (如果不存在则返回nil，如果存在则获取原来值，并设置新值)

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801120319.png)


#### List（列表类型）

列表，可以作为栈、队列、阻塞队列

---

将一个值或者多个值插入到列表头部 `lpush name value`

获取list区间中的值 `lrange name beginIndex endIndex`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801135150.png)

将一个值或者多个值插入到列表尾部 `rpush name value`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801135319.png)

头部移除第一个元素 `lpop list`

尾部移除第一个元素 `rpop list`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801135555.png)

---

通过下标获取值 `lindex list INDEX`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801135730.png)

---

获取list长度 `llen list`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801135833.png)

移除指定个数的指定值 `lrem list NUMBER VALUE`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801140105.png)

只保留一部分元素，从某个位置开始截取某个长度 `ltrim list beginIndex Length`

此时list已经被改变了

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801140408.png)

---

移除最后一个元素，并移动到新的列表中 `rpoplpush oldlist newlist`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801140706.png)

---

将列表中指定下标的值替换为其他值 `lset list INDEX VALUE`

如果不存在列表则报错，如果超出index范围，则报错

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801140939.png)

---

在指定值前或后插入新值 `linsert list AFTER|BEFORE originValue VALUE`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801141219.png)

---

***List总结***

> - 实际上是一个链表， before , after , left, right 都可以插入值
> - 如果key不存在，创建新的链表
> - 如果key存在，新增内容
> - 如果移除了所有值，空链表，也代表不存在
> - 在两边插入或者改动值，效率最高；中间元素，效率会低一点

---

#### Set（集合类型）

`set` 中的值是不能重复的

`set` 是无序不重复集合

---

set添加元素 `sadd myset VALUES`

查看指定set成员 `smembers myset`

判断set中是否包含某元素 `sismember myset VALUE`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801141912.png)

---

获取set中的元素个数 `scard myset`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801142100.png)

---

移除set中的指定元素 `srem myset VALUE`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801142156.png)

---

随机抽出指定个数的元素（默认为1） `srandmember myset [quantity]`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801142400.png)

---

随机删除key `spop myset`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801142605.png)

---

将一个指定值，移动到另一个集合中 `smove set1 set2 VALUE`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801142754.png)

---

数字集合类：
- 差集 `sdiff set1 set2`
- 交集 `sinter set1 set2`
- 并集 `sunion set1 set2`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801143049.png)

---

#### Hash（哈希类型）

`Map` 集合 k-v类型

本质与 `String` 类型没有太大区别，还是一个简单的k-v格式

`Hash` 更适合用来存对象

---

添加新元素 `hset myhash field value`

获取一个字段值 `hget myhash field`

批量添加元素 `hmset myhash field1 value1 field2 value2 ...`

批量获取值 `hmget myhash field1 field2 ...`

获取全部值 `hgetall myhash`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801143603.png)

---

删除指定field的值，k-v同时消失

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801143814.png)

---

获取hash长度 `hlen myhash`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801143906.png)

---

判断某个key是否存在 `hexists myhash field`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801144023.png)

---

只获取所有的field `hkeys myhash`

只获取所有的value `hvals myhash`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801144118.png)

---

步长自增 `hincrby myhash field step`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801144416.png)

---

为不存在的字段设置值 `hsetnx myhash field value`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801144534.png)



#### Zset（有序集合）

在 `set` 的基础上，增加了一个值（可以理解为权值）

---

添加新元素 `zadd myzset RANK_SCORE VALUE`

添加多个值 `zadd myzset 2 value 3 value3 ...`

获取范围内容 `zrange myzset beginIndex endIndex`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801145135.png)

---

排序

按照 min,max 排序 `zrangebyscore myzset MIN MAX [withscores]` ；加上 `withscores` 则输出带上 `score` 值

inf代表 `∞`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801145757.png)

升序 `zrange myzset beginIndex endIndex`

降序 `zrevrange myzset beginIndex endIndex`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801150349.png)

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801150509.png)

---

移除指定元素 `zrem myzset VALUE`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801150117.png)

---

获取有序集合zset中的元素数量 `zcard myzset`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801150217.png)

---

获取指定区间的成员数量 `zcount myzset beginScore endScore`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801150628.png)

---

## 三种特殊数据类型

### geospatial（地理位置）

朋友的定位、附近的人、打车距离计算等

需要城市精度维度查询

---

#### GEOADD

添加地理位置 `geoadd key 纬度 经度 名称`

> 规则：南极、北极无法直接添加；一般会下载城市数据，直接通过 `Java` 程序一次性导入
> 
> 有效经度 -180 ~ 180
> 
> 有效纬度 -85.05112878 ~ 85.05112878

```bash
geoadd china:city 116.40 39.90 beijing
geoadd china:city 121.47 31.23 shanghai
geoadd china:city 106.50 29.53 chongqing 114.05 22.52 shenzhen
geoadd china:city 120.16 30.24 hangzhou
geoadd china:city 108.96 34.26 xiAn
```

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801153136.png)

#### GEODIST

返回两个给定位置之间的距离

> - m 米
> - km 千米
> - mi 英里
> - ft 英尺

```bash
> geodist china:city beijing shanghai km
> 1067.3
```

#### GEOHASH

返回一个或多个位置元素的 `Geohash` 表示(11个字符)

将二维的经纬度转换为一维的字符串，若两个字符串越接近，则距离越近

```bash
> geohash china:city beijing chongqing
> 1)wx4fbxxfke0
> 2)wm5xzrybty0
```

#### GEOPOS

从key里返回所有给定位置元素的位置（经度和纬度）

获取指定的城市的经度和纬度

**一定是一个坐标值**

#### GEORADIUS

以给定的经纬度为中心，找出某以半径内的元素

```bash
> georadius china:city 110 30 1000 km
> 1)chongqing
> 2)xian
> 3)shenzhen
> 4)hangzhou
> georadius china:city 110 30 500 km
> 1)chongqing
> 2)xiAn
> georadius china:city 110 30 500 km withdist withcoord count 2 # 获取两个在110 30 半径500km范围内的城市的经纬度(withcoord) 距离(withdist)
```

#### GEORADIUSBYMEMBER

找出位于指定范围内的元素，中心点是由给定的位置元素决定

```bash
> georadiusbymember china:city beijing 1000 km
> 1)beijing
> 2)xiAn
> georadiusbymember china:city shanghai 400 km
> 1)hangzhou
> 2)shanghai
```

---

#### 小结

> GEO 底层的实现原理其实就是 Zset
> 
> 我们可以用zset命令来操作

```bash
> zrange china:city 0 -1 # 查看所有城市
> zrem china:city beijing # 移除zset中的元素

......
```

### Hyperloglog

> 什么是基数
> 
>> A {1,3,5,7,8,9} B {1,3,5,7,8} -> 9
>> 
>> 基数即不重复的元素

`Hyperloglog` 是用于基数统计的算法

例如：网页的 `UV` （一个人访问一个网站多次，但是还是算作一个人）

传统方式：set保存用户id，这个方式如果保存大量用户id，就比较麻烦。目的是为了计数，而不是记录id

`Hyperloglog` 优点：

> 占用的内存是固定的，2^64不同的元素的技术，只需要12kb内存
> 
> 如果从内存角度来比较，`Hyperloglog` 首选


如果允许容错，那么一定可以使用 `Hyperloglog`

如果不允许容错，就用 `Set` 或自定义数据类型

---

#### PFadd

添加元素

```bash
> PFadd mykey a b c d e f g h i j # 创建第一组元素
> PFCOUNT mykey # 统计数量
> PFadd mykey2 i j z x c v b n m # i j b 重复
> PFCOUNT mykey2
> PFMERGE mykey3 mykey mykey2 # 合并两组
> PFCOUNT mykey3 # 查看去重之后的数量
```

### Bitmap

只有两个状态的，都可以使用 `Bitmap` ，都是操作二进制位来进行记录

**位存储**

> 只有0和1来记录

例如：统计活跃用户或不活跃用户，统计登陆用户和未登陆用户

---

模拟一周打卡：(1打卡 0未打卡)

```bash
> setbit sign 0 0
> setbit sign 1 0
> setbit sign 2 1
> setbit sign 3 0
> setbit sign 4 0
> setbit sign 5 1
> setbit sign 6 1
```

查看某一天是否打卡：

```bash
> getbit sign 4
> 0
> getbit sign 6
> 1
```

统计打卡的天数：

```bash
> bitcount sign
> 3
```

---

## Redis 事务

> Redis单条命令 **保证** 原子性
> 
> `Redis` 事务时 **不保证** 原子性的
> 
> `Redis` 没有隔离级别的概念


### 本质

一组命令的集合，一次性执行一组命令

一个事务中的所有命令都会被序列化，在事务执行过程中，会按照顺序执行

所有的命令在事务中，并没有直接被执行；只有发起执行命令时候才会执行

- 一次性
- 顺序性
- 排他性

### 事务顺序

> Redis事务
> - 开启事务 `multi`
> - 命令入队 `...`
> - 执行事务 `exec`

```bash
> multi # 开启事务
> set k1 v1
> set k2 v2
> get k2
> set k3 v3
> exec # 事务执行
1) OK
2) OK
3) v2
4) OK
```

*放弃事务* `DISCARD`

```bash
> multi
> set k1 v1
> set k2 v2
> set k4 v4
> discard # 取消事务
> get k4
(nil)
```

> 放弃事务后，入队的命令都不会被执行

### 事务中存在错误

#### 编译型异常

即命令有错

> 事务中所有的命令都不会被执行

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801162505.png)

#### 运行时异常

即事务队列中存在语法性(1/0)错误

> 执行命令的时候，其他命令是可以正常执行的，错误命令抛出异常

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801162639.png)

## Redis 监控 (Watch)

### 悲观锁

认为，任何时候都会出问题；无论做什么都会加锁

> 非常影响性能

### 乐观锁

认为，任何时候都不会出现问题；不会上锁，在更新数据的时候去判断一下，在此期间是否修改过该数据

> mysql中 即 获取 `version` ，更新时候比较 `version`


### Redis 监视测试

> `Watch`
> 
> 此时多线程修改值，使用 `watch` 可以当作 `redis` 的乐观锁操作

#### success

```bash
> set money 100
> set out 0
> watch money
> multi # 事务正常结束，数据期间没有发生变动，此时正常执行成功
> decrby money 20
> incrby out 20
# 执行成功
```

#### failed

> Thread 1


```bash
> watch money
> multi
> decrby money 10
> incrby out 10 # 此时跳转 Thread 2
> exec # 修改失败，执行之前另外一个线程修改了我们的值，此时失败
(nil)

> unwatch # 解锁
> watch money # 获取最新值，重新加锁
> multi
> decrby money 10
> incrby out 10
> exec # 此时执行成功；比对监视的值是否发生变化，未发生变化，执行成功
```

> Thread 2

```bash
> get money
> set money 1000
```

---

# Jedis

我们要使用 `Java` 来操作 `Redis`

## 概述

> 什么是 `Jedis`
> 
>> `Jedis` 是 `Redis` 官方推荐的 `Java` 连接工具

## 测试

1. 导入 `Jedis` 依赖

```java
<dependency>  
  <groupId>redis.clients</groupId>  
  <artifactId>jedis</artifactId>  
  <version>3.2.0</version>  
</dependency>
```

2. 编码测试
	- 连接数据库
	- 操作命令
	- 断开连接

```java
package com.melody;  
  
import redis.clients.jedis.Jedis;  
  
public class TestPing {  
  
    public static void main(String[] args) {  
        // 1. new jedis 对象  
        Jedis jedis = new Jedis("127.0.0.1", 6379);  
        // Jedis 所有的命令就是所学所有指令  
        System.out.println(jedis.ping()); // PONG 则连接成功  
    }  
}
```

输出：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801173754.png)

## 常用API

### Redis-Key

```java
package com.melody;  
  
import redis.clients.jedis.Jedis;  
  
import java.util.Set;  
  
public class TestPing {  
  
    public static void main(String[] args) {  
        // 1. new jedis 对象  
        Jedis jedis = new Jedis("127.0.0.1", 6379);  
        // Jedis 所有的命令就是所学所有指令  
        System.out.println(jedis.ping()); // PONG 则连接成功  
  
        System.out.println("Clear Data:" + jedis.flushDB());  
        System.out.println("判断某个键是否存在:" + jedis.exists("username"));  
        System.out.println("新增<'username','melody'>键值对：" + jedis.set("username", "melody"));  
        System.out.println("新增<'password','fahaxiki'>键值对：" + jedis.set("password", "fahaxiki"));  
        System.out.println("所有key如下：");  
        Set<String> keys = jedis.keys("*");  
        System.out.println(keys);  
        System.out.println("删除键password:" + jedis.del("password"));  
        System.out.println("判断键password是否存在:" + jedis.exists("password"));  
        System.out.println("查看键username值类型:" + jedis.type("username"));  
        System.out.println("随机返回key空间的一个:" + jedis.randomKey());  
        System.out.println("重命名key:" + jedis.rename("username", "name"));  
        System.out.println("取出改后的name:" + jedis.get("name"));  
        System.out.println("按索引查询" + jedis.select(0));  
        System.out.println("删除当前选择数据库中的所有key:" + jedis.flushDB());  
        System.out.println("当前数据库中key的数目：" + jedis.dbSize());  
        System.out.println("删除所有数据库中所有key:" + jedis.flushAll());  
    }  
  
}
```

输出：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220801174538.png)

### String

```java
package com.melody.base;  
  
import redis.clients.jedis.Jedis;  
  
import java.util.concurrent.TimeUnit;  
  
public class TestString {  
    public static void main(String[] args) {  
        Jedis jedis = new Jedis("127.0.0.1", 6379);  
  
        jedis.flushDB();  
        System.out.println("===========增加数据===========");  
        System.out.println(jedis.set("key1","value1"));  
        System.out.println(jedis.set("key2","value2"));  
        System.out.println(jedis.set("key3", "value3"));  
        System.out.println("删除键key2:"+jedis.del("key2"));  
        System.out.println("获取键key2:"+jedis.get("key2"));  
        System.out.println("修改key1:"+jedis.set("key1", "value1Changed"));  
        System.out.println("获取key1的值："+jedis.get("key1"));  
        System.out.println("在key3后面加入值："+jedis.append("key3", "End"));  
        System.out.println("key3的值："+jedis.get("key3"));  
        System.out.println("增加多个键值对："+jedis.mset("key01","value01","key02","value02","key03","value03"));  
        System.out.println("获取多个键值对："+jedis.mget("key01","key02","key03"));  
        System.out.println("获取多个键值对："+jedis.mget("key01","key02","key03","key04"));  
        System.out.println("删除多个键值对："+jedis.del("key01","key02"));  
        System.out.println("获取多个键值对："+jedis.mget("key01","key02","key03"));  
  
        jedis.flushDB();  
        System.out.println("===========新增键值对防止覆盖原先值==============");  
        System.out.println(jedis.setnx("key1", "value1"));  
        System.out.println(jedis.setnx("key2", "value2"));  
        System.out.println(jedis.setnx("key2", "value2-new"));  
        System.out.println(jedis.get("key1"));  
        System.out.println(jedis.get("key2"));  
  
        System.out.println("===========新增键值对并设置有效时间=============");  
        System.out.println(jedis.setex("key3", 2, "value3"));  
        System.out.println(jedis.get("key3"));  
        try {  
            TimeUnit.SECONDS.sleep(3);  
        } catch (InterruptedException e) {  
            e.printStackTrace();  
        }  
        System.out.println(jedis.get("key3"));  
  
        System.out.println("===========获取原值，更新为新值==========");  
        System.out.println(jedis.getSet("key2", "key2GetSet"));  
        System.out.println(jedis.get("key2"));  
  
        System.out.println("获得key2的值的字串："+jedis.getrange("key2", 2, 4));  
    }  
}
```

输出：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220802090730.png)

### List

```java
package com.melody.base;  
  
import redis.clients.jedis.Jedis;  
  
public class TestList {  
    public static void main(String[] args) {  
        Jedis jedis = new Jedis("127.0.0.1", 6379);  
        jedis.flushDB();  
        System.out.println("===========添加一个list===========");  
        jedis.lpush("collections", "ArrayList", "Vector", "Stack", "HashMap", "WeakHashMap", "LinkedHashMap");  
        jedis.lpush("collections", "HashSet");  
        jedis.lpush("collections", "TreeSet");  
        jedis.lpush("collections", "TreeMap");  
        System.out.println("collections的内容："+jedis.lrange("collections", 0, -1));//-1代表倒数第一个元素，-2代表倒数第二个元素,end为-1表示查询全部  
        System.out.println("collections区间0-3的元素："+jedis.lrange("collections",0,3));  
        System.out.println("===============================");  
        // 删除列表指定的值 ，第二个参数为删除的个数（有重复时），后add进去的值先被删，类似于出栈  
        System.out.println("删除指定元素个数："+jedis.lrem("collections", 2, "HashMap"));  
        System.out.println("collections的内容："+jedis.lrange("collections", 0, -1));  
        System.out.println("删除下表0-3区间之外的元素："+jedis.ltrim("collections", 0, 3));  
        System.out.println("collections的内容："+jedis.lrange("collections", 0, -1));  
        System.out.println("collections列表出栈（左端）："+jedis.lpop("collections"));  
        System.out.println("collections的内容："+jedis.lrange("collections", 0, -1));  
        System.out.println("collections添加元素，从列表右端，与lpush相对应："+jedis.rpush("collections", "EnumMap"));  
        System.out.println("collections的内容："+jedis.lrange("collections", 0, -1));  
        System.out.println("collections列表出栈（右端）："+jedis.rpop("collections"));  
        System.out.println("collections的内容："+jedis.lrange("collections", 0, -1));  
        System.out.println("修改collections指定下标1的内容："+jedis.lset("collections", 1, "LinkedArrayList"));  
        System.out.println("collections的内容："+jedis.lrange("collections", 0, -1));  
        System.out.println("===============================");  
        System.out.println("collections的长度："+jedis.llen("collections"));  
        System.out.println("获取collections下标为2的元素："+jedis.lindex("collections", 2));  
        System.out.println("===============================");  
        jedis.lpush("sortedList", "3","6","2","0","7","4");  
        System.out.println("sortedList排序前："+jedis.lrange("sortedList", 0, -1));  
        System.out.println(jedis.sort("sortedList"));  
        System.out.println("sortedList排序后："+jedis.lrange("sortedList", 0, -1));  
    }  
}
```

输出：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220802090855.png)

### Set

```java
package com.melody.base;  
  
import redis.clients.jedis.Jedis;  
  
public class TestSet {  
    public static void main(String[] args) {  
        Jedis jedis = new Jedis("127.0.0.1", 6379);  
        jedis.flushDB();  
        System.out.println("============向集合中添加元素（不重复）============");  
        System.out.println(jedis.sadd("eleSet", "e1","e2","e4","e3","e0","e8","e7","e5"));  
        System.out.println(jedis.sadd("eleSet", "e6"));  
        System.out.println(jedis.sadd("eleSet", "e6"));  
        System.out.println("eleSet的所有元素为："+jedis.smembers("eleSet"));  
        System.out.println("删除一个元素e0："+jedis.srem("eleSet", "e0"));  
        System.out.println("eleSet的所有元素为："+jedis.smembers("eleSet"));  
        System.out.println("删除两个元素e7和e6："+jedis.srem("eleSet", "e7","e6"));  
        System.out.println("eleSet的所有元素为："+jedis.smembers("eleSet"));  
        System.out.println("随机的移除集合中的一个元素："+jedis.spop("eleSet"));  
        System.out.println("随机的移除集合中的一个元素："+jedis.spop("eleSet"));  
        System.out.println("eleSet的所有元素为："+jedis.smembers("eleSet"));  
        System.out.println("eleSet中包含元素的个数："+jedis.scard("eleSet"));  
        System.out.println("e3是否在eleSet中："+jedis.sismember("eleSet", "e3"));  
        System.out.println("e1是否在eleSet中："+jedis.sismember("eleSet", "e1"));  
        System.out.println("e1是否在eleSet中："+jedis.sismember("eleSet", "e5"));  
        System.out.println("=================================");  
        System.out.println(jedis.sadd("eleSet1", "e1","e2","e4","e3","e0","e8","e7","e5"));  
        System.out.println(jedis.sadd("eleSet2", "e1","e2","e4","e3","e0","e8"));  
        System.out.println("将eleSet1中删除e1并存入eleSet3中："+jedis.smove("eleSet1", "eleSet3", "e1"));//移到集合元素  
        System.out.println("将eleSet1中删除e2并存入eleSet3中："+jedis.smove("eleSet1", "eleSet3", "e2"));  
        System.out.println("eleSet1中的元素："+jedis.smembers("eleSet1"));  
        System.out.println("eleSet3中的元素："+jedis.smembers("eleSet3"));  
        System.out.println("============集合运算=================");  
        System.out.println("eleSet1中的元素："+jedis.smembers("eleSet1"));  
        System.out.println("eleSet2中的元素："+jedis.smembers("eleSet2"));  
        System.out.println("eleSet1和eleSet2的交集:"+jedis.sinter("eleSet1","eleSet2"));  
        System.out.println("eleSet1和eleSet2的并集:"+jedis.sunion("eleSet1","eleSet2"));  
        System.out.println("eleSet1和eleSet2的差集:"+jedis.sdiff("eleSet1","eleSet2"));//eleSet1中有，eleSet2中没有  
        jedis.sinterstore("eleSet4","eleSet1","eleSet2");//求交集并将交集保存到dstkey的集合  
        System.out.println("eleSet4中的元素："+jedis.smembers("eleSet4"));  
    }  
}
```

输出：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220802090944.png)

### Hash

```java
package com.melody.base;  
  
import redis.clients.jedis.Jedis;  
  
import java.util.HashMap;  
import java.util.Map;  
  
public class TestHash {  
    public static void main(String[] args) {  
        Jedis jedis = new Jedis("127.0.0.1", 6379);  
        jedis.flushDB();  
        Map<String,String> map = new HashMap<String,String>();  
        map.put("key1","value1");  
        map.put("key2","value2");  
        map.put("key3","value3");  
        map.put("key4","value4");  
        //添加名称为hash（key）的hash元素  
        jedis.hmset("hash",map);  
        //向名称为hash的hash中添加key为key5，value为value5元素  
        jedis.hset("hash", "key5", "value5");  
        System.out.println("散列hash的所有键值对为："+jedis.hgetAll("hash"));//return Map<String,String>  
        System.out.println("散列hash的所有键为："+jedis.hkeys("hash"));//return Set<String>  
        System.out.println("散列hash的所有值为："+jedis.hvals("hash"));//return List<String>  
        System.out.println("将key6保存的值加上一个整数，如果key6不存在则添加key6："+jedis.hincrBy("hash", "key6", 6));  
        System.out.println("散列hash的所有键值对为："+jedis.hgetAll("hash"));  
        System.out.println("将key6保存的值加上一个整数，如果key6不存在则添加key6："+jedis.hincrBy("hash", "key6", 3));  
        System.out.println("散列hash的所有键值对为："+jedis.hgetAll("hash"));  
        System.out.println("删除一个或者多个键值对："+jedis.hdel("hash", "key2"));  
        System.out.println("散列hash的所有键值对为："+jedis.hgetAll("hash"));  
        System.out.println("散列hash中键值对的个数："+jedis.hlen("hash"));  
        System.out.println("判断hash中是否存在key2："+jedis.hexists("hash","key2"));  
        System.out.println("判断hash中是否存在key3："+jedis.hexists("hash","key3"));  
        System.out.println("获取hash中的值："+jedis.hmget("hash","key3"));  
        System.out.println("获取hash中的值："+jedis.hmget("hash","key3","key4"));  
    }  
}
```

输出：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220802091041.png)

### Zset

与其他类似

---

### TestPassword

```java
package com.melody.base;  
  
import redis.clients.jedis.Jedis;  
  
public class TestPassword {  
    public static void main(String[] args) {  
        Jedis jedis = new Jedis("127.0.0.1", 6379);  
  
        //验证密码，如果没有设置密码这段代码省略  
//        jedis.auth("password");  
  
        jedis.connect(); //连接  
        jedis.disconnect(); //断开连接  
  
        jedis.flushAll(); //清空所有的key  
    }  
}
```

---

## 事务

```java
package com.melody.multi;  
  
import com.alibaba.fastjson.JSONObject;  
import redis.clients.jedis.Jedis;  
import redis.clients.jedis.Transaction;  
  
public class TestMulti {  
    public static void main(String[] args) {  
        //创建客户端连接服务端，redis服务端需要被开启  
        Jedis jedis = new Jedis("127.0.0.1", 6379);  
        jedis.flushDB();  
  
        JSONObject jsonObject = new JSONObject();  
        jsonObject.put("hello", "world");  
        jsonObject.put("name", "java");  
        //开启事务  
        Transaction multi = jedis.multi();  
        String result = jsonObject.toJSONString();  
        System.out.println(result);  
        try{  
            //向redis存入一条数据  
            multi.set("json", result);  
            //再存入一条数据  
            multi.set("json2", result);  
            //这里引发了异常，用0作为被除数  
            int i = 100/0;  
            //如果没有引发异常，执行进入队列的命令  
            multi.exec();  
        }catch(Exception e){  
            e.printStackTrace();  
            //如果出现异常，回滚  
            multi.discard();  
        }finally{  
            System.out.println(jedis.get("json"));  
            System.out.println(jedis.get("json2"));  
            //最终关闭客户端  
            jedis.close();  
        }  
    }  
}
```

输出：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220802091829.png)

---

# SpringBoot 整合 Redis

`SpringBoot` 操作数据： `spring-data` `jpa` `jdbc` `mongodb` `redis`

`SpringData` 也是和 `SpringBoot` 齐名的项目

> 在 `SpringBoot 2.x` 之后，原来使用的 `Jedis` 被替换为了 `lettuce`
> 
>> `Jedis`: 采用的直连，多个线程操作是不安全的；如果要避免不安全的，使用 `Jedis Pool` 连接池 (类似BIO模式)
>> 
>> `Lettuce`: 采用 `netty`，实例可以在多个线程中共享，不存在线程不安全的情况，可以减少线程数量 (类似NIO模式)

## 源码分析

```java
@Bean
@ConditionalonMissingBean(name="redisTemplate")//我们可以自己定义一个redisTemplate来替换这个默认的！
public RedisTemplate<object,object>redisTemplate(RedisconnectionFactory redisconnectionFactory)
	throws UnknownHostException {
	//默认的RedisTemplate没有过多的设置，redis对象都是需要序列化！
	//两个泛型都是object,object的类型，我们后使用需要强制转换<string,object>
	RedisTemplate<object,object>template new RedisTemplate<>();
	template.setConnectionFactory (redisConnectionFactory);
	return template;
}
@Bean
@ConditionalonMissingBean,//由于string是redis中最常使用的类型，所以说单独提出来了一个bean!
public stringRedisTemplate stringRedisTemplate(RedisconnectionFactory redisconnectionFactory)
	throws UnknownHostException {
	stringRedisTemplate template new stringRedisTemplate();
	template.setConnectionFactory(redisConnectionFactory);
	return template;
}
```

## 测试

1. 导入依赖

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```

2. 配置连接

```properties
# springboot 所有的配置类，都有一个自动配置类 RedisAutoConfiguration# 自动配置类都会绑定一个 properties 配置文件 RedisProperties  
spring.redis.host=127.0.0.1  
spring.redis.port=6379  
spring.redis.database=0
```

3. 测试

```java
package com.melody;  
  
import org.junit.jupiter.api.Test;  
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.boot.test.context.SpringBootTest;  
import org.springframework.data.redis.core.RedisTemplate;  
  
import javax.annotation.Resource;  
import javax.annotation.Resources;  
  
@SpringBootTest  
class RedisSpringBootApplicationTests {  
  
    @Autowired  
    private RedisTemplate redisTemplate;  
  
    @Test  
    void contextLoads() {  
        // redisTemplate 操作不同的数据类型,api和我们的指令是一样的  
        // opsForValue 操作字符串，类似string  
        // opsForList 操作List  
        // opsForSet 操作Set  
        // opsForHash 操作Hash  
        // opsForGeo 操作GEO  
        // opsForZSet 操作Zset  
  
        // 除了基本的操作，我们常用的方法都可以直接通过redisTemplate来操作，比如事务和基本的CRUD  
  
        // 获取redis的连接对象  
        /*RedisConnection connection = redisTemplate.getConnectionFactory().getConnection();  
        connection.flushDb();*/  
        redisTemplate.opsForValue().set("mykey", "melody");  
        System.out.println(redisTemplate.opsForValue().get("mykey"));  
    }  
  
}
```


# Redis.conf 详解

启动的时候，就通过配置文件启动

## 单位

> 配置文件 unit 单位对大小写不敏感


```conf
# Note on units: when memory size is needed, it is possible to specify
# it in the usual form of 1k 5GB 4M and so forth:
#
# 1k => 1000 bytes
# 1kb => 1024 bytes
# 1m => 1000000 bytes
# 1mb => 1024*1024 bytes
# 1g => 1000000000 bytes
# 1gb => 1024*1024*1024 bytes
#
# units are case insensitive so 1GB 1Gb 1gB are all the same.
```

## 包含

> 可以将多个conf文件组成一个

```conf
# Include one or more other config files here.  This is useful if you
# have a standard template that goes to all Redis servers but also need
# to customize a few per-server settings.  Include files can include
# other files, so use this wisely.
#
# Notice option "include" won't be rewritten by command "CONFIG REWRITE"
# from admin or Redis Sentinel. Since Redis always uses the last processed
# line as value of a configuration directive, you'd better put includes
# at the beginning of this file to avoid overwriting config change at runtime.
#
# If instead you are interested in using includes to override configuration
# options, it is better to use include as the last line.
#
# include .\path\to\local.conf
# include c:\path\to\other.conf
```

## 网络

```conf
bind 127.0.0.1 # 绑定的ip
protected-mode yes # 是否受保护
port 6379 # 端口设置
```

## 通用

```conf
daemonize yes # 默认no，以守护进程的方式运行，需要自己设置为yes
pidfile /var/run/redis_6379.pid # 如果以后台的方式运行，我们就需要指定一个pid文件

# Specify the server verbosity level.
# This can be one of:
# debug (a lot of information, useful for development/testing)
# verbose (many rarely useful info, but not a mess like the debug level)
# notice (moderately verbose, what you want in production probably)
# warning (only very important / critical messages are logged)
loglevel notice # 日志级别

logfile "" #日志的文件位置名

databases 16 # 默认数据库数量,默认16

always-show-log yes # 是否总是显示logo
```

## 快照

> 持久化：在规定时间内，执行了多少次操作，则会持久到 .rdb .aof

> redis 是内存数据库，如果没有持久化，那么数据断电及失

```conf
save 900 1 # 900s内，如果至少有一个key进行修改，我们就进行持久化操作
save 300 10 # 300s内，如果至少有10个key进行修改，我们就进行持久化操作
save 60 10000

stop-writes-on-bgsave-error yes # 持久化如果出错，是否继续工作

rdbcompression yes # 是否压缩 rdb 文件，需要消耗一些CPU资源

rdbchecksum yes # 保存rdb文件时候，检查校验错误

dis ./ # rdb文件保存目录
```

## 安全

命令设置密码： `config set requirepass "fahaxiki"`

密码验证登陆： `auth fahaxiki`

```conf
requirepass fahaxiki # 配置文件设置密码为fahaxiki
```

## 限制 CLIENTS

```conf
maxclients 10000 # 设置能连接上redis的最大客户端数量

maxmemory <bytes> # redis配置最大的内存容量

maxmemory-policy noeviction # 内存达到上限的处理策略
```

### 6种策略

1. `volatile-lru` 只对设置了过期时间的key进行LRU(默认值)
2. `allkeys-lru` 删除lru算法的key
3. `valatile-random` 随机删除即将过期的key
4. `allkeys-random` 随机删除key 
5. `valatile-ttl` 删除即将过期的
6. `noeviction` 永不过期，返回错误

## APPEND ONLY 模式 aof配置

```conf
appendonly no # 默认不开启aof模式，默认使用rdb方式持久化，在大部分情况下rdb够用

appendfilename "appendonly.aof" # 持久化的文件名字

appendfsync everysec # 有no always 三种选项；everysec每秒执行一次sync，可能会丢失这1s的数据；always每次修改都会sync，消耗性能；no不执行sync，此时OS自己同步数据，速度最快

auto-aof-rewrite-min-size 64mb # 如果aof文件大于64mb，太大了，fork一个新的进程来将我们的文件进行重写
```

# Redis持久化

`Redis` 是内存数据库，如果不将内存种的数据库状态保存到磁盘，那么一旦服务器进程退出，服务器种的数据库状态也会消失，所以 `Redis` 提供了持久化功能

## RDB (Redis Database)

在主从复制中，rdb就是备用了

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220802105947.png)

> 在指定的时间间隔内将内存种的数据集快照写入磁盘，也就是Snapshot快照，它恢复时是将快照文件直接读到内存里
> 
>> `Redis` 会淡出创建（fork）一个子进程来进行持久化，会先将数据写入到一个临时文件中，待持久化过程都结束了，再用这个临时文件替换上次持久化好的文件。
>> 
>> 整个过程中，主进程不进行任何IO操作。这就确保了极高的性能。如果需要进行大规模数据的恢复，且对于数据恢复的完整性不是非常敏感，那 `RDB` 方式比 `AOF` 方式更加高效。
>> 
>> `RDB` 的缺点是最后一次持久化后的数据可能丢失。
>> 
>> 我们默认的就是 `RDB`，一般情况下不需要修改这个配置

==`RDB` 保存的文件是 `dump.rdb`==

有时候再生产环境我们会将这个文件备份

### 触发机制

1. `save` 的规则满足的情况下，会自动触发 `RDB` 规则
2. 执行 `flushall` 命令，也会触发我们的 `rdb` 规则
3. 退出 `redis` ，也会产生 `rdb` 文件

备份就自动生成一个 `dump.rdb`

### 恢复 rdb 文件

只需要将rdb文件放再我们redis启动目录就可以 

查看存放位置 `config get dir`

> `Redis` 启动的时候会自动检查 `dump.rdb` 恢复其中的数据


### 优点

- 适合大规模的数据恢复 `dump.rdb`
- 对数据完整性要求不高

### 缺点

- 需要一定的时间间隔进程操作；如果 `Redis` 意外宕机了，这个最后一次修改的数据就没有了
- fork进程的时候，会占用一定的内存空间

## AOF (Append Only File)

将我们的所有命令都记录下来,类似.zsh_history,恢复的时候把这个文件都执行一遍

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220802111201.png)

> 以日志的形式来记录每个写操作，将 `Redis` 执行过的所有指令记录下来（读操作不记录），只许追加文件但不可以改写文件， `Redis` 启动之初会读取该文件重新构建数据，
> 
> 换言之， `Redis` 重启的话就根据日志文件的内容将写指令从前到后执行一次以完成数据的恢复工作

==`AOF` 保存的是 `appendonly.aof` 文件==

`AOF` 默认不开启，需要手动进行配置 `appendonly yes`，重启即生效

### 文件修复

> `redis-check-aof` 在 `appendonly.aof` 出错的时候（被破坏了）进行修复
> 
> 如果 `aof` 文件有错误，`Redis` 是无法启动的，需要修复 `aof` 文件， `redis` 提供工具 `redis-check-aof --fix appendonly.aof`


### 优点

- 每一次修改都同步，文件的完整性更加好
- 每秒同步一次，可能会丢失一秒的数据
- 从不同步，效率最高

### 缺点

- 相对于数据文件来说， `AOF` 远远大于 `RDB`，修复的速度也比 `rdb` 慢
- `AOF` 运行效率比 `RDB` 慢，所以 `Redis` 默认配置是 `RDB`

## 扩展

1. RDB持久化方式能够在指定的时间间隔内对你的数据进行快照存储
2. AOF持久化方式记录每次对服务器写的操作，当服务器重启的时候会重新执行这些命令来恢复原始的数据，AOF命令以Redis协议追加保存每次写的操作到文件末尾，Redis还能对AOF文件进行后台重写，使得AOF文件的体积不至于过大。
3. 只做缓存，如果你只希望你的数据在服务器运行的时候存在，你也可以不使用任何特久化
4. 同时开启两种持久化方式
- 在这种情况下，当rdis重启的时候会优先载入AOF文件来恢复原始的数据，因为在通常情况下AOF文件保存的数据集要比RDB文件保存的数据集要完整。
- RDB的数据不实时，同时使用两者时服务器重启也只会找AOF文件，那要不要只使用AOF呢？作者建议不要，因为RDB更适合用于备份数据库(AOF在不断变化不好备份)，快速重启，而且不会有AOF可能潜在的Bug,留着作为一个万一的手段。
5. 性能建议
- 因为RDB文件只用作后备用途，建议只在Slve上持久化RDB文件，而且只要15分钟备份一次就够了，只保留save9001这条规则。
- 如果Enable AOF,好处是在最恶劣情况下也只会丢失不超过两秒数据，启动脚本较简单只load自己的AOF文件就可以了，代价一是带来了持续的IO,二是AOF rewrite的最后将rewrite过程中产生的新数据写到新文件造成的阻塞几乎是不可避免的。只要硬盘许可，应该尽量减少AOF rewrite的频率，AOF重写的基础大小默认值64M太小了，可以设到5G以上，默认超过原大小100%大小重写可以改到适当的数值。
- 如果不Enable AOF,仅靠Master-Slave Repllcation实现高可用性也可以，能省掉一大笔IO,也减少了rewritel时带来的系统波动。代价是如果Master/Slave同时倒掉，会丢失十几分钟的数据，启动脚本也要比较两个Master/Slave中的RDB文件，载入较新的那个，微博就是这种架构。

# Redis 发布订阅

`Redis` 发布订阅(pub/sub)是一种消息通信模式：发送者(pub)发送消息，订阅者(sub)接收消息

`Redis` 客户端可以订阅任意数量的频道

订阅/发布消息图：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220802113102.png)

频道channel1，以及订阅这个频道的三个客户端的关系：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220802113329.png)

当有新消息通过 `PUBLISH` 命令发送给频道 channel1时，这个消息就会被发送给订阅他的三个客户端

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220802113435.png)

## 命令

这些命令被广泛用于构建即使通信应用，比如网络聊天室(chatroom)和实时广播、实时提醒等。

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220802113551.png)

## 测试

client1 订阅端
```bash
> subscribe melody
```

client2 发送端
```bash
> publish melody "hello world"
```

## 原理

> Redis是使用C实现的，通过分析Redis源码里的pubsub.c文件，了解发布和订阅机制的底层实现，籍此加深对Redis的理解。
> 
> Redis通过PUBLISH、SUBSCRIBE和PSUBSCRIBE等命令实现发布和订阅功能。
> 
> 通过SUBSCRIBE命令订阅某频道后，redis-server里维护了一个字典，字典的键就是一个个channel,而字典的值则是一个链表，链表中保存了所有订阅这个channel的客户端。SUBSCRIBE命令的关键，就是将客户端添加到给定channel的订阅链表中。
> 
> 通过PUBLISH命令向订阅者发送消息，redis-.server会使用给定的频道作为键，在它所维护的channel字典中查找记录了订阅这个频道的所有客户端的链表，遍历这个链表，将消息发布给所有订阅者。
> 
> Pub/Sub从字面上理解就是发布(Publish)与订阅(Subscribe),在Redist中，你可以设定对某一个key值进行消息发布及消息订阅，当一个ky值上进行了消息发布后，所有订阅它的客户端都会收到相应的消息。这一功能最明显的用法就是用作实时消息系统，比如普通的即时聊天，群聊等功能。

---

# Redis 主从复制

## 概念

> 主从复制：
>> 是指将一台 `Redis` 服务器的数据，复制到其他的 `Redis` 服务器。前者称为主节点(master/leader)，后者称为从节点(slave/follower)
>> 数据的复制是单向的，只能从主节点到从节点。`master` 以写为主，`slave` 以读为主
>> ==默认情况下，每台 `Redis` 服务器都是主节点；== 且一个主节点可以有多个从节点（或没有从节点），但一个从节点只能有一个主节点

> 主从复制的作用：
>> 1. 数据冗余：主从复制实现了数据的热备份，是持久化之外的一种数据冗余方式
>> 2. 故障恢复：当主节点出现问题时，可以由从节点提供服务，实现快速的故障恢复；实际上是一种服务的冗余。
>> 3. 负载均衡：在主从复制的基础上，配合读写分离，可以由主节点提供写服务，由从节点提供读服务（即写Redis数据时应用连接主节点，读Redis数据时应用连接从节点），分担服务器负载，尤其是在写少读多的场景下，通过多个从节点分担读负担，可以大大提高Redis的并发量
>> 4. 高可用基石：除了上述作用以外，主从复制还是哨兵和集群能够实施的基础

一般来说，要将 `Redis` 运用于工程项目中，只使用一台 `Redis` 是万万不能的，原因如下：

1. 从结构上，单个 `Redis` 服务器会发生单点故障，并且一台服务器需要处理所有的请求负载，压力较大；
2. 从容量上，单个 `Redis` 服务器内存容量有限，就算一台 `Redis` 服务器内存容量为256GB，也不能将所有内存用作 `Redis` 存储内存，一般来说，单台 `Redis` 最大使用内存不应该超过20GB

对于“读多写少”的场景，可以使用如下架构：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220802115814.png)

## 环境配置

只配置从库，不用配置主库

```bash
> info replication # 查看当前库的信息
role:master # 角色
connected_slaves:0 # 没有从机
```

`redis.conf` 复制出多份，如：`redis79.conf` `redis80.conf` `redis81.conf`

### 初始配置

要配置：端口、PID名字、log文件名字、dump.rdb名字

`redis79.conf` :
```conf
logfile "6379.log"
dbfilename dump6379.rdb
```

`redis80.conf` :
```conf
port 6380
pidfile /var/run/redis_6380.pid
logfile "6380.log"
dbfilename dump6380.rdb
```

`redis80.conf` :
```conf
port 6381
pidfile /var/run/redis_6381.pid
logfile "6381.log"
dbfilename dump6381.rdb
```

### 启动

启动三个redis服务：

> master 

```bash
> redis-server config/redis79.conf
```

> slave1

```bash
> redis-server config/redis80.conf
```

> slave2

```bash
> redis-server config/redis81.conf
```

### 一主二从配置

一般情况下只配置从机即可

> master 

```bash
# 主机不需要配置
```

> slave1 

```bash
redis-cli -p 6380
> slaveof 127.0.0.1 6379
> info replication 
role: slave # 从机
```

> slave2 

```bash
redis-cli -p 6381
> slaveof 127.0.0.1 6379
> info replication 
role: slave # 从机
```

#### 配置文件配置主从

真实的主从配置应该在配置文件中配置，是永久的！命令配置只是暂时的

```conf
replicaof 127.0.0.1 6379
```

#### 细节

主机可以写，从机只可以读，从机不能写

> 主机中的所有信息和数据，都会自动被从机保存

> 主机断开连接时，从机依旧连接到主机上，但是没有写操作，此时，主机如果回来了，从机依旧可以直接取到主机写入的内容
> 
> 如果是使用命令来配置的主从复制，此时，如果从机重启就会变回master
> 
> 只要变为从机，立马就会从主机中获取值

## 复制原理

> slave启动成功连接到master后会发送一个sync命令
> 
> master接收到命令，启动后台的存盘进程，同时收集所有接收到的用于修改数据集命令，在后台进程执行完毕后，master将传送整个数据文件到slave，并完成一次完全同步
> 
> - **全量复制**：slave服务在接收到数据库文件数据后，将其存盘并加载到内存中
> - **增量复制**：master继续将新的所有收集到的修改命令一次传给slave，完成同步
> 
> 但只要是重新连接master，一次完全同步（全量复制）将被执行，数据一定可以在从机中获取到

## 层层链路模型

79是80master 80是81master

80是79slave 81是80slave

> 此时的80依旧是从节点，无法进行写入

## 主机断开情况

如果主机断开，能不能自己重新选择一个主机? 可以

```bash
> slaveof no one # 主机断开时候，使自己变为主机，其他节点可以手动连接到最新的主节点
```

但之前的主机修复完成后，那就只能重新连接

# 哨兵模式（自动选取主机的模式）

## 概念

> 主从切换技术的方法：
>> 当主服务器宕机后，需要手动把一台从服务器切换为主服务器，需要人工干预，费时费力，还会造成一段时间服务不可用。

> 哨兵模式：
>> 哨兵模式是一种特殊的模式，首先 `Redis` 提供了哨兵的命令，哨兵是一个独立的进程，作为进程，他会独立运行。
>> 
>> 其原理是 **哨兵通过发送命令，等待Redis服务器相应，从而监控运行的多个Redis实例**
>> 
>> 后台监控主机是否故障，故障后根据投票 ==自动将从库转换为主库==

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220802145316.png)

## 作用

哨兵的两个作用：

1. 通过发送命令，让Redis服务器返回监控其运行状态，包括主服务器和从服务器
2. 当哨兵检测到master宕机，会自动将slave切换成master，然后通过 ***订阅发布模式*** 通知其他的从服务器，修改配置文件，让他们切换主机

## 多哨兵模式

> 然而一个哨兵进程对Redis服务器进行监控，可能会出现问题，为此，我们可以使用多个哨兵进行监控。
> 各个哨兵之间还会进行监控，这样就形成了多哨兵模式

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220802145645.png)

假设主服务器宕机，哨兵1先检测到这个结果，系统并不会马上进行failover过程，仅仅是哨兵1主观的认为主服务器不可用，这个现象称为 **主观下线**

当后面的哨兵也检测到主服务器不可用，并且数量达到一定值时，那么哨兵之间就会进行一次投票，投票的结果由一个哨兵发起，进行failover[故障转移]操作。

切换成功后，就会通过发布订阅模式，让各个哨兵把自己监控的从服务器实现切换主机，这个过程称为 **客观下线**

## 测试

一主二从

1. 配置哨兵配置文件

> sentinel.conf
> ```conf
> sentinel monitor myredis 127.0.0.1 6379 1 # 1代表主机宕机后会投票,myredis为被监控的名称
> ```

2. 启动哨兵

```bash
$ redis-sentinel config/sentinel.conf
```

住过master节点断开了，这个时候就会从从机中随机选择一个从机作为新主机(这里有一个投票算法)

如果之前的主机回来了，之恶能归并到新的主机下，当作从机，这就是哨兵模式的规则

## 优点

1. 哨兵集群，基于主从复制模式，所有的主从配置优点，它都有
2. 主从可以切换，故障可以转移，系统的可用性更好
3. 哨兵模式就是主从模式的升级，手动到自动，更加健壮

## 缺点

1. Redis在线扩容不好，集群容量一旦达到上限，在线扩容十分麻烦
2. 实现哨兵模式的配置很麻烦，里边有很多选择

> 如果有哨兵集群，需要配置port

---

# Redis缓存穿透和雪崩

Redis缓存的使用，极大的提升了应用程序的性能和效率，特别是数据查询方面。但同时，它也带来了一些问题。

其中，最要害的问题，就是数据的一致性问题，从严格意义上讲，这个问题无解。如果对数据的一致性要求很高，那么就不能使用缓存

另外的一些典型问题就是，缓存穿透、缓存雪崩和缓存击穿。

## 缓存穿透(查不到导致)

### 概念

> 缓存穿透：
> 
>> 用户想要查询一个数据，发现redis内存数据库没有，也就是缓存没有命中，于是向持久层数据库查询。发现也没有，于是本次查询失败
>> 
>> 当用户很多的时候，缓存都没有命中，于是都去请求了持久层数据库，这会给持久层数据库造成很大的压力，这时候就相当于出现了缓存穿透

### 解决方案

### 布隆过滤器

> 布隆过滤器是一种数据结构，对所有可能查询的参数以hash形式存储，在控制层先进行校验，不符合则丢弃，从而避免了对底层存储系统的查询压力

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220802152623.png)

但这种方法会存在两个问题：

1. 如果空值能够被缓存起来，这就意味着缓存需要更多的空间存储更多的键，因为着当中可能会有很多的空值的键
2. 即使对空值设置过期时间，还是会存在缓存层和存储层的数据会有一段时间窗口的不一致，这对于需要保持一致性的业务会有影响

## 缓存击穿(量太大，缓存过期导致)

### 概念

> 缓存击穿：
> 
>> 是指一个key非常热点，在不停的扛着大并发，大并发集中对这一个点进行访问，当这个key在失效的瞬间，持续的大并发就穿破缓存，直接请求数据库，就像在一个屏障上凿开了一个洞
>> 
>> 当某个key在过期的瞬间，有大量的请求并发访问，这类数据一般是热点数据，由于缓存过期，会同时访问数据库来查询最新数据，并且写回缓存，会导致数据库瞬间压力过大

### 解决方案

#### 设置热点数据用不过期

从缓存层面看，没有设置过期时间，所以不会出现热点key过期后产生的问题

#### 加互斥锁

分布式锁：使用分布式锁，保证对于每个key同时只有一个线程取查询后端服务，其他线程没有获取分布式锁的权限，因此值需要等待即可。这种方式讲高并发的压力转移到了分布式锁，因此对分布式锁的考验很大。

## 缓存雪崩

### 概念

> 缓存雪崩：
> 
>> 是指在某一个时间段，缓存集中过期失效，Redis宕机

产生雪崩的原因之一：比如马上到双11，要迎来一波抢购，这波商品时间比较集中的放入了缓存，假设缓存一小时。na 

那么到了凌晨一点的时候，这批商品的缓存就都过期了，而对这批商品的访问查询，都落到了数据库上，对于数据而言，就会产生周期性压力波峰。

于是所有的请求都会到达存储层，存储层的调用量会暴增，造成存储层也挂掉的情况。

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220802154040.png)

其实集中过期，倒不是非常致命，比较致命的缓存雪崩，是缓存服务器某个节点宕机或断网。因为自然形成的缓存雪崩，一定实在某个时间段集中创建缓存，

这个时候，数据库也是可以顶住压力的。无非就是对数据库产生周期性的压力而已。

而缓存服务节点的宕机，对数据库服务器造成的压力是不可预知的，很有可能瞬间就把数据库压垮。

### 解决方案

#### redis高可用

> 这个思想的含义是，既然redis有可能挂掉，那就增加几台redis，这样一台挂掉之后其他的还可以继续工作，其实就是搭建的集群

#### 限流降级

> 这个解决方案的思想是，在缓存失效后，通过加锁或者队列来控制读数据库写缓存的线程数量。比如对某个key只允许一个线程查询数据和写缓存，其他线程等待。

#### 数据预热

> 数据加热的含义就是在正式部署之前，先把可能的数据先预先访问一遍，这样部分可能大量访问的数据就会加载到缓存中。在几件共发生大并发访问前手动触发加载缓存不同的key，设置不同的过期时间，让缓存失效的时间尽量均匀