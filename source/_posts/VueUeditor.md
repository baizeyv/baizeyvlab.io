---
title: vue ueditor guide
date: 2022-07-29 10:11:00
tags: 
 - vue
 - editor
 - upload
categories:
 - Editor
 - Java
---

# vue-ueditor-wrap安装、封装及使用

## 安装 vue ueditor

`npm install vue-ueditor-wrap`

## 下载已处理的 UEditor

进入[此连接下载](https://github.com/HaoChuan9421/vue-ueditor-wrap/tree/master/assets/downloads)对应版本

> 该文章以 `jsp` 版本为例

将下载的压缩包解压后重命名为 `UEditor` ，放入 `vue` 项目的 `public` 文件夹中，如下图所示：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220729111032.png)

将 `jsp/config.json` 中的内容放入 `ueditor.config.js` ，放在 `toolbars` 之后即可：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220729111332.png)

修改配置中的上传文件的名称以及 `action`

> 此处上传文件名称即为 **发起请求是传文件参的参数名称**， 一定要与上传接口对应上！
> 
> `action` 即 `url` 后缀的 `/?action=uploadimage` 此样式
> 
> 同时也要注意配置文件中的 **访问路径前缀**

## 上传接口注意事项

> `UEditor` 要求上传接口返回的参数中包含 `state` , `url` , `title` , `original`
> 
>> 当返回的 `state` 为 `SUCCESS` 时，即为上传成功，表示上传的状态
>> 
>> `url` 表示该文件真实的 `url` 地址
>> 
>> `title` 代表该文件在标签中的 `title` 字段
>> 
>> `original` 代表该文件在标签中的 `alt` 字段  

`Controller` 例子：

> 其中包含了自己项目中的内容，这仅仅是一个展示返回字段的例子

```java
@PostMapping("/uploadTest")  
public String uploadFileTest(MultipartFile file) throws Exception {  
    try {  
        // 上传文件路径  
        String filePath = NiuaConfig.getUploadPath();  
        // 上传并返回新文件名称  
        String fileName = FileUploadUtils.upload(filePath, file);  
        String url = serverConfig.getUrl() + fileName;  
        redisTemplate.opsForValue().set("uploadUrl", url);  
        ObjectMapper mp = new ObjectMapper();  
        UEResJson neo = new UEResJson();  
        neo.setState("SUCCESS");  
        neo.setUrl(url); // url  
        neo.setTitle(fileName); // title  
        neo.setOriginal(fileName); // alt  
  
        String str = mp.writeValueAsString(neo);  
  
        return str;  
    } catch (Exception e) {  
        return "";  
    }  
}
```

> 该实体类中，除此之外，也可选 `type` , `size` 字段

```java
import lombok.Data;  
  
import java.io.Serializable;  
  
@Data  
public class UEResJson implements Serializable {  
  
    private String state;  
  
    private String url;  
  
    private String title;  
  
    private String original;  
  
}
```


## 封装 UE 子组件

在 `components` 文件夹中新建 `UEditor` 文件夹，并在其中创建 `UE.vue` 文件

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220729111135.png)

`UE.vue` 内容如下：

> 注意 `config` 中的 `UEDITOR_HOME_URL` 以及 `serverUrl`
> 
> `UEDITOR_HOME_URL` 即下载的配置文件的路径
> 
> `serverUrl` 即上传模块的通用接口

```js
<template>  
    <div>  
        <VueUeditorWrap  v-model="content" :config="config"></VueUeditorWrap>  
        <el-button typeof="primary" @click="showContent()">Get UE Content</el-button>  
    </div>  
</template>  
  
<script>  
  
import VueUeditorWrap from 'vue-ueditor-wrap'  
import {getToken} from "@/utils/auth";  
export default {  
    name: "UE",  
    components: {  
        VueUeditorWrap,  
    },  
    data() {  
        return {  
        }  
    },  
    methods: {  
        showContent() {  
            alert(this.content);  
        },  
    },  
    props: {  
        content: {  
            type: String,  
            default: '<h2><img src="http://img.baidu.com/hi/jx2/j_0003.gif"/>Vue + UEditor + v-model双向绑定!+!$#</h2>'  
        },  
        config: {  
            type: Object,  
            default: () => ({  
                UEDITOR_HOME_URL: "/UEditor/",  
                serverUrl: "/v1/admin/common/uploadTest",  
                headers: {  
                    Authorization: "Bearer " + getToken(),  
                },  
                // 编辑器不自动被内容撑高  
                autoHeightEnabled: false,  
                // 初始容器高度  
                initialFrameHeight: 500,  
                // 初始容器宽度  
                initialFrameWidth: '100%',  
                lang:'zh-cn'  
            })  
        },  
    }  
}  
  
</script>  
  
<style scoped>  
  
</style>
```

## 在父组件中使用 UE 子组件

- 直接使用

```js
<template>
	<div>
		<UE></UE>
	</div>
</template>

<script>
import UE from "@/components/UEditor/UE"
export default {  
    components: {  
        UE  
    },
}
</script>
  
<style>  
</style>
```

- 修改子组件中的默认内容

```js
<template>
	<div>
		<UE :content="myContent" :config="myConfig"></UE>
	</div>
</template>

<script>
import UE from "@/components/UEditor/UE"
export default {  
    components: {  
        UE  
    },
    data() {
	    return {
		    myContent: 'init content',
		    myConfig: {  
	            type: Object,  
	            default: () => ({  
	                UEDITOR_HOME_URL: "/UEditor/",  
	                serverUrl: "/v1/admin/common/uploadTest",  
	                headers: {  
	                    Authorization: "Bearer " + getToken(),  
	                },  
	                // 编辑器不自动被内容撑高  
	                autoHeightEnabled: false,  
	                // 初始容器高度  
	                initialFrameHeight: 500,  
	                // 初始容器宽度  
	                initialFrameWidth: '100%',  
	                lang:'zh-cn'  
	            })  
	        }, 
	    }
    }
}
</script>
  
<style>  
</style>
```

## 可能遇到的问题

### 视频问题

上传视频成功后，视频的样式显示为以下时，为正常状态，可在预览模式下播放该视频。

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220729111157.png)

如果要在编辑时使该视频可播放，进行如下操作：

> ***注意：此操作会导致视频不可修改大小，可能出现无法选中视频、无法删除视频、视频之后的内容无法编辑等情况；请谨慎使用！***

>以下操作均可只修改 `ueditor.all.js` 之后 使用 `uglifyjs` 工具 将其压缩为 `ueditor.all.min.js` 后进行替换，此时不必修改 `ueditor.all.min.js`，***推荐使用该方法***

> `uglifyjs` 安装及使用
> 
> `npm install uglify-js`
> 
> `uglifyjs ueditor.all.js -m -o ueditor.all.min.js`

- `ueditor.all.min.js`

1. 搜索 `ueditor.all.min.js` ，查询 `UE.htmlparser` , `this.filterInputRule` , `toHtml` 这些内容紧邻的三行， (查询以下内容即可)

**将此三行进行注释**

```js
a = UE.htmlparses(a);
this.filterInputRule(a);
a = a.toHtml();
```

2. 搜索 `edui-upload-video video-js vjs-default-skin` 内容，找到类似以下内容的一行：

`for (var k, m = c.length; a < m; a++) k = c [a], l = "upload" == h ? "edui-upload-video video-js vjs-default-skin" : "edui-faked-video", g.push (d (k.url, k.width || 420, k.height || 280, "tmpVedio" + a, null, l, "image"));`

将最后的 `image` 改为 `video` 即可

- `ueditor.all.js`

1. 搜索 `var root = UE.htmlparser(html)` ，注释以下三行内容

```js
//var root = UE.htmlparser(html);
//me.filterInputRule(root);
//html = root.toHtml();
```

2. 搜索 ` html.push (creatInsertStr (vi.url, vi.width || 420, vi.height || 280, id + i, null, cl, 'image'));`

将最后的 `image` 改为 `video` 即可

3. 搜索 `creatInsertStr`

找到  `var html = creatInsertStr( img2video ? node.getAttr('_url') : node.getAttr('src'),node.getAttr('width'),node.getAttr('height'),null,node.getStyle('float') || '',className,img2video ? 'image':'video');`

将最后的 `image` 改为 `video` 即可

### header 问题

如果在上传过程中需要传 `header`，则进行以下操作：

> 点击上传的 `header` 已经在修改后的版本内置了，只需修改拖拽上传的即可，这里仅展示修改 `ueditor.all.js` 
> 
>  **修改后记得压缩为 `ueditor.all.min.js`**

1. 在 `ueditor.all.js` 中搜索 `UE.plugin.register('autopuload', function (){` 在此函数中，找到以下内容

```js
var xhr = new XMLHttpRequest(),  
    fd = new FormData(),  
    params = utils.serializeParam(me.queryCommandValue('serverparam')) || '',  
    url = utils.formatUrl(actionUrl + (actionUrl.indexOf('?') == -1 ? '?':'&') + params);  
  
fd.append(fieldName, file, file.name || ('blob.' + file.type.substr('image/'.length)));  
fd.append('type', 'ajax');  
xhr.open("post", url, true);  
xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
```

在此之后插入

```js
// <++>  
if(me.options.headers && Object.prototype.toString.apply(me.options.headers) === "[object Object]"){  
    for(var key in me.options.headers){  
        xhr.setRequestHeader(key, me.options.headers[key])  
    }  
}  
// <++>
```

得到

```js
var xhr = new XMLHttpRequest(),  
    fd = new FormData(),  
    params = utils.serializeParam(me.queryCommandValue('serverparam')) || '',  
    url = utils.formatUrl(actionUrl + (actionUrl.indexOf('?') == -1 ? '?':'&') + params);  
  
fd.append(fieldName, file, file.name || ('blob.' + file.type.substr('image/'.length)));  
fd.append('type', 'ajax');  
xhr.open("post", url, true);  
xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");  
// <++>  
if(me.options.headers && Object.prototype.toString.apply(me.options.headers) === "[object Object]"){  
    for(var key in me.options.headers){  
        xhr.setRequestHeader(key, me.options.headers[key])  
    }  
}  
// <++>  
xhr.addEventListener('load', function (e) {
```

2. 在 `ueditor.all.js` 中搜索 `UE.ajax = function() {` 在此函数中，找到以下内容

```js
var method = ajaxOpts.method.toUpperCase();  
var str = url + (url.indexOf("?")==-1?"?":"&") + (method=="POST"?"":submitStr+ "&noCache=" + +new Date);  
xhr.open(method, str, ajaxOpts.async);  
xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4) {  
        if (!timeIsOut && xhr.status == 200) {  
            ajaxOpts.onsuccess(xhr);  
        } else {  
            ajaxOpts.onerror(xhr);  
        }  
    }  
};
```

在此之后插入

```js
// <++>  
xhr.setRequestHeader("Authorization", localStorage.getItem('Token'));  
// <++>
```

得到

```js
  
var method = ajaxOpts.method.toUpperCase();  
var str = url + (url.indexOf("?")==-1?"?":"&") + (method=="POST"?"":submitStr+ "&noCache=" + +new Date);  
xhr.open(method, str, ajaxOpts.async);  
xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4) {  
        if (!timeIsOut && xhr.status == 200) {  
            ajaxOpts.onsuccess(xhr);  
        } else {  
            ajaxOpts.onerror(xhr);  
        }  
    }  
};  
// <++>  
xhr.setRequestHeader("Authorization", localStorage.getItem('Token'));  
// <++>  
if (method == "POST") {  
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');  
    xhr.send(submitStr);  
} else {  
    xhr.send(null);  
}
```