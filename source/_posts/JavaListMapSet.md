---
title: Java 集合
date: 2022-08-23 12:01:00
tags: 
 - java
 - basic
categories:
 - Java
---

# Java 集合

## 集合简介

`Java` 集合可分为 `Set` , `List` , `Queue` , `Map` 四种体系

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823000500.png)

`Java` 集合就像一种容器，可以把多个对象（实际上是对象的引用，但习惯上都称对象）放进该容器中。

从 `Java5` 增加了泛型以后， `Java` 集合可以记住容器中对象的数据类型，使得编码更加简洁、健壮。

## 集合和数组的区别

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823000659.png)

## List 集合 

### ArrayList 集合

#### ArrayList 集合的特点

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823000843.png)

#### ArrayList 实现原理

1. ArrayList 是 List 接口的可变数组非同步实现，并允许包括 null 在内的所有元素
2. 底层使用数组实现
3. 该集合是可变长度数组，数组扩容时，会将老数组中的元素重新拷贝一份到新的数组中，每次数组容量增长大约是其容量的 1.5 倍，这种操作的代价很高
4. 采用了 Fail-Fast 机制，面对并发的修改时，迭代器很快就会完全失败，而不是冒着在将来某个不确定时间发生任意不确定行为的风险
5. remove 方法会让下标到数组末尾的元素向前移动一个单位，并把最后一位的值置空，方便 GC

#### ArrayList集合的一些方法

- `.add(Object element)` 向列表的尾部添加指定的元素
- `.size()` 返回列表中的元素个数
- `.get(int index)` 返回列表中指定位置的元素，index从0开始
- `.add(int index, Object element)` 在列表的指定位置（从0开始）插入指定元素
- `.set(int i, Object element)` 使用元素 element 替换索引 i 位置的元素，并返回被替换的元素
- `.clear()` 从列表中移除所有元素
- `.isEmpty()` 判断列表是否包含元素，不包含元素则返回 true，否则返回 false
- `.contains(Object o)` 如果列表包含指定的元素，则返回 true
- `.remove(int index)` 移除列表中指定位置的元素，并返回被删除元素，删除位置后面的元素（若有）向前移动
- `.remove(Object o)` 从List集合中移除第一次出现的指定元素，移除成功返回true，否则返回false。当且仅当List集合中含有满足`(o==null ? get(i)==null : o.equals(get(i)))` 条件的最低索引i的元素时才会返回true。
- `iterator()` 返回按适当顺序在列表的元素上进行迭代的迭代器。

#### 示例

```java
public class Test {
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<String>();//<String>泛型标识集合中存储的是字符串形式的元素
		list.add("a"); // add() 用于向List集合容器中添加元素
		list.add("b");
		System.out.println(list);
		System.out.println(list.size()); // size() 用于获取集合中元素个数
		System.out.println(list.get(1)); // get() 获取指定索引（从0开始）位置的元素
	}
}
```

```java
public class Test {
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<String>();
		list.add("baizeyv");
		list.add("melody");
		System.out.println(list);
		list.add(1, "fahaxiki"); // 在指定位置添加元素，原来位置的元素后移
		System.out.println(list);
	}
}
```

```java
public class Test {
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<String>();
		list.add("baizeyv");
		list.add("melody");
		System.out.println(list);
		list.set(1, "fahaxiki"); // 替换指定位置的元素
		System.out.println(list);
	}
}
```

```java
public class Test {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>();
        list.add("baizeyv");
        list.add("melody");
        System.out.println(list);
        list.clear(); //清空List集合中的所有元素。
        System.out.println(list.isEmpty()); //集合容器没有元素，则true。
        System.out.println(list);
    }
}
```

```java
public class Test {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>();
        list.add("baizeyv");
        list.add("melody");
        boolean flag = list.contains("gc"); //用来判断集合容器中是否含有参数元素。
        System.out.println(flag);
    }
}
```

```java
public class Test {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>();
        list.add("baizeyv");
        list.add("melody");
        list.add("gc");
        System.out.println(list.remove(1)); //删除指定索引（从0开始）位置的元素，并将元素返回,并后面元素前移。
        System.out.println(list);
    }
}
```

```java
public class Test {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>();
        list.add("baizeyv");
        list.add("melody");
        list.add("melody");
        list.add("fahaxiki");
        System.out.println(list.remove("melody")); //删除List集合元素，返回boolean,并后面元素前移。
        System.out.println(list);
    }
}
```

```java
public class Test {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>();
        list.add("baizeyv");
        list.add("melody");
        Iterator<String> ite = list.iterator(); //将List集合中元素转到iterator变量中。
        while(ite.hasNext()) //hasNext()判断当前“指针”下面是否还有元素的方法，java中没有指针，这只是举个例子。
        {
            System.out.println(ite.next()); //如果“指针”下面有元素，则移动“指针”并获取相应位置的元素。
        }
    }
}
```

for循环遍历List集合
```java
public class Test {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>();
        list.add("baizeyv");
        list.add("melody");
        for (int i = 0; i < list.size(); i++) {  //用size方法获取长度。
            System.out.println(list.get(i));  //用get方法获取值。
        }
    }
}
```

加强for循环遍历List集合
```java
public class Test {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>();
        list.add("baizeyv");
        list.add("melody");
        for (int i = 0; i < list.size(); i++) {  //用size方法获取长度。
            System.out.println(list.get(i));  //用get方法获取值。
        }
    }
}
```

### LinkedList 集合

#### LinkedList 集合的特点

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823105124.png)

#### LinkedList 实现原理

1. LinkedList 是 List 接口的双向链表非同步实现，并允许包括 null 在内的所有元素
2. 底层的数据结构是基于双向链表的，该数据结构我们称为节点
3. 双向链表节点对应的类 Node 的实例，Node 中包含成员变量：prev，next，item。其中，prev 是该节点的上一个节点，next 是该节点的下一个节点，item 是该节点所包含的值
4. 它的查找是分两半查找，先判断 index 是在链表的哪一半，然后再去对应区域查找，这样最多只要遍历链表的一半节点即可找到

#### LinkedList 集合的一些方法

- `boolean add(Object element)` 将元素附加到列表的末尾
- `boolean add(int index, Object element)` 指定位置插入
- `void addFirst(E element)` 元素附加到列表的头部
- `void addLast(E element)` 元素附加到列表的尾部
- `Object get(int index)` 根据下标获取数据
- `Object getFirst()` 返回链表的第一个元素
- `Object getLast()` 返回链接列表的最后一个元素
- `boolean contains(Object element)` 如果元素存在于列表中，则返回true
- `Object set(int index, Object element)` 用于用新元素替换列表中的现有元素
- `E remove()` 删除第一个元素
- `E remove(int location)` 删除指定位置的元素
- `E removeFirst()` 删除并返回链接列表的头部一个元素
- `E removeLast()` 删除并返回链接列表的尾部一个元素
- `void clear()` 删除列表中的所有元素
- `.size()` 获取链表长度

#### 示例

```java
LinkedList<Integer> linkedList = new LinkedList<>();

linkedList.add(1);

linkedList.add(2);

linkedList.add(3);

linkedList.addFirst(4);

linkedList.addFirst(5);

linkedList.addLast(6);

linkedList.add(2,9);

System.out.println(linkedList); // [5, 4, 9, 1, 2, 3, 6]
```

```java
LinkedList<Integer> linkedList = new LinkedList<>();

linkedList.add(1);

linkedList.add(2);

linkedList.add(3);

System.out.println("获取下标为1的元素："+linkedList.get(1));

System.out.println("链表的第一个元素："+linkedList.getFirst());

System.out.println("链表的最后一个元素："+linkedList.getLast());
```

```java
LinkedList<Integer> linkedList = new LinkedList<>();

linkedList.add(1);

linkedList.add(2);

linkedList.add(3);

linkedList.add(1);

System.out.println("是否出现过元素1："+linkedList.contains(1)); // true

System.out.println("是否出现过元素4:"+linkedList.contains(4)); // false
```

```java
LinkedList<Integer> linkedList = new LinkedList<>();

linkedList.add(1);

linkedList.add(2);

linkedList.add(3);

linkedList.add(1);

linkedList.set(1,9);

System.out.println("更新过的链表："+linkedList); // [1, 9, 3, 1]
```

```java
LinkedList<Integer> linkedList = new LinkedList<>();
linkedList.add(1);
linkedList.add(2);
linkedList.add(3);
linkedList.add(1);
linkedList.remove(); //删除第一个元素
linkedList.remove(2);//删除指定位置的元素
System.out.println(linkedList);
```

```java
LinkedList<Integer> linkedList = new LinkedList<>();
linkedList.add(1);
linkedList.add(2);
linkedList.add(3);
linkedList.add(1);
linkedList.clear();
System.out.println(linkedList); // []
```

```java
LinkedList<Integer> linkedList = new LinkedList<>();
linkedList.add(1);
linkedList.add(2);
linkedList.add(3);
linkedList.add(1);
System.out.println("链表的长度："+linkedList.size()); // 4
```

### Vector 集合(不建议使用)

#### Vector 集合的特点

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823110222.png)

### List 集合总结

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823110312.png)

## Map 集合

### HashMap 集合

#### HashMap 示意图

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823110906.png)

#### HashMap 的特点

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823110937.png)

#### HashMap 实现原理

1. HashMap 是基于哈希表的 Map 接口的非同步实现，允许使用 null 值和 null 键，但不保证映射的顺序
2. 底层使用数组实现，数组中每一项是个单向链表，即数组和链表的结合体；当链表长度大于一定阈值时，链表转换为红黑树，这样减少链表查询时间
3. HashMap 在底层将 key-value 当成一个整体进行处理，这个整体就是一个 Node 对象。HashMap 底层采用一个 Node [] 数组来保存所有的 key-value 对，当需要存储一个 Node 对象时，会根据 key 的 hash 算法来决定其在数组中的存储位置，在根据 equals 方法决定其在该数组位置上的链表中的存储位置；当需要取出一个 Node 时，也会根据 key 的 hash 算法找到其在数组中的存储位置，再根据 equals 方法从该位置上的链表中取出该 Node
4. HashMap 进行数组扩容需要重新计算扩容后每个元素在数组中的位置，很耗性能
5. 采用了 Fail-Fast 机制，通过一个 modCount 值记录修改次数，对 HashMap 内容的修改都将增加这个值。迭代器初始化过程中会将这个值赋给迭代器的 expectedModCount，在迭代过程中，判断 modCount 跟 expectedModCount 是否相等，如果不相等就表示已经有其他线程修改了 Map，马上抛出异常

#### HashMap 的常用方法

- `put(K key, V value)` 将键（key）/值（value）映射存放到Map集合中
- `get(Object key)` 返回指定键所映射的值，没有该key对应的值则返回 null，即获取key对应的value
- `.size()` 返回Map集合中数据数量，准确说是返回key-value的组数
- `clear()` 清空Map集合
- `isEmpty()` 判断Map集合中是否有数据，如果没有则返回true，否则返回false
- `remove(Object key)` 删除Map集合中键为key的数据并返回其所对应value值
- `containsValue(Object value)` 判断是否含有value
- `putAll(Hashmap)` 添加另一个同一类型的map下的所有数据
- `replace(key, value)` 替换这个key的value

#### 示例

```java
public class Test {
    public static void main(String[] args) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        map.put("Tom", 100);//向HashMap中添加元素  
    }
}
```

```java
public class Test {
    public static void main(String[] args) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        map.put("Tom", 100);
        map.put("Tom", 0);
        int score = map.get("Tom");// 获取key对应的value
        System.out.println(score);// key不允许重复，若重复，则覆盖已有key的value // 0
    }
}
```

```java
public class Test {
    public static void main(String[] args) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        map.put("Tom", 100);
        map.put("Jim", 90);
        map.put("Sam", 91);
        System.out.println(map.size());
    }
}
```

```java
public class Test {
    public static void main(String[] args) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        map.put("Tom", 100);
        map.put("Jim", 90);
        map.put("Sam", 91);
        map.clear();// 清空map中的key-value
        System.out.println(map.size());
    }
}
```

```java
public class Test {
    public static void main(String[] args) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        map.put("Tom", 100);
        map.put("Jim", 90);
        map.put("Sam", 91);
        map.clear();// 清空map中的key-value
        System.out.println(map.isEmpty());
    }
}
```

```java
public class Test {
    public static void main(String[] args) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        map.put("Tom", 100);
        map.put("Jim", 90);
        map.put("Sam", 91);
        map.remove("Tom");
        System.out.println(map);
    }
}
```

```java
public class Test {
    public static void main(String[] args) {
        HashMap<String, Integer> map=new HashMap<>();
        /*boolean*///判断map中是否存在这个value
        System.out.println(map.containsValue(1));//false
        map.put("DEMO", 1);
        System.out.println(map.containsValue(1));//true
    }
}
```

```java
public class Test {
    public static void main(String[] args) {
        HashMap<String, Integer> map=new HashMap<>();
        HashMap<String, Integer> map1=new HashMap<>();
        /*void*///将同一类型的map添加到另一个map中
        map1.put("DEMO1", 1);
        map.put("DEMO2", 2);
        System.out.println(map);//{DEMO2=2}
        map.putAll(map1);
        System.out.println(map);//{DEMO1=1, DEMO2=2}
    }
}
```

```java
public class Test {
    public static void main(String[] args) {
        HashMap<String, Integer> map=new HashMap<>();
        /*value*///判断map中是否存在这个key
        map.put("DEMO1", 1);
        map.put("DEMO2", 2);
        System.out.println(map);//{DEMO1=1, DEMO2=2}
        System.out.println(map.replace("DEMO2", 1));//2
        System.out.println(map);//{DEMO1=1, DEMO2=1}
    }
}
```

### TreeMap 集合

#### TreeMap 的特点

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823112001.png)

#### TreeMap 常用方法

- `ceilingEntry(K key)` 返回指定的 Key 大于或等于的最小值的元素，如果没有，则返回 null

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		Entry entry = treeMap.ceilingEntry("2.5");
		System.out.println(entry.getKey() + "||" + entry.getValue()); // 3||demo3
		entry = treeMap.ceilingEntry("4");
		System.out.println(entry); // null
	}
}
```

- `ceilingKey(K key)` 返回指定的 Key 大于或等于的最小值的 Key，如果没有，则返回 null

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		Object obj = treeMap.ceilingKey("2.5");
		System.out.println(obj); // 3
		obj = treeMap.ceilingKey("5");
		System.out.println(obj); // null
	}
}
```

- `clone()` 返回集合的副本

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		Object obj = treeMap.clone();
		System.out.println("treeMap集合中所有的元素：" + treeMap);
		System.out.println("treeMap的副本：" + obj);
		TreeMap tm = (TreeMap)obj;
		System.out.println(tm.get("2"));
	}
}
```

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823113208.png)

- `public Comparator super<K> comparator()` 如果使用默认的比较器，就返回 null，如果使用其他的比较器，则返回比较器的哈希码值

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap1 = new TreeMap();
		TreeMap treeMap2 = new TreeMap();
		treeMap1.put("1", "demo1");
		treeMap1.put("2", "demo2");
		treeMap2.put("a", "demo1");
		treeMap2.put("b", "demo2");
		System.out.println("treeMap1集合中所有的元素:" + treeMap1);
		System.out.println("treeMap2集合中所有的元素:" + treeMap2);
		Comparator comparator = treeMap1.comparator();
		System.out.println(comparator);
		comparator = treeMap2.comparator();
		System.out.println(comparator);
		TreeMap tm = new TreeMap(new Comparator<String>() {
			public int compare(String o1, String o2) {
				return o2.compareTo(o1); // 正负代表大小
			}
		});
		tm.put("1", "test1");
		tm.put("2", "test2");
		tm.put("3", "test3");
		System.out.println("tm集合中所有的元素：" + tm);
		System.out.println(tm.comparator());
	}
}
```

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823114134.png)

- `descendingKeySet()` 返回集合的全部 Key，并且是逆序的

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		Set set = treeMap.descendingKeySet();
		System.out.println(set);
	}
}
```

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823114403.png)

- `descendingMap()` 把集合逆序返回

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		System.out.println(treeMap);
		Map map = treeMap.descendingMap();
		System.out.println(map);
	}
}
```

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823114619.png)

- `firstEntry()` 返回集合中最小 Key 的元素

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		Entry entry = treeMap.firstEntry();
		System.out.println(entry); // 1=demo1
	}
}
```

- `firstKey()` 返回集合中最小 Key 的 key

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		Object key = treeMap.firstKey();
		System.out.println(key); // 1
	}
}
```

- `floorEntry(K key)` 返回小于等于 key 的最大 Key 的元素

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		Entry entry = treeMap.floorEntry("2.5");
		System.out.println(entry); // 2=demo2
	}
}
```

- `floorKey(K key)` 返回小于等于 key 的最大 Key 的 key

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		Object key = treeMap.floorKey("2.5");
		System.out.println(key); // 2
	}
}
```

- `headMap(K toKey)` 返回 Key 小于 toKey 的所有元素

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		Map map = treeMap.headMap("2.5");
		System.out.println(map);
	}
}
```

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823115629.png)

- `headMap(K toKey, boolean inclusive)` 当 inclusive 为 true 时，就是返回 Key 小于等于 toKey 的所有元素

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		Map map = treeMap.headMap("2", true);
		System.out.println(map);
		map = treeMap.headMap("2", false);
		System.out.println(map);
	}
}
```

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823115830.png)

- `higherEntry(K key)` 返回 Key 大于 key 的所有元素

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		Entry entry = treeMap.higherEntry("2.5");
		System.out.println(entry); // 3=demo3
		entry = treeMap.higherEntry("2");
		System.out.println(entry); // 3=demo3
	}
}
```

- `higherKey(K key)` 返回 Key 大于 key 的所有 Key

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		Object key = treeMap.higherKey("2.5");
		System.out.println(key); // 3
		key = treeMap.higherKey("2");
		System.out.println(key); // 3
	}
}
```

- `lastEntry()` 返回 Key 最大的元素

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		Entry entry = treeMap.lastEntry();
		System.out.println(entry); // 3=demo3
	}
}
```

- `lastKey()` 返回 Key 最大的 Key

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		Object key = treeMap.lastKey();
		System.out.println(key); // 3
	}
}
```

- `lowerEntry(K key)` 

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		Entry entry = treeMap.lowerEntry("2");
		System.out.println(entry); // 1=demo1
		entry = treeMap.lowerEntry("2.5");
		System.out.println(entry); // 2=demo2
	}
}
```

- `lowerKey(K key)` 返回小于 key 最大的 Key

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		Object key = treeMap.lowerKey("2");
		System.out.println(key); // 1
		key = treeMap.lowerKey("2.5");
		System.out.println(key); // 2
	}
}
```

- `pollFirstEntry()` 删除 key 最小的元素

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		System.out.println("删除前treeMap集合中所有的元素：" + treeMap);
		treeMap.pollFirstEntry();
		System.out.println("删除后treeMap集合中所有的元素：" + treeMap);
	}
}
```

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823121055.png)

- `pollLastEntry()` 删除最大 Key 的元素

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		System.out.println("删除前treeMap集合中所有的元素：" + treeMap);
		treeMap.pollLastEntry();
		System.out.println("删除后treeMap集合中所有的元素：" + treeMap);
	}
}
```

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823121232.png)

- `subMap(K fromKey, boolean fromInclusive, K toKey, boolean toInclusive)` 截取集合中 Key 从 fromKey 到 toKey 的元素，否是截取他们本身，取决于 true 或者 false

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		treeMap.put("4", "demo4");
		Map map = treeMap.subMap("1", true, "4", true);
		System.out.println(map);
		map = treeMap.subMap("1", false, "4", true);
		System.out.println(map);
		map = treeMap.subMap("1", false, "4", false);
		System.out.println(map);
	}
}
```

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823121531.png)

- `subMap(K fromKey, K toKey)` 截取集合中 Key 从 fromKey 到 toKey 的元素，包括 fromKey，不包括 toKey

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		treeMap.put("4", "demo4");
		Map map = treeMap.subMap("1", "4");
		System.out.println(map);
		map = treeMap.subMap("2", "3");
		System.out.println(map);
		map = treeMap.subMap("3", "3");
		System.out.println(map);
	}
}
```

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823121754.png)

- `tailMap(K fromKey)` 截取 Key 大于等于 fromKey 的所有元素

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		treeMap.put("4", "demo4");
		Map map = treeMap.tailMap("2");
		System.out.println(map);
	}
}
```

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823121928.png)

- `tailMap(K fromKey, boolean inclusive)` 当 inclusive 为 true 时，截取 Key 大于等于 fromKey 的所有元素，否则截取 Key 大于 fromKey 的所有元素

```java
public class Test {
	public static void main(String[] args) {
		TreeMap treeMap = new TreeMap();
		treeMap.put("1", "demo1");
		treeMap.put("2", "demo2");
		treeMap.put("3", "demo3");
		treeMap.put("4", "demo4");
		Map map = treeMap.tailMap("2", true);
		System.out.println(map);
		map = treeMap.tailMap("2", false);
		System.out.println(map);
	}
}
```

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823122128.png)

## Set 集合

### HashSet 集合

#### HashSet 的特点

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823130357.png)

#### HashSet 实现原理

1. HashSet 由哈希表 (实际上是一个 HashMap 实例) 支持，不保证 set 的迭代顺序，并允许使用 null 元素
2. 基于 HashMap 实现，API 也是对 HashMap 的行为进行了封装，可参考 HashMap

#### HashSet 常用方法

- `add(Object o)` 向Set集合中添加元素，不允许添加重复数据
- `size()` 返回Set集合中的元素个数

```java
public class Test {
    public static void main(String[] args) {
        HashSet<String> set = new HashSet<String>(); //调用HashSet无参构造方法——>创建HashMap对象并给map全局变量。
        set.add("melody");
        set.add("baizeyv");
        System.out.println(set);
        System.out.println(set.size());
    }
}
```

> 注意：
> 
>> 不会按照保存的顺序存储数据（顺序不定），遍历时不能保证下次结果和上次相同
>> 
>> 且向HashSet集合中添加元素，HashSet add方法实质是map全局变量调用了put方法，将数据存到了key，因为HashMap的 key不允许，所以HashSet添加的元素也不允许重复

- `.remove(Object o)` 删除Set集合中的obj对象，删除成功返回true，否则返回false
- `.isEmpty()` 如果Set不包含元素，则返回 true

```java
public class Test {
    public static void main(String[] args) {
        HashSet<String> set = new HashSet<String>();
        set.add("baizeyv");
        set.add("melody");
        System.out.println(set.isEmpty());
        System.out.println(set.remove("melody"));
        System.out.println(set);
    }
}
```

- `.clear()` 移除此Set中的所有元素

```java
public class Test {
    public static void main(String[] args) {
        HashSet<String> set = new HashSet<String>();
        set.add("melody");
        set.add("fahaxiki");
        System.out.println(set);
        set.clear();
        System.out.println(set);
    }
}
```

- `.iterator()` 返回在此Set中的元素上进行迭代的迭代器

```java
public static void main(String[] args) {
    HashSet<String> set = new HashSet<String>();
    set.add("baizeyv");
    set.add("melody");
    Iterator<String> ite =set.iterator();
    while(ite.hasNext())
    {
        System.out.println(ite.next());
    }
}
```

- `.contains(Object o)` 判断集合中是否包含obj元素

```java
public class Test {
    public static void main(String[] args) {
        HashSet<String> set = new HashSet<String>();
        set.add("melody");
        set.add("baizeyv");
        System.out.println(set.contains("melody"));
    }
}
```

加强for循环遍历 `Set` 集合

```java
⑧：加强for循环遍历Set集合：
public class Test {
    public static void main(String[] args) {
        HashSet<String> set = new HashSet<String>();
        set.add("melody");
        set.add("fahaxiki");
        for (String name : set) {   //使用foreach进行遍历。
            System.out.println(name);
        }
    }
}
```

### LinkedHashSet 集合

#### LinkedHashSet 集合的特点

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823131025.png)

### TreeSet 集合

#### TreeSet 集合的特点

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823131100.png)

#### TreeSet 集合基本使用

- 插入是按字典序排序的

```java
public class Test {
    public static void main(String[] args) {
        TreeSet ts=new TreeSet();
        ts.add("agg");
        ts.add("abcd");
        ts.add("ffas");
        Iterator it=ts.iterator();
        while(it.hasNext()) {
            System.out.println(it.next());
        }
    }
}
```

输出： 按照字典序排序的方式进行排序

```txt
abcd
agg
ffas
```

- 如果插入的是自定义对象 需要让类实现 Comparable 接口并且必须要重写compareTo

```java
class Person implements Comparable{
    
    String name;
    int age;
    Person(String name,int age)
    {
        this.name=name;
        this.age=age;
    }

    @Override
    public int compareTo(Object o) {
        Person p=(Person)o;
        //先对姓名字典序比较 如果相同 比较年龄
        if(this.name.compareTo(p.name)!=0) {
            return this.name.compareTo(p.name);
        }
        else
        {
            if(this.age>p.age) return 1;
            else if(this.age<p.age) return -1;
        }
        return 0;
    }

}

public class Test {
    public static void main(String args[])
    {
        TreeSet ts=new TreeSet();
        ts.add(new Person("agg",21));
        ts.add(new Person("abcd",12));
        ts.add(new Person("ffas",8));
        ts.add(new Person("agg",12));
        Iterator it=ts.iterator();
        while(it.hasNext())
        {
            Person p=(Person)it.next();
            System.out.println(p.name+":"+p.age);
        }
    }
}
```

输出：

```txt
abcd:12
agg:12
agg:21
ffas:8
```

### HashSet, LinkedHashSet, TreeSet使用场景

**HashSet:** HashSet的性能基本上比LinkedHashSet和TreeSet要好，特别是添加和查询，这也是用的最多的两个操作

**LinkedHashSet:** LinkedHashSet的查询稍慢一些，但是他可以维持元素的添加顺序。所以只有要求当插入顺序和取出顺序一致的时候 才使用LinkedHashSet

**TreeSet:** 只有在需要对元素进行排序时使用

### List 和 Set 集合的区别

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823131525.png)