---
title: Java Thread Pool
date: 2022-08-24 13:01:00
tags: 
 - java
 - thread
categories:
 - Java
---

# Java 线程池

## 线程池概念

创建 `Java` 线程需要给线程分配 *堆栈* 内存以及初始化内存，还需要进行系统调用，

频繁的创建和销毁线程会大大降低系统的运行效率，

采用线程池来管理线程有以下好处：

> 1. 提升性能：线程池能独立负责线程的创建、维护和分配
> 2. 线程管理：每个 `Java` 线程池会保持一些基本的线程统计信息，对线程进行有效管理

总结为以下优点：

> 1. ***降低资源消耗*** : 通过重复利用已创建的线程降低线程创建和销毁造成的消耗
> 2. ***提高相应速度*** : 当任务到达时，任务可以不需要的等到线程创建就能立即执行
> 3. ***提高线程的可管理性*** : 线程是稀缺资源，如果无限制的创建，不仅会消耗系统资源，还会降低系统的稳定性，使用线程池可以进行统一的分配，调优和监控

## Executor 框架

### Executor 简介

`Executor` 框架是 `Java5` 之后引进的，在 `Java5` 之后，通过Executor来启动线程比使用 `Thread` 的start方法更好，除了更容易管理，效率更好（用线程池实现，节约开销）外，还有关键的一点：**有助于避免this逃逸问题**

> 补充：
> 
>> `this` 逃逸：是指在构造函数返回之前，其他线程就持有该对象的引用。调用尚未构造完全的对象的方法可能引发令人疑惑的错误。

`Executor` 框架不仅包括了线程池的管理，还提供了线程工厂、队列以及拒绝策略等，`Executor` 框架让并发编程更简单。

### Executor 框架结构（三大部分组成）

#### 任务(Runnable / Callable)

**执行任务需要实现 `Runnable` 接口 或 `Callable` 接口。**

`Runnable` 接口 或 `Callable` 接口实现类都可以被 `ThreadPollExecutor` 或 `ScheduledThreadPoolExecutor` 执行

#### 任务的执行(Executor)

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823181040.png)

如图所示：

包括 任务执行机制的核心接口 `Executor` 

以及 继承自 `Executor` 接口的 `ExecutorService` 接口

`ThreadPoolExecutor` 和 `ScheduledThreadPoolExecutor` 这两个关键类实现了 `ExecutorService` 接口

> 这里提了很多底层的类关系，但是，实际上我们需要更多关注的是 `ThreadPoolExecutor` 这个类
> 
> 这个类在我们实际使用线程池的过程中，使用频率很高

> 注意：
> 
>> 通过查看 `ScheduledThreadPoolExecutor` 源代码我们发现 `ScheduledThreadPoolExecutor` 实际上是继承了 `ThreadPoolExecutor`
>> 
>> 并实现了 `ScheduledExecutorService` ,
>> 
>> 而 `ScheduledExecutorSevice` 又实现了 `ExecutorService`

`ThreadPoolExecutor` 类描述：

```java
// AbstractExecutorService 实现了 ExecutorService 接口
public class ThreadPoolExecutor extends AbstractExecutorService
```

`ScheduledThreadPoolExecutor` 类描述：

```java
// ScheduledexecutorService 实现了 ExecutorService 接口
public class ScheduledThreadPoolExecutor extends ThreadPoolExecutor implements ScheduledExecutorService
```

#### 异步计算的结果(Future)

`Future` 接口以及 `Future` 接口的实现类 `FutureTask` 类都可以代表异步计算的结果。

当我们把 `Runnable` 接口 或 `Callable` 接口的实现类提交给 `ThreadPoolExecutor` 或 `ScheduledThreadPoolExecutor` 执行。（调用 `submit()` 方法时会返回一个 `FutureTask` 对象）

### Executor 框架的使用示意图

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220823182630.png)

1. 主线程首先要创建实现 `Runnable` 或 `Callable` 接口的任务对象
2. 把创建完成的实现 `Runnable` / `Callable` 接口的对象直接交给 `ExecutorService` 执行：`ExecutorService.execute(Runnable command)` ；或者也可以把 `Runnable` 对象或 `Callable` 对象提交给 `ExecutorService` 执行：`ExecutorService.submit(Runnable task)` 或 `ExecutorService.submit(Callable<T> task)`
3. 如果执行 `ExecutorService.submit(...)` , `ExecutorService` 将返回一个实现 `Future` 接口的对象
4. 我们刚才也提到了，执行 `execute()` 方法和 `submit()` 方法的区别， `submit()` 会返回一个 `FutureTask` 对象。由于 `FutureTask` 实现了 `Runnable` ，我们也可以创建 `FutureTask` ，然后交给 `ExecutorService` 执行
5. 最后，主线程可以执行 `FutureTask.get()` 方法来等待任务执行完成。主线程也可以执行 `FutureTask.cancel(boolean mayInterruptIfRunning)` 来取消执行此任务的执行

> `execute()` 与 `submit()` 的区别
> 
>> - `execute()` 方法入参仅为 `Runnable` 类型
>> - `submit()` 方法入参可为 `Runnable` 类型，也可为 `Callable` 类型
>> - `Runnable` 的 `run()` 方法没有返回值， `Callable` 的 `call()` 方法可以返回，可知 `execute()` 方法没有返回值，`submit()` 方法又返回值
>> - `execute()` 方法的异常只能使用 `try-catch` 捕获
>> - `submit()` 方法不管提交的是 `Runnable` 还是 `Callable` 类型的任务，如果不对返回值 `Future` 调用 `get()` 方法，异常都会吃掉

## ThreadPoolExecutor 类简单介绍

**线程池实现类 `ThreadPoolExecutor` 是 `Executor` 框架最核心的类**

### ThreadPoolExecutor 类分析

`ThreadPoolExecutor` 类中提供了四个构造方法。以下看最长的一个，其余三个都是以此为基础产生（给定某些默认参数的构造方法）

```java
/**
 * 用给定的初始参数创建一个新的ThreadPoolExecutor。
 */
public ThreadPoolExecutor(int corePoolSize,//线程池的核心线程数量
	  int maximumPoolSize,//线程池的最大线程数
	  long keepAliveTime,//当线程数大于核心线程数时，多余的空闲线程存活的最长时间
	  TimeUnit unit,//时间单位
	  BlockingQueue<Runnable> workQueue,//任务队列，用来储存等待执行任务的队列
	  ThreadFactory threadFactory,//线程工厂，用来创建线程，一般默认即可
	  RejectedExecutionHandler handler//拒绝策略，当提交的任务过多而不能及时处理时，我们可以定制策略来处理任务
   ) {
	if (corePoolSize < 0 ||
		maximumPoolSize <= 0 ||
		maximumPoolSize < corePoolSize ||
		keepAliveTime < 0)
		thrownew IllegalArgumentException();
	if (workQueue == null || threadFactory == null || handler == null)
		thrownew NullPointerException();
	this.corePoolSize = corePoolSize;
	this.maximumPoolSize = maximumPoolSize;
	this.workQueue = workQueue;
	this.keepAliveTime = unit.toNanos(keepAliveTime);
	this.threadFactory = threadFactory;
	this.handler = handler;
}
```

**`ThreadPoolExecutor` 3个最重要的参数:**
- `corePoolSize`: 核心线程数定义了最小可以同时运行的线程的数量
- `maximumPoolSize`: 当队列中存放的任务达到队列容量的时候，当前可以同时运行的线程数量变为最大线程数
- `workQueue`: 当新任务来的时候会先判断当前运行的线程数量是否达到核心线程数，如果达到的话，新人就会被存放在队列中

**`ThreadPoolExecutor` 其他常见参数:**
- `keepAliveTime`: 当线程池中的线程数量大于 `corePoolSize` 的时候，如果这时没有新的任务提交，核心线程外的线程不会立即销毁，而是会等待，直到等待的时候超过了 `keepAliveTime` 才会被回收销毁
- `unit`: `keepAliveTime` 参数的时间单位
- `threadFactory`: `executor` 创建新线程的时候会用到
- `handler`: 饱和策略

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220824094858.png)

`ThreadPoolExecutor` 饱和策略定义：

如果当前同时运行的线程数量达到最大线程数量并且队列也已经被放满了时， `ThreadPoolTaskExecutor` 定义了一些策略：

- `ThreadPoolExecutor.AbortPolicy` : 抛出 `RejectedExecutionException` 来拒绝新任务的处理
- `ThreadPoolExecutor.CallerRunsPolicy` : 调用执行自己的线程运行任务。您不会任务请求。但是这种策略会降低对于新任务的提交速度，影响程序的整体性能。另外，这个策略喜欢增加队列容量。如果您的应用程序可以承受此延迟并且不能丢且任何一个任务请求的话，可以选择这个策略。
- `ThreadPoolExecutor.DiscardPolicy` : 不处理新任务，直接丢弃掉
- `ThreadPoolExecutor.DiscardOldestPolicy` : 此策略将丢弃最早的未处理的任务请求

例子：

> `Spring` 通过 `ThreadPoolTaskExecutor` 或者直接通过 `ThreadPoolExecutor` 的构造函数创建线程池的时候，
> 
> 当我们不指定 `RejectedExecutionHandler` 饱和策略的话来配置线程池的时候，默认使用的是 `ThreadPoolExecutor.AbortPolicy`
> 
> 在默认情况下， `ThreadPoolExecutor` 将抛出 `RejectedExecutionException` 来拒绝信赖的任务，这代表将丢失对这个任务的处理
> 
> 对于可伸缩的应用程序，建议使用 `ThreadPoolExecutor.CallerRunsPolicy`，当最大池被填满时，此策略为我们他提供了可伸缩队列

### ThreadPoolExecutor 构造函数创建线程池

线程资源必须通过线程池提供，不允许在应用中自行显式创建线程。

> 使用线程池的好处是减少在创建和销毁线程上所小寒的时间以及系统资源开销，解决资源不足的问题
> 
> 如果不使用线程池，有可能会造成系统创建大量同类线程而导致消耗完内存或者“过度切换”的问题

强制线程池不允许使用 `Executors` 创建，而是通过 `ThreadPoolExecutor` 构造函数的方式，这样的处理方式更加明确线程池的运行规则，规避资源耗尽的风险

> `Executors` 返回线程池对象的弊端如下：
> - `FixedThreadPool` 和 `SingleThreadExecutor` 允许请求的队列长度为 `Integer.MAX_VALUE` 可能堆积大量的请求，从而导致 OOM
> - `CachedThreadPool` 和 `ScheduledThreadPool` 允许创建的线程数量为 `Integer.MAX_VALUE` 可能会创建大量线程，从而导致 OOM

#### 方式一：通过 ThreadPoolExecutor 构造函数实现（推荐）

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220824100909.png)

#### 方式二：通过 Executor 框架的工具类 Executors 来实现

可以创建三种类型的 `ThreadPoolExecutor`:
- `FixedThreadPool`
- `SingleThreadExecutor`
- `CachedThreadPool`

对应 `Executors` 工具类中的方法如图：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220824101110.png)

## ThreadPoolExecutor 使用示例

### 示例代码 Runnable + ThreadPoolExecutor

首先创建一个 `Runnable` 接口的实现类，也可以是 `Callable` 接口

`MyRunnable.java`

```java
/**
 * 这是简单的 Runnable 类，需要大约5s来执行其任务
 */
public class MyRunnable implements Runnable {
	private String command;

	public MyRunnable(String s) {
		this.command = s;
	}

	private void processCommand() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		System.out.println(Thread.currentThread().getName() + "Start.Time" + new Date());
		processCommand();
		System.out.println(Thread.currentThread().getName() + "End.Time" + new Date());
	}

	@Override
	public String toString() {
		return this.command;
	}
}
```

`ThreadPoolExecutorDemo.java`

```java
public class ThreadPoolExecutorDemo {
	private static final int CORE_POOL_SIZE = 5;
	private static final int MAX_POOL_SIZE = 10;
	private static final int QUEUE_CAPACITY = 100;
	private static final Long KEEP_ALIVE_TIME = 1L;
	public static void main(String[] args) {
		// 通过 ThreadPoolExecutor 构造函数自定义参数创建
		ThreadPoolExecutor executor = new ThreadPoolExecutor(
			CORE_POOL_SIZE,
			MAX_POOL_SIZE,
			KEEP_ALIVE_TIME,
			TimeUnit.SECONDS,
			new ArrayBlockingQueue<>(QUEUE_CAPACITY),
			new ThreadPoolExecutor.CallerRunsPolicy()
		);
		for(int i = 0; i < 10; i++) {
			// 创建 worker Thread 对象（worker Thread类实现了Runnable接口）
			Runnable worker = new MyRunnable("" + i);
			// 执行 Runnable
			executor.execute(worker);
		}
		// 终止线程池
		executor.shutdown();
		while(!executor.isTerminated()) {
			
		}
		System.out.println("Finished All Threads");
	}
}
```

输出：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220824103243.png)

### 线程池原理分析

通过以上输出结果可以看到：*线程池每次会同时执行5个任务，这5个任务执行完之后，剩余的5个任务才会被执行*

**为了搞懂线程池原理，我们首先需要分析 `execute` 方法**，以下为其源码

```java
	// 存放线程池的运行状态 (runState) 和线程池内有效线程的数量 (workerCount)
	private final AtomicInteger ctl = new AtomicInteger(ctlOf(RUNNING, 0));

    private static int workerCountOf(int c) {
        return c & CAPACITY;
    }

    private final BlockingQueue<Runnable> workQueue;

    public void execute(Runnable command) {
        // 如果任务为null，则抛出异常。
        if (command == null)
            throw new NullPointerException();
        // ctl 中保存的线程池当前的一些状态信息
        int c = ctl.get();

        //  下面会涉及到 3 步 操作
        // 1.首先判断当前线程池中执行的任务数量是否小于 corePoolSize
        // 如果小于的话，通过addWorker(command, true)新建一个线程，并将任务(command)添加到该线程中；然后，启动该线程从而执行任务。
        if (workerCountOf(c) < corePoolSize) {
            if (addWorker(command, true))
                return;
            c = ctl.get();
        }
        // 2.如果当前执行的任务数量大于等于 corePoolSize 的时候就会走到这里
        // 通过 isRunning 方法判断线程池状态，线程池处于 RUNNING 状态并且队列可以加入任务，该任务才会被加入进去
        if (isRunning(c) && workQueue.offer(command)) {
            int recheck = ctl.get();
            // 再次获取线程池状态，如果线程池状态不是 RUNNING 状态就需要从任务队列中移除任务，并尝试判断线程是否全部执行完毕。同时执行拒绝策略。
            if (!isRunning(recheck) && remove(command))
                reject(command);
                // 如果当前线程池为空就新创建一个线程并执行。
            elseif (workerCountOf(recheck) == 0)
                addWorker(null, false);
        }
        //3. 通过addWorker(command, false)新建一个线程，并将任务(command)添加到该线程中；然后，启动该线程从而执行任务。
        //如果addWorker(command, false)执行失败，则通过reject()执行相应的拒绝策略的内容。
        elseif (!addWorker(command, false))
            reject(command);
    }
```

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220824104747.png)

### 示例代码 Callable + ThreadPoolExecutor

`MyCallable.java`

```java
public class MyCallable implements Callable<String> {
    @Override
    public String call() throws Exception {
        Thread.sleep(1000);
        //返回执行当前 Callable 的线程名字
        return Thread.currentThread().getName();
    }
}
```

`CallableDemo.java`

```java
public class CallableDemo {

    private static final int CORE_POOL_SIZE = 5;
    private static final int MAX_POOL_SIZE = 10;
    private static final int QUEUE_CAPACITY = 100;
    private static final Long KEEP_ALIVE_TIME = 1L;

    public static void main(String[] args) {

        //使用阿里巴巴推荐的创建线程池的方式
        //通过ThreadPoolExecutor构造函数自定义参数创建
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                CORE_POOL_SIZE,
                MAX_POOL_SIZE,
                KEEP_ALIVE_TIME,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(QUEUE_CAPACITY),
                new ThreadPoolExecutor.CallerRunsPolicy());

        List<Future<String>> futureList = new ArrayList<>();
        Callable<String> callable = new MyCallable();
        for (int i = 0; i < 10; i++) {
            //提交任务到线程池
            Future<String> future = executor.submit(callable);
            //将返回值 future 添加到 list，我们可以通过 future 获得 执行 Callable 得到的返回值
            futureList.add(future);
        }
        for (Future<String> fut : futureList) {
            try {
                System.out.println(new Date() + "::" + fut.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        //关闭线程池
        executor.shutdown();
    }
}
```

输出：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220824110738.png)

### 常见对比

#### Runnable vs Callable

`Runnable` 自 `Java1.0` 以来一致存在

`Callable` 在 `Java1.5` 中引入，目的就是为了来处理 `Runnable` 不支持的用例。

`Runnable` 接口不会返回结果或抛出检查异常，但是 `Callable` 接口可以

如果任务不需要返回结果或抛出异常推荐使用 `Runnable` 接口

---

工具类 `Executors` 可以实现 `Runnable` 对象和 `Callable` 对象之间的相互转换。

(`Executors.callable(Runnable task)`) 或 (`Executors.callable(Runnable task, Object result)`)

---

`Runnable.java` 

```java
@FunctionalInterface
publicinterface Runnable {
   /**
    * 被线程执行，没有返回值也无法抛出异常
    */
    public abstract void run();
}
```

`Callable.java` 

```java
@FunctionalInterface
publicinterface Callable<V> {
    /**
     * 计算结果，或在无法这样做时抛出异常。
     * @return 计算得出的结果
     * @throws 如果无法计算结果，则抛出异常
     */
    V call() throws Exception;
}
```

#### execute() vs submit()

1. `execute()` 方法用户提交不需要返回值的任务，所以无法判断任务是否被线程池执行成功与否
2. `submit()` 方法用于提交需要返回值的任务。线程池会返回一个 `Future` 类型的对象，通过这个 `Future` 对象可以判断任务是否执行成功。并且可以通过 `Future` 的 `get()` 方法来获取返回值，`get()` 方法会阻塞当前线程直到任务完成，而使用 `get(long time out, TimeUnit unit)` 方法则会阻塞当前线程一段时候后立即返回，这时候有可能任务没有执行完

`AbstractExecutorService` 接口中 `submit` 方法源码：

```java
public Future<?> submit(Runnable task) {
	if (task == null) throw new NullPointerException();
	RunnableFuture<Void> ftask = newTaskFor(task, null);
	execute(ftask);
	return ftask;
}
```

上面方法调用的 `newTaskFor` 方法返回了一个 `FutureTask` 对象

```java
protected <T> RunnableFuture<T> newTaskFor(Runnable runnable, T value) {
	return new FutureTask<T>(runnable, value);
}
```

#### shutdown() vs shutdownNow()

- `shutdown()`: 关闭线程池，线程池的状态变为 `SHUTDOWN`。线程池不再接受新任务了，但是队列里的任务需要执行完毕。
- `shutdownNow()`: 关闭线程池，线程池的状态变为 `STOP`。线程池会终止当前正在运行的任务，并停止处理排队的任务并返回正在等待执行的List

#### isTerminated() vs isShutdown()

- `isShutDown` 当调用 `shutdown()` 方法后返回为true
- `isTerminated` 当调用 `shutdown()` 方法后，并且所有提交的任务完成后返回为true

## 常见的线程池详解

### FixedThreadPool

#### 简介

`FixedThreadPool` 被称为可重用固定线程数的线程池，源码如下：

```java
/**
     * 创建一个可重用固定数量线程的线程池
     */
    public static ExecutorService newFixedThreadPool(int nThreads, ThreadFactory threadFactory) {
        return new ThreadPoolExecutor(nThreads, nThreads,
			  0L, TimeUnit.MILLISECONDS,
			  new LinkedBlockingQueue<Runnable>(),
			  threadFactory);
    }
```

另外还有一个 `FixedThreadPool` 的实现方法，和上面类似

```java
public static ExecutorService newFixedThreadPool(int nThreads) {
        return new ThreadPoolExecutor(nThreads, nThreads,
			  0L, TimeUnit.MILLISECONDS,
			  new LinkedBlockingQueue<Runnable>());
}
```

从源码中可以看出新创建的 `FixedThreadPool` 的 `corePoolSize` 和 `maximumPoolSize` 都被设置为 `nThreads`，这个 `nThreads` 参数是我们使用的时候自己传递的。

#### 执行任务过程介绍

`FixedThreadPool` 的 `execute()` 方法运行示意图如下：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220824112501.png)

**上图说明：**

1. 如果当前运行的线程数小于 `corePoolSize` ，如果再在新任务的话，就创建新的线程来执行任务
2. 当前运行的线程数等于 `corePoolSize` 后，如果再来新的任务，会将任务加入 `LinkedBlockingQueue`
3. 线程池中的线程执行完手头的任务后，会在循环中反复从 `LinkedBlockingQueue` 中获取任务来执行

#### 不推荐使用 FixedThreadPool 原因

`FixedThreadPool` 使用无界队列 `LinkedBlockingQueue` (队列的容量为 `Integer.MAX_VALUE`)作为线程池的工作队列会对线程池有如下影响：

1. 当线程池中的线程数达到 `corePoolSize` h偶，新任务将在无界队列中等待，因此线程池中的线程数不会超过 `corePoolSize`
2. 由于使用无界队列时 `maximumPoolSize` 将会是一个无效参数，因为不可能存在任务队列满的情况。所以，通过创建 `FixedThreadPool` 的源码可以看出创建的 `FixedThreadPool` 的 `corePoolSize` 和 `maximumPoolSize` 被设置为同一个值
3. 由于以上两条，使用无界队列时， `keepAliveTime` 将会是一个无效参数
4. 运行中的 `FixedThreadPool` (未执行 `shutdown()` 或 `shutdownNow()`) 不会拒绝任务，再任务比较多的时候会导致 OOM(内存溢出)

### SingleThreadExecutor

#### 简介

`SingleThreadExecutor` 是只有一个线程的线程池，实现源码如下：

```java
/**
     *返回只有一个线程的线程池
     */
    public static ExecutorService newSingleThreadExecutor(ThreadFactory threadFactory) {
        return new FinalizableDelegatedExecutorService
            (new ThreadPoolExecutor(1, 1,
                                    0L, TimeUnit.MILLISECONDS,
                                    new LinkedBlockingQueue<Runnable>(),
                                    threadFactory));
    }
```

```java
public static ExecutorService newSingleThreadExecutor() {
        return new FinalizableDelegatedExecutorService
            (new ThreadPoolExecutor(1, 1,
			0L, TimeUnit.MILLISECONDS,
			new LinkedBlockingQueue<Runnable>()));
}
```

从以上源码可以看出新创建的 `SingleThreadExecutor` 的 `corePoolSize` 和 `mmaximumPoolSize` 都被设置为1，其他参数和 `FixedThreadPool` 相同

#### 执行任务过程介绍

`SingleThreadExecutor` 的运行示意图如下：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220824113619.png)

**上图说明：**
1. 如果当前运行的线程数量小于 `corePoolSize`，则会创建一个新的线程执行任务
2. 当前线程池中有一个运行的线程后，将任务加入 `LinkedBlockingQueue`
3. 线程执行完当前的任务后，会在循环中反复从 `LinkedBlockingQueue` 中获取任务来执行

#### 不推荐 SingleThreadExecutor 原因

`SingleThreadExecutor` 使用无界队列 `LinkedBlockingQueue` 作为线程池的工作队列（队列的容量为 `Integer.MAX_VALUE`）

`SingleThreadExecutor` 使用无界队列作为线程池的工作队列会对线程池带来的影响与 `FixedThreadPool` 相同，简单说就是可能会导致 OOM

### CachedThreadPool

#### 简介

`CachedThreadPool` 是一个会根据需要创建新线程的线程池，实现源码如下：

```java
/**
     * 创建一个线程池，根据需要创建新线程，但会在先前构建的线程可用时重用它。
     */
    public static ExecutorService newCachedThreadPool(ThreadFactory threadFactory) {
        return new ThreadPoolExecutor(0, Integer.MAX_VALUE,
                                      60L, TimeUnit.SECONDS,
                                      new SynchronousQueue<Runnable>(),
                                      threadFactory);
    }
```

```java
public static ExecutorService newCachedThreadPool() {
	return new ThreadPoolExecutor(0, Integer.MAX_VALUE,
		  60L, TimeUnit.SECONDS,
		  new SynchronousQueue<Runnable>());
}
```

从以上源码得出：

`CachedThreadPool` 的 `corePoolSize` 被设置为空（0）， `maximumPoolSize` 被设置为 `Integer.MAX_VALUE`，即它是无界的，

这就意味着如果主线程提交任务的速度高于 `maximumPool` 中线程处理任务的速度时， `CachedThreadPool` 会不断创建新的线程。

极端情况下，这样会导致耗尽 CPU 和内存资源

#### 执行任务过程介绍

`CachedThreadPool` 的 `execute()` 方法执行示意图如下：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220824114622.png)

**上图说明：**
1. 首先执行 `SynchronousQueue.offer(Runnable task)` 提交任务到任务队列。如果当前 `maximumPool` 中有空闲线程正在执行 `SynchronousQueue.poll(keepAliveTime, TimeUnit.NANOSECONDS)`,那么主线程执行offer操作与空闲线程执行的poll操作配对成功，主线程把任务交给空闲线程执行，`execute()` 方法执行完成，否则执行以下操作
2. 当初始 `maximumPool` 为空，或者 `maximumPool` 中没有空闲线程时，将没有线程执行 `SynchronousQueue.poll(keepAliveTime, TimeUnit.NANOSECONDS)` 。这种情况下，步骤1将失败，此时 `CachedThreadPool` 会创建新线程执行任务， `execute`方法执行完成

#### 不推荐 CachedThreadPool 原因

`CachedThreadPool` 允许创建的线程数量为 `Integer.MAX_VALUE` ，可能会创建大量线程，从而导致 OOM

## ScheduledThreadPoolExecutor 详解

`ScheduledThreadPoolExecutor` 主要用来在给定的延迟后运行任务，或者定期执行任务

(这个在实际项目中基本不会被用到)

### 简介

`ScheduledThreadPoolExecutor` 使用的任务队列 `DelayQueue` 封装了一个 `PriorityQueue` , `PriorityQueue` 会对队列中的任务进行排序，执行所需时间短的放在前面先被执行(`ScheduledFutureTask` 的 `Time` 变量小的先执行)，如果执行所需时间相同则先提交的任务会被先执行(`ScheduledFutureTask` 的 `squenceNumber` 变量小的先执行)

`ScheduledThreadPoolExecutor` 和 `Timer` 的比较:
- `Timer` 对系统时钟的变化敏感， `ScheduledThreadPoolExecutor` 不是
- `Timer` 只有一个执行线程，因此长时间运行的任务可以延迟其他任务。 `ScheduledThreadPoolExecutor` 可以配置任意数量的线程。此外，如果你想（通过提供 `ThreadFactory`），我们可以完全控制创建的线程
- 在 `TimerTask` 中抛出的运行时异常会杀死一个线程，从而导致 `Timer` 死机，即计划任务将不再运行。`ScheduledThreadExecutor` 不仅捕获运行时异常，还允许您在需要时处理他们（通过重写 `afterExecute` 方法 `ThreadPoolExecutor`）。抛出异常的任务将被取消，但其他任务将继续运行。

### 运行机制

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220824125554.png)

**`ScheduledThreadPoolExecutor` 的执行主要分为两大部分：**

1.  当调用 `ScheduledThreadPoolExecutor` 的 **`scheduleAtFixedRate()`** 方法或者 **`scheduleWirhFixedDelay()`** 方法时，会向 `ScheduledThreadPoolExecutor` 的 **`DelayQueue`** 添加一个实现了 **`RunnableScheduledFuture`** 接口的 **`ScheduledFutureTask`** 。
2.  线程池中的线程从 `DelayQueue` 中获取 `ScheduledFutureTask`，然后执行任务。

**`ScheduledThreadPoolExecutor` 为了实现周期性的执行任务，对 `ThreadPoolExecutor` 做了如下修改：**

-   使用 **`DelayQueue`** 作为任务队列；
-   获取任务的方不同
-   执行周期任务后，增加了额外的处理

### ScheduledThreadPoolExecutor 执行周期任务的步骤

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220824125708.png)

1.  线程 1 从 `DelayQueue` 中获取已到期的 `ScheduledFutureTask（DelayQueue.take()）`。到期任务是指 `ScheduledFutureTask` 的 time 大于等于当前系统的时间；
2.  线程 1 执行这个 `ScheduledFutureTask`；
3.  线程 1 修改 `ScheduledFutureTask` 的 time 变量为下次将要被执行的时间；
4.  线程 1 把这个修改 time 之后的 `ScheduledFutureTask` 放回 `DelayQueue` 中(`DelayQueue.add()`)。

## 线程池大小确定

**如果我们设置的线程池数量太小的话，如果同一时间有大量任务 / 请求需要处理，可能会导致大量的请求 / 任务在任务队列中排队等待执行，甚至会出现任务队列满了之后任务 / 请求无法处理的情况，或者大量任务堆积在任务队列导致 OOM。这样很明显是有问题的！CPU 根本没有得到充分利用。**

**但是，如果我们设置线程数量太大，大量线程可能会同时在争取 CPU 资源，这样会导致大量的上下文切换，从而增加线程的执行时间，影响了整体执行效率。**

> 上下文切换：
>
> 多线程编程中一般线程的个数都大于 CPU 核心的个数，而一个 CPU 核心在任意时刻只能被一个线程使用，为了让这些线程都能得到有效执行，CPU 采取的策略是为每个线程分配时间片并轮转的形式。当一个线程的时间片用完的时候就会重新处于就绪状态让给其他线程使用，这个过程就属于一次上下文切换。概括来说就是：当前任务在执行完 CPU 时间片切换到另一个任务之前会先保存自己的状态，以便下次再切换回这个任务时，可以再加载这个任务的状态。**任务从保存到再加载的过程就是一次上下文切换**。
>
> 上下文切换通常是计算密集型的。也就是说，它需要相当可观的处理器时间，在每秒几十上百次的切换中，每次切换都需要纳秒量级的时间。所以，上下文切换对系统来说意味着消耗大量的 CPU 时间，事实上，可能是操作系统中时间消耗最大的操作。
>
> Linux 相比与其他操作系统（包括其他类 Unix 系统）有很多的优点，其中有一项就是，其上下文切换和模式切换的时间消耗非常少。

有一个简单并且适用面比较广的公式：

-   **CPU 密集型任务 (N+1)：** 这种任务消耗的主要是 CPU 资源，可以将线程数设置为 N（CPU 核心数）+1，比 CPU 核心数多出来的一个线程是为了防止线程偶发的缺页中断，或者其它原因导致的任务暂停而带来的影响。一旦任务暂停，CPU 就会处于空闲状态，而在这种情况下多出来的一个线程就可以充分利用 CPU 的空闲时间。
-   **I/O 密集型任务 (2N)：** 这种任务应用起来，系统会用大部分的时间来处理 I/O 交互，而线程在处理 I/O 的时间段内不会占用 CPU 来处理，这时就可以将 CPU 交出给其它线程使用。因此在 I/O 密集型任务的应用中，我们可以多配置一些线程，具体的计算方法是 2N。