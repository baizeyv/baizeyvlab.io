---
title: Linux 实用命令
date: 2022-08-23 15:01:00
tags: 
 - linux
 - command
categories:
 - Linux
---

# Linux 实用命令

## 进程管理

*** `ps` ***

> 使用 `ps` 命令即可查看当前系统中正在执行的进程的各种进程信息

### 基本语法

`ps -options`

> `-a` ：显示当前终端所有进程信息
> 
> `-u` ：以用户的形式显示进程信息
> 
> `-x` ：显示后台进程运行的参数

例如：

```shell
ps -aux | grep mysql # 查看mysql服务的进程
```

说明：

> 1. `grep` 命令用于查找文件里符合条件的字符串
> 2. 命令格式： **命令A | 命令B** ，即命令A的正确输出作为命令B的操作对象 （命令B用来操作命令A的输出结果）

#### 查看父进程

`ps -ef` 是以全格式显示当前的所有进程

> `-e` ：显示所有进程
> 
> `-f` ：全格式显示


:::tip
	- UID代表用户ID，PID代表进程ID
:::

### 终止进程

`kill` 或 `killall`

#### 基本语法

`kill -options PID` ***即通过进程号杀死进程***

`killall ProcessName` ***即通过进程名称杀死进程，支持通配符***

`kill -9 PID` ***-9代表强制进程立即停止***

### 查看进程树

`pstree`

#### 基本语法

`pstree -options` 

常用选项：

> `-p` ：显示进程的PID
> `-u` ：显示进程所属用户


## Vi - Vim - NeoVim

Pass

## 内存使用情况

### 方式一

通过以下命令查看 `RAM` 使用情况

```bash
cat /proc/meminfo
```

> `/proc/meminfo` 列出了所有有关内存的使用情况

### 方式二

使用 `free` 查看系统内存使用情况

```bash
free
```

各列说明：

| 列名       | 意义                   |
| ---------- | ---------------------- |
| total      | 总计物理内存的大小     |
| used       | 已使用的内存的大小     |
| free       | 当前可用内存剩余的大小 |
| shared     | 多个进程共享的内存总额 |
| buff/cache | 磁盘缓存的大小         |

### 方式三

使用 `top` 命令，该命令用于实时显示进程的动态

```bash
top
```

列值说明：

| 列名    | 意义                                                                       |
| ------- | -------------------------------------------------------------------------- |
| PID     | 进程ID                                                                     |
| USER    | 进程所有者                                                                 |
| PR      | 进程优先级，越小越优先                                                     |
| VIRT    | 进程占用的虚拟内存                                                         |
| RES     | 进程占用的物理内存                                                         |
| SHR     | 进程使用的共享内存                                                         |
| S:      | 进程的状态，S标识休眠，R表示正在运行，Z表示僵死状态，N表示该进程优先级为负 |
| %CPU    | 进程占用CPU的使用                                                          |
| %MEM    | 进程使用的物理内存和总内存的百分比                                         |
| TIME+   | 该进程启动后占用的总的CPU时间，即占用CPU使用时间的累加和                   |
| COMMAND | 进程启动命令名称                                                           |

## 查看端口占用情况

`lsof(list open files)` 是一个列出当前系统打开文件的工具

`lsof` 查看端口占用的语法如下：

```bash
lsof -i:端口号
```

列值说明： 

| 列名    | 意义                                                      |
| ------- | --------------------------------------------------------- |
| COMMAND | 进程名称                                                  |
| PID     | 进程ID                                                    |
| USER    | 进程所有者                                                |
| FD      | 文件描述符，应用程序通过文件描述符识别该文件。如cwd,txt等 |
| TYPE    | 文件类型，如DIR,REG等                                     |
| DEVICE  | 指定磁盘的名称                                            |
| SIZE    | 文件的大小                                                |
| NODE    | 索引节点（文件在磁盘上的标识）                            |
| NAME    | 打开文件的确切名称                                        |

## 动态查看实时日志

使用 `tail` 命令，实时监控日志

命令示例：(以pacman日志为例)

```bash
tail -f /var/log/pacman.log
```

### tail 命令语法

`tail [ -f ] [ -c Number | -n Number | -m Number | -b Number | -k Number ] [ File ]`

参数详解：

- `-f` 该参数用于监视 File 文件增长
- `-c Number` 从 Number 字节位置读取指定文件
- `-n Number` 从 Number 行位置读取指定文件
- `-m Number` 从 Number 多字节字符位置读取该文件
- `-b Number` 从 Number 标识的 512 字节块位置读取指定文件
- `-k Number` 从 Number 表示的 1KB 块位置读取指定文件

查看 `tomcat` 日志

```bash
tail -f catalina.out
```

## awk

`awk` 是一个强大的文本分析工具，把文件逐行读入，以空格为默认分隔符将每行切片，切开的部分再进行分析处理

### 基本用法

```bash
awk [选项参数] 'pattern1{action1} pattern2{action2}...' filename
```

> `pattern`: 表示awk再数据中查找的内容，就是匹配模式
> 
> `action`: 表示再找到匹配内容时所执行的一系列命令

> 注意：行匹配语句awk只能用单引号。单引号内部可以使用双引号，但是顺序不能错

### 常用选项参数说明

- `-F`: 指定输入文件分隔符（指定分隔符）
- `-v`: 复制一个用户定义变量（定义变量）
- `-f`: 引用awk执行脚本（调用脚本）

### 特殊要点（内置变量）

- `$0`: 表示整个当前行
- `$1`: 每行第一个字段
- `NF`: 字段数量变量
- `NR`: 每行的记录号，多文件记录递增
- `FNR`: 与 `NR` 类似，不过多文件记录不递增，每个文件都从1开始
- `\t`: 制表符
- `\n`: 换行符
- `FS`: `BEGIN` 时定义分隔符
- `RS`: 输入的记录分隔符，默认为换行符（即文本时按一行一行输入）
- `~`: 匹配，与 `==` 相比不是精确比较
- `!~`: 不匹配，不精确比较
- `==`: 等于，必须全部相等，精确比较
- `!=`: 不等于，精确比较
- `&&`: 逻辑与
- `||`: 逻辑或
- `+`: 匹配时表示1个或一个以上
- `/[0-9][0-9]+/`: 两个或两个以上数字
- `/[0-9][0-9]*/`: 一个或一个以上数字
- `FILENAME`: 文件名
- `OFS`: 输出字段分隔符，默认为空格，可以改为制表符等
- `ORS`: 输出的记录分隔符，默认为换行符，即处理结果也是一行一行输出到屏幕
- `-F'[:#/]'`: 定义三个分隔符

### 案例

案例文件如下：

```txt
[root@jiangnan awk]# cp /etc/passwd ./
[root@jiangnan awk]# cat passwd 
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
```

#### 示例1

搜索passwd文件以root关键字开头的所有行，并输出该行的第7列

```bash
awk -F: '/^root/{print $7}' passwd
```

输出： `/bin/bash`

> `-F`，指定分隔符为 `:`
> 
> `print`: 打印
> 
> `$7`: 第7列（域）
> 
> `^` 在 root 前，表示以指定字符开头，如果没有，则表示有指定字符的行，位置不限

#### 示例2

搜索passwd文件以root关键字开头的所有行，并输出该行的第一列和第7列，中间以","分割

```bash
awk -F: '/^root/{print $1","$7}' passwd
```

输出： `root,/bin/bash`

> 注意：只有匹配了pattern的行才会执行action

#### 示例3

将passwd文件中的用户id增加数值1并输出

```bash
awk -v i=1 -F: '{print $3+i}' passwd
```

输出：

```txt
1
2
3
4
...
```

#### 示例4

将awk程序写入文件，通过 `-f` 调用脚本

awk脚本文件`test`：

```txt
{print $1,$3,$NF}
```

```bash
awk -F: -f test passwd
```

输出：

```txt
root 0 /bin/bash
bin 1 /sbin/nologin
...
```

#### 示例5

只显示passwd文件的第一列和第七列，以逗号分割，且在行前面添加列名user,shell，最后以行添加 last,/bin/last

```bash
awk -F: 'BEGIN{print "user, shell"} {print $1","$7} END{print "last,/bin/last"}' passwd
```

输出:

```txt
user, shell 
root,/bin/bash
bin,/sbin/nologin
...
last,/bin/last
```

> `BEGIN` 在所有数据读取行之前执行
> 
> `END` 在所有数据执行后执行

#### 示例6

统计passwd文件名，每行的行号，每行的列数

```bash
awk -F: '{print "filename:" FILENAME ", linenumber:" NR ",columns:" NF}' passwd
```

输出：

```txt
filename:passwd, linenumber:1,columns:7
filename:passwd, linenumber:2,columns:7
filename:passwd, linenumber:3,columns:7
...
```

#### 示例7

查询文件中空行所在的行号

```bash
awk '/^$/{print NR} passwd'
```

> `'/root$/{print $1}'` 其中 `/root$/` 代表以root结尾的行

#### 示例8

输出文件第3行的所有数据

```bash
awk 'NR==3{print $0}' passwd
```

#### 示例9

输出第一个字段为root所在行

```bash
awk -F: '$1=="root"{print $0}' passwd
```

#### 示例10

查看已使用的内存

```bash
head -2 /proc/meminfo | awk 'NR==1{a=$2}NR==2{b=$2;print(a-b)*100/a "%"}'
```

示例输出： `95.1234%`

### awk程序的优先级

`BEGIN` 时优先级最高的代码块，是在执行 `PROGRAM` 之前执行的，不需要提供数据源，因为不涉及到任何数据的处理，也不依赖于 `PROGRAM` 代码块

`PROGRAM` 是对数据流干什么，是必选代码块，也是默认代码块，所以在执行时必须提供数据源

`END` 时处理完数据流后的操作，如果需要执行 `END` 代码块，就必须需要 `PROGRAM` 的支持，单个无法执行

> 总结：
> 
>> 1. `BEGIN` 在开始处理数据流之前执行，可选项
>> 2. `PROGRAM` 如何处理数据流，必选项
>> 3. `END` 处理完数据流后执行，可选项

> `BEGIN` 不需要数据源（即数据文件）就可以执行操作
> 
> `END` 没有数据源则无法执行