---
title: Java 8 Lambda
date: 2022-08-22 14:11:00
tags: 
 - java
 - lambda
categories:
 - Java
---

# Java Lambda

## Lambda 表达式概述

Lambda表达式（闭包）：`Java8` 的新特性，`lammbda` 运行将函数作为一个方法的参数，也就是 *函数作为参数* 传递到方法中。

> 使用 `lambda` 表达式可以让代码更简洁

`Lambda` 表达式的使用场景： ***用以简化接口实现***

关于接口实现，可以有很多方式实现。例如：设计接口的实现类、使用匿名内部类等

但是 `lambda` 表达式，比这两种方式都简单

### 示例

```java
public class TestA {
	public static void main(String[] args) {
		Test test = () -> {
			System.out.println("test lambda");
		}
	}
}

interface Test {
	public void test();
}
```

## Lambda 表达式对接口的要求

并不是所有接口都可以使用 `lambda` 表达式。

`Lambda` 表达式只是一个匿名方法。若实现的接口中的方法过多或者过少的时候，`lambda` 表达式均不适用

**lambda表达式，只能实现函数式接口** 

### 函数式接口

> 一个接口中，有且只有一个实现类必须实现的抽象方法，就是函数式接口

#### 示例

```java
interface Test {
	public void test();
}
```

### @FunctionalInterface

`@FunctionalInterface` 是用在接口前的注解，用于判断这个接口是否是一个函数式接口。

如果是函数式接口，则没有问题。若不是函数式接口，则会报错。功能类似于 `@Override`

#### 示例

```java
@FunctionalInterface 
interface Test {
	public void test();
}
```

### Lambda 表达式基础语法

`lambda` 表达式，本质就是一个匿名函数，故在写 `lambda` 表达式时，不需要关心方法名，同时也不需要关心返回值类型。

只需要关注两个部分：
1. 参数列表
2. 方法体

基础语法：
```
(arg1, arg2, ...) -> {
	method body;
}
```

> 参数部分：方法的参数列表，要求和要实现的接口中的方法参数部分一直，包括参数的数量和类型
> 
> 方法体部分：方法的实现部分，如果接口中定义的方法有返回值，则在实现的时候，注意返回值的返回
> 
> `->` ：分割参数部分和方法体部分

#### 示例

```java
public class TestCls {
	public static void main(String[] args) {
		// no args
		NoArgs noArgs = () -> {
			System.out.println("noargs test");
		}
		noArgs.test();

		// an args
		AnArgs anArgs = () -> {
			System.out.println("anargs test");
		}
		anArgs.test();

		// arg and return value
		ArgAndRet argAndRet = (name, age) -> {
			System.out.println(name + "is " + age + " years old");
			return age + 1;
		}
		int ae = argAndRet.test("testName", 18);
		System.out.println(age); // 19
	}
}

interface NoArgs {
	public void test();
}

interface AnArg {
	public void test();
}

interface ArgAndRet {
	public int test(String name, int age);
}
```

### Lambda 表达式进阶语法

#### 参数部分的精简

##### 参数的类型

由于在接口的方法中，已经定义了每一个参数的类型是什么。而且在使用 `lambda` 表达式实现接口的时候，必须要保证参数的数量和类型需要和接口中的方法保持一致。因此，此时 `lambda` 表达式中的参数的类型可以 *省略不写*

> 如果需要省略参数的类型，要保证，每一个参数的类型都必须省略不写
> 
> 绝不能出现，有的参数类型省略了，有的参数类型没有省略

##### 参数的小括号

如果方法的参数列表中的参数数量有且只有一个，此时，参数列表的小括号可以 *省略不写*

> 只有当参数的数量是一个的时候，才可以省略
> 
> 省略小括号的同时，必须要省略参数的类型

```java
Test test = name -> {
	System.out.println(name + "Test");
}
test.test("NAME");
```

#### 方法体部分的精简

##### 方法体大括号的精简

当一个方法体中的逻辑，有且只有一句的情况下，大括号可以省略

```java
Test test = name -> System.out.println(name);
test.test("NAME");
```

##### return的精简

如果一个方法中唯一的一条语句是一个返回语句，此时在省略大括号的同时，也必须省略return

```java
Test test = name -> name;
```

## 函数引用

`lambda` 表达式时为了简化接口的实现而设计的。

在 `lambda` 表达式中，不应该出现比较复杂的逻辑。

如果在 `lambda` 表达式中出现了过于复杂的逻辑，会对程序的可读性造成很大的影响。

如果在 `lambda` 表达式中需要处理的逻辑比较复杂，一般情况会单独写一个方法，在表达式中直接引用这个方法。

**函数引用：** 引用一个已经存在的方法，使其代替 `lambda` 表达式完成接口的实现

### 静态方法的引用

语法：
> `类::静态方法`

注意事项：
> 1. 在引用的方法后面，不需要添加小括号
> 2. 引用的这个方法，参数（数量、类型）和返回值，必须和接口中定义的一致

#### 示例

```java
public class TestB {
	public static void main(String[] args) {
		Test test = Tmp::calc;
		System.out.println(test.test(2,3));
	}
}

public class Tmp {
	public static int calc(int a, int b) {
		if(a > b) {
			return a - b;
		}
		return b - a;
	}
}

interface Test {
	int test(int a, int b);
}
```

### 非静态方法的引用

语法：
> `对象::非静态方法`

注意事项： 
> 1. 在引用的方法后面，不需要添加小括号 
> 2. 引用的这个方法，参数（数量、类型）和返回值，必须和接口中定义的一致 

#### 示例

```java
public class TestC {
	public static void main(String[] args) {
		Test test = new Calc()::c;
		System.out.println(test.calc(2,3));
	}
	private static class Calc {
		public int c(int a, int b) {
			return a > b ? a - b : b - a;
		}
	}
}

interface Test {
	int calc(int a, int b);
}
```

### 构造方法的引用

使用场景：
> 如果某一个函数式接口中定义的方法，仅仅是为了得到一个类的对象。
> 
> 此时我们可以使用构造方法的引用，简化这个方法的实现

语法：
> `类名::new`

注意事项： 
> 可以通过接口中的方法的参数，区分引用不同的构造方法

#### 示例

```java
public class Test {
    private static class Dog{
        String name;
        int age;
        //无参构造
        public Dog(){
            System.out.println("一个Dog对象通过无参构造被实例化了");
        }
        //有参构造
        public Dog(String name,int age){
            System.out.println("一个Dog对象通过有参构造被实例化了");
            this.name = name;
            this.age = age;
        }
    }
    //定义一个函数式接口,用以获取无参的对象
    @FunctionalInterface
    private interface GetDog{
        //若此方法仅仅是为了获得一个Dog对象，而且通过无参构造去获取一个Dog对象作为返回值
        Dog test();
    }

    //定义一个函数式接口,用以获取有参的对象
    @FunctionalInterface
    private interface GetDogWithParameter{
        //若此方法仅仅是为了获得一个Dog对象，而且通过有参构造去获取一个Dog对象作为返回值
        Dog test(String name,int age);
    }

    // 测试
    public static void main(String[] args) {
        //lambda表达式实现接口
        GetDog lm = Dog::new; //引用到Dog类中的无参构造方法，获取到一个Dog对象
        Dog dog = lm.test();
        System.out.println("修狗的名字："+dog.name+" 修狗的年龄："+dog.age); //修狗的名字：null 修狗的年龄：0
        GetDogWithParameter lm2 = Dog::new;//引用到Dog类中的有参构造，来获取一个Dog对象
        Dog dog1 = lm2.test("萨摩耶",2);
        System.out.println("修狗的名字："+dog1.name+" 修狗的年龄："+dog1.age);//修狗的名字：萨摩耶 修狗的年龄：2

    }
}
```

## Lambda 表达式注意问题

> 如果在 `lambda` 表达式中，使用到了局部变量，那么这个局部变量会被隐式的声明为 `final`。是一个常量，不能修改值