---
title: Spring AOP
date: 2022-08-22 12:11:00
tags: 
 - spring
 - aop
categories:
 - Spring
---

# Spring AOP

## AOP 概述

`AOP(Aspect Oriented Programing)` 意为：面向切面编程

是通过预编译方式和运行期动态代理实现程序功能的统一维护的一种技术。

`AOP` 是 `OOP` 的延续，是函数式变成的一种衍生范型。

利用 `AOP` 可以对业务逻辑的各个部分进行隔离，从而使得业务逻辑各部分之间的耦合度降低，提高程序的可重用性，同时提高了开发的效率。

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220822115141.png)

> Spring的AOP就是将公共的业务（日志，安全等）和领域业务结合起来，当执行领域业务时，将会把公共业务加进来，实现公共业务的重复利用；领域业务更纯粹，程序员专注领域业务，其本质还是动态代理

## AOP 在 Spring 中的作用

***提供声明式事务；允许用户自定义切面***

- **横切关注点**：跨越应用程序多个模块的方法或功能。即是，与我们业务逻辑无关的，但是我们需要关注的而部分，就是横切关注点。如日志、安全、缓存、事务等...
- **切面(ASPECT)**：横切关注点被模块化的特殊对象。即，它是一个类。
- **通知(ADVICE)**：切面必须要完成的工作。即，它是切面类中的一个方法
- **目标(TARGET)**：被通知的对象
- **代理(PROXY)**：向目标对象应用通知之后创建的对象
- **切入点(PointCut)**：切面通知执行的“位置”的定义（实际被增强的方法）
- **连接点(JointPoint)**：与切入点匹配的执行点

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220822115851.png)

## 5种通知类型

SpringAOP中，通过Advice定义横切逻辑，Spring中支持5种类型的Advice:

| 通知类型     | 连接点               | 实现接口                                        |
| ------------ | -------------------- | ----------------------------------------------- |
| 前置通知     | 方法调用前           | org.springframework.aop.MethodBeforeAdvice      |
| 后置通知     | 方法执行后           | org.springframework.aop.AfterReturningAdvice    |
| 环绕通知     | 方法调用前与执行后   | org.aopalliance.intercept.MethodInterceptor     |
| 异常抛出通知 | 方法抛出异常         | org.springframework.aop.ThrowsAdvice            |
| 引介通知     | 类中增加新的方法属性 | org.springframework.aop.IntroductionInterceptor |

> 即：`AOP` 在不改变原有代码的情况下，去增加新的功能。

## 使用 Spring 实现 AOP

:::Tip
- 实现AOP需要导入以下依赖
:::

```xml
<dependency>
	<groupId>org.aspectj</groupId>
	<artifactId>aspectjweaver</artifactId>
	<version>1.9.4</version>
</dependency>
```

### 方式一

***通过 Spring API 实现***

#### 业务接口及实现类

首先编写我们的业务接口以及实现类

```java
public interface UserService {
	public void add();
	public void delete();
	public void update();
	public void search();
}
```

```java
public class UserServiceImpl implements UserService {
	@Override
	public void add() {
		System.out.println("ADD...");
	}
	
	@Override 
	public void delete() {
		System.out.println("DELETE...");
	}
	
	@Override 
	public void update() {
		System.out.println("UPDATE...");
	}

	@Override 
	public void search() {
		System.out.println("SEARCH...");
	}
}
```

#### 编写增强类

之后我们编写增强类，此处编写一个前置增强和一个后置增强

```java
public class Log implements MethodBeforeAdvice {
	/**
	 * method: 要执行的目标对象的方法
	 * objects: 被调用的方法的参数
	 * object: 目标对象
	 */
	@Override 
	public void before(Method method, Object[] objects, Object o) throws Throwable {
		System.out.println(o.getClass().getName() + "的" + method.getName() + "METHOD EXEC...");
	}
}
```

```java
public class AfterLog implements AfterReturningAdvice {
	/**
	 * returnValue 目标方法返回值
	 * method 被调用的方法
	 * argfs 被调用方法的对象的参数
	 * target 被调用的目标对象
	 */
	@Override 
	public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
		System.out.println("EXEC." + target.getClass().getName() + "的" + method.getName() + "METHOD,RETURN:" + returnValue)
	}
}
```

#### Spring 文件中注册

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans" 
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
       xmlns:aop="http://www.springframework.org/schema/aop" 
       xsi:schemaLocation="http://www.springframework.org/schema/beans 
        http://www.springframework.org/schema/beans/spring-beans.xsd 
        http://www.springframework.org/schema/aop 
        http://www.springframework.org/schema/aop/spring-aop.xsd">
            <!--注册bean-->
    <bean id="userService" class="com.melody.service.UserServiceImpl"/> 
    <bean id="log" class="com.melody.log.Log"/>
    <bean id="afterLog" class="com.melody.log.AfterLog"/>
        <!--aop的配置-->
    <aop:config>
        <!--切入点  expression:表达式匹配要执行的方法--> 
        <aop:pointcut id="pointcut" expression="execution(* com.melody.service.UserServiceImpl.*(..))"/>
        <!--执行环绕; advice-ref执行方法 . pointcut-ref切入点--> 
        <aop:advisor advice-ref="log" pointcut-ref="pointcut"/> 
        <aop:advisor advice-ref="afterLog" pointcut-ref="pointcut"/> 
    </aop:config>
</beans>
```

#### 测试

```java
public class Test {
	@Test
	public void test() {
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		UserService userService = (UserService)context.getBean("userService");
		userService.search();
	}
}
```

### 方式二

***自定义类实现AOP***

#### 自定义切入类

```java
public class DiyPointcut {
	public void before() {
		System.out.println("BEFORE");
	}

	public void after() {
		System.out.println("AFTER");
	}
}
```

#### Spring 配置

```xml
<!--第二种方式自定义实现-->
<!--注册bean-->
<bean id="diy" class="com.kuang.config.DiyPointcut"/>
<!--aop的配置-->
<aop:config>
    <!--第二种方式：使用AOP的标签实现-->
    <aop:aspect ref="diy">
        <aop:pointcut id="diyPonitcut" expression="execution(* com.kuang.service.UserServiceImpl.*(..))"/>
        <aop:before pointcut-ref="diyPonitcut" method="before"/> 
        <aop:after pointcut-ref="diyPonitcut" method="after"/> 
    </aop:aspect>
</aop:config>
```

#### 测试

```java
public class Test {
	@Test
	public void test() {
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		UserService userService = (UserService)context.getBean("userService");
		userService.add();
	}
}
```

### 方式三

***使用注解实现***

#### 编写增强类

```java
@Aspect
public class AnnotationPointcut {
	@Before("execution(* com.melody.service.UserServiceImpl.*(..))")
	public void before() {
		System.out.println("BEFORE");
	}

	@After("execution(* com.melody.service.UserServiceImpl.*(..))")
	public void after() {
		System.out.println("AFTER");
	}

	@Around("execution(* com.melody.service.UserServiceImpl.*(..))")
	public void around(ProceedingJoinPoint jp) throws Throwable {
		System.out.println("around before");
		System.out.println("签名：" + jp.getSignature());
		// 执行目标方法proceed
		Object proceed = jp.proceed();
		System.out.println("around after");
		System.out.println(proceed);
	}
}
```

#### Spring 配置

```xml
<!--第三种方式:注解实现-->
<bean id="annotationPointcut" class="com.melody.config.AnnotationPointcut"/> <aop:aspectj-autoproxy/>
```

`aop:aspectj-autoproxy` 说明：

> 1. 通过aop命名空间的 `<aop:aspectj-autoproxy />` 声明自动为spring容器中那些配置@aspectJ切面 的bean创建代理，织入切面。当然，spring 在内部依旧采用 `AnnotationAwareAspectJAutoProxyCreator` 进行自动代理的创建工作，但具体实现的细节已经被 `<aop:aspectj-autoproxy />` 隐藏起来了
>
> 2. `<aop:aspectj-autoproxy />` 有一个 `proxy-target-class` 属性，默认为false，表示使用jdk动态 代理织入增强，当配为 `<aop:aspectj-autoproxy  poxy-target-class="true"/>` 时，表示使用 CGLib动态代理技术织入增强。不过即使proxy-target-class设置为false，如果目标类没有声明接口，则spring将自动使用CGLib动态代理。