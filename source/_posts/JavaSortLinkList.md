---
title: Java实现有序单链表并实现反转
date: 2022-08-22 19:01:00
tags: 
 - java
 - data struct
categories:
 - Java
---

# Java 实现有序单链表

```java
public class LinkNode {  
  
    public int data;  
  
    public LinkNode next;  
  
    public LinkNode(int data) {  
        this.data = data;  
    }  
  
    public void out() {  
        System.out.print("| " + data + " |");  
    }  
  
}
```

```java
public class MyLinkList {  
  
    // 头节点  
    private LinkNode head;  
  
    public MyLinkList() {  
        // 初始化头节点  
        this.head = null;  
    }  
  
    public boolean isEmpty() {  
        return head == null;  
    }  
  
    /**  
     * asc insert     * @param data  
     */  
    public void insert(int data) {  
        LinkNode newNode = new LinkNode(data);  
        LinkNode pre = null;  
        LinkNode cur = head;  
  
        while (cur != null && data > cur.data) {  
            pre = cur;  
            cur = cur.next;  
        }  
  
        if(pre == null) {  
            head = newNode;  
        } else {  
            pre.next = newNode;  
        }  
  
        newNode.next = cur;  
    }  
  
    /**  
     * 单链表反转  
     */  
    public void reverse() {  
        if(head == null || head.next == null) {  
            return;  
        }  
        LinkNode cur = head.next;  
        head.next = null;  
        while (cur != null) {  
            LinkNode curNext = cur.next;  
            cur.next = head;  
            head = cur;  
            cur = curNext;  
        }  
    }  
  
    public void out() {  
        LinkNode cur = head;  
        while (cur != null) {  
            cur.out();  
            cur = cur.next;  
        }  
        System.out.println();  
    }  
  
}
```

```java
import java.util.Arrays;  
  
public class Main {  
    public static void main(String[] args) {  
        MyLinkList myLinkList = new MyLinkList();  
        myLinkList.insert(4);  
        myLinkList.insert(3);  
        myLinkList.insert(1);  
        myLinkList.insert(2);  
        myLinkList.out();  
        myLinkList.reverse();  
        myLinkList.out();  
    }  
}
```

输出：
![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220822192237.png)
