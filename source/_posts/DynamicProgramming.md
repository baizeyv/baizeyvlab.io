---
title: dynamic programming
date: 2022-08-31 22:01:00
tags: 
 - dp
 - ds 
 - 128
categories:
 - DP
 - DS
---

# 动态规划

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220831215241.png)

## 题目特点

1. 计数
	- 有多少种方式走到右下加
	- 有多少种方法选出k个数使得和为sum
2. 求最大最小值
	- 从左上角走到右下角路径的最大数字和
	- 最长上升子序列长度
3. 求存在性
	- 取石子游戏，先手是否必胜
	- 能不能选出k个数使得和为sum

> LintCode 669: Coin Change
> 
>> 有三种硬币2、5、7元，每种硬币足够多
>> 
>> 买一本书要27元
>> 
>> 如何用最少的硬币组合正好付清，不需要找钱

直觉做题：最少的硬币组合 -> 尽量用面值大的硬币

直觉得出： 7 + 7 + 7 + 2 + 2 + 2 = 27 

正确答案： 7 + 5 + 5 + 5 + 5 = 27

## 动态规划组成部分

1. 确定状态
2. 转移方程
3. 初始条件和边界情况
4. 计算顺序

### 确定状态

1. 状态在动态规划中的作用属于定海神针

2. 简单的说，解动态规划的时候需要开一个数组，数组的每个元素 `array[i]` 或者 `array[i][j]` 代表什么

	- 类似数学题中的 `x`, `y`, `z` 代表什么

3. 确定状态需要两个意识：
	1. 最后一步
	2. 子问题

> ***最后一步***：
> 
>> 不知道最优策略，但是最优策略一定是 k 个硬币面值加起来为27
>> 
>> 所以一定有一枚 *最后* 的硬币 a<sub>k</sub>
>> 
>> 除了最后一枚硬币，前面的面值和为 27-a<sub>k</sub>
>> ![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220831220643.png)
>>> **关键点**：
>>> 1. 不关心前面 k-1 枚硬币怎么拼出 27-a<sub>k</sub> 的
>>> 2. 因为是最优策略，所以拼出的 27-a<sub>k</sub> 的硬币数一定要少，否则就不是最优策略了

> ***子问题***：
> 
>> 所以就变为了求：最少用多少枚硬币可以拼出 27-a<sub>k</sub>
>> 
>> 原问题是最少用多少枚硬币拼出27
>> 
>> 现在将原问题转化成了一个子问题,而且规模更小： 27-a<sub>k</sub>
>> 
>> 为了简化定义，我们设定状态 `f(X)=最少用多少枚硬币拼出X`

我们还不知道最后的硬币 a<sub>k</sub> 是多少

但是最后的硬币 a<sub>k</sub> 只可能是 2 或 5 或 7

如果 a<sub>k</sub> 是 2 ，`f(27)` 应该是 `f(27-2) + 1` （加上最后这一枚硬币2）

如果 a<sub>k</sub> 是 5 ，`f(27)` 应该是 `f(27-5) + 1`

如果 a<sub>k</sub> 是 7 ，`f(27)` 应该是 `f(27-7) + 1`

除此之外，没有其他的可能

因为需要求最小值，所以：

`f(27) = min{f(27-2) + 1, f(27-5) + 1, f(27-7) + 1}`

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220831221755.png)

### 转移方程

- 设状态 `f[X]=最少用多少枚硬币拼出X`
- 对于任意X,`f[X] = min{f[X-2] + 1, f[X-5] + 1, f[X-7] + 1}`

### 初始条件和边界情况

- `f[27] = min{f[27-2] + 1, f[27-5] + 1, f[27-7] + 1}`

存在两个问题：

1. `x-2`, `x-5` 或 `x-7` 小于0怎么办？
2. 什么时候停下来？

如果不能拼出Y，就定义 `f[Y]=正无穷` ：`f[-1]=f[-2]=...=正无穷`

所以 `f[1]=min{f[-1]+1,f[-4]+1,f[-6]+1=正无穷` ，表示拼不出来1

- 初始条件 `f[0]=0` 

> ***如果一个状态不能从转移方程求得，但是它又有值，那么应该手动设置初始值***

### 计算顺序

拼出X所需要的最少硬币数量：`f[X] = min{f[X-2] + 1, f[X-5] + 1, f[X-7] + 1}`

初始条件：`f[0]=0`

然后计算 `f[1],f[2]...f[27]`

当我们计算到 `f[x]` 时候，`f[x-2]`,`f[x-5]`,`f[x-7]` 都应已经得到结果了

> 当我们进行到要算的状态时，要保证其用到的其他状态必须先于它计算出来

### 解法

#### 递归解法

```java
public void f(int x) {
	if(x == 0) {
		return 0;
	}
	int res = MAX_VALUE; // 假设初始值是无穷大
	if(x >= 2) {
		res = Math.min(f(x-2)+1, res);
	}
	if(x >= 5) {
		res = Math.min(f(x-5)+1, res);
	}
	if(x >= 7) {
		res = Math.min(f(x-7)+1, res);
	}
	return res;
}
```

递归解法的问题：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220831222135.png)

有很多节点（包括其下节点）计算了很多次

**重复、冗余的计算，效率低下**

#### 标准做法

- 将计算结果保存下来，并改变计算顺序

```java
// coins {2,5,7} amount 27
public int coinChange(int[] coins, int amount) {
	int[] f = new int[amount + 1]; // 用到的最大的下标+1，就是数组大小
	int n = coins.length;

	// init
	f[0] = 0;
	int i, j;
	for(i = 1; i <= amount; ++i) {
		f[i] = Integer.MAX_VALUE;
		for(j = 0; j < n; ++j) {
			if(i >= coins[j] && f[i - coins[j]] != Integer.MAX_VALUE) {
				f[i] = Math.min(f[i], f[i-coins[j]] + 1);
			}
		}
	}

	if(f[amount] == Integer.MAX_VALUE) {
		f[amount] = -1;
	}
	return f[amount];
}
```

---

## 例题

LintCode 114: Unique Paths

给定m行n列，左上角触发，只能向下或右走，求多少种方式走到右下角

最后一步：向右或者向下

右下角坐标为 (m-1, n-1)

那么前一步一定在 (m-2, n-1) 或 (m-1, n-2)

如果有x种方式走到(m-2, n-1)，有y种方式走到(m-1, n-2)，那么就有x+y种方式走到(m-1, n-1)

其中两个坐标为子问题了

设：`f[i][j]` 代表有多少种方式走到(i,j)

初始条件: `f[0][0]=1` 只有一种方式走到左上角

边界情况 i=0或j=0，则前一步只能由一个方向过来 `f[i][j]=1`

```java
public int uniquePaths(int m, int n) {
	int[][] f = new int[m][n];
	int i, j;
	for(i = 0; i < m ; i++) {
		for(j=0; j < n; j++) {
			if(i == 0 || j == 0) {
				f[i][j] = 1;
			} else {
				f[i][j] = f[i][j-1] + f[i-1][j];
			}
		}
	}
	return f[m-1][n-1];
}
```

---

LintCode 116: Jump Game

由n个石头，分别在x轴的0,1,...,n-1位置

一只青蛙在0，想跳到石头n-1

如果青蛙在第i块上，最后可以向右跳 a<sub>i</sub>

青蛙能否跳到石头n-1


最后一步：跳到n-1石头上，是从i调过来的 `i<n-1`   `n-a-i<=`a<sub>i</sub> 青蛙可以跳到石头i

现在子问题：青蛙是否可以跳到i

设 `f[j]` 表示青蛙能不能跳到石头j

`f[j]` = OR<sub>0<=i<j</sub>(f[i] AND i + a[i] >= j)

初始条件 f[0]=true

```java
public boolean canJump(int[] arr) {
	boolean[] f = new boolean[arr.length];
	f[0] = true;
	for(int j = 1; j < n; j++) {
		f[j] = false;
		// enum the previous stoen i 
		for(int i = 0; i < j; i++) {
			if(f[i] && i + arr[i] >= j) {
				f[j] = true;
				break;
			}
		}
	}
	return f[n-1];
}
```