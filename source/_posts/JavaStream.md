---
title: Java 8 Stream
date: 2022-08-22 17:11:00
tags: 
 - java
 - stream
categories:
 - Java
---

# Java Stream

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220822144403.png)

## Stream 概述

什么是 `Stream` ?
> `Stream` 会将要处理的元素集合看作一种流，在流的过程中，借助 `Stream API` 对流中的元素进行操作，比如：筛选、排序、聚合等

`Stream` 可以由数组或集合创建，对流的操作分为两种：
1. 中间操作，每次返回一个新的流，可以有多个。（筛选 `filter` 、映射 `map` 、排序 `sorted` 、去重组合 `skip--limit`）
2. 终端操作，每个流只能进行一次终端操作，终端操作结束后流无法再次使用。终端操作会产生一个新的集合或值。(遍历 `foreach` 、匹配 `find-match` 、规约 `reduce` 、聚合 `max-min-count` 、收集 `collect`)

`Stream` 的特性：
> 1. `stream` 不存储数据，二十按照特定的规则对数据进行计算，一般会输出结果
> 2. `stream` 不会改变数据源，通常情况下会产生一个新的集合或一个值
> 3. `stream` 具有延迟执行特性，只有调用终端操作时，中间操作才会执行

## Stream 与传统遍历对比

几乎所有的集合（如 `Collection` 接口或 `Map` 接口等）都支持直接或间接的遍历 操作。

而当我们需要对集合中的元素进行操作的时候，除了必须的添加、删除、获取外，最典型的就是集合遍历，例如：

```java
public class Demo1 {
	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("abc");

		List<String> listA = new ArrayList<>();
		for(String s : list) {
			if(s.startsWith("a")) {
				listA.add(s);
			}
		}

		List<String> listB = new ArrayList<>();
		for(String s : listA) {
			if(s.length() == 3) {
				listB.add(s);
			}
		}

		for(String s: listB) {
			System.out.println(s);
		}
	}
}
```

循环遍历的弊端：
> `for` 循环的语法是 “怎么做”
> 
> `for` 循环的循环体是 “做什么”
> 为什么要使用循环？
> 
> 因为要进行遍历。但循环并不是遍历的唯一方式
> 
> 遍历是指每一个元素逐一进行处理，而并不是从第一个到最后一个顺次处理的循环


### Stream 写法

```java
public class Demo2 {
	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("abc");
		list.stream()
			.filter(name -> name.startsWith("a"))
			.filter(name -> name.length() == 3)
			.forEach(name -> System.out.println(name));
	}
}
```

## Stream 的创建

`Stream` 可以通过集合数组创建。

### 用集合创建流

通过 `java.util.Collection.stream()` 方法用集合创建流

```java
List<String> list = Arrays.asList("a", "b", "c");
// 创建一个顺序流
Stream<String> stream = list.stream();
// 创建一个并行流
Stream<String> parallelStream = list.parallelStream();
```

> `stream` 和 `parallelStream` 简单区分：
> 
>> `stream` 是顺序流，由主线程按顺序对流执行操作
>> 
>> `parallelStream` 是并行流，内部以多线程并行执行的方式对流进行操作，但前提是流中的数据处理没有顺序要求
>
> 如果流中的数据量足够大，并行流可以加快处理速度。
> 
> 除了直接创建并行流，也可以通过 `parallel()` 把顺序流转换成并行流：
> ```java
> Optional<Integer> findFirst = list.stream().parallel().filter(x->x>6).findFirst();
> List<Integer> list = Arrays.asList(1,2,3,65,8);
> System.out.println(findFirst.get());
> ```

### 用数组创建流

使用 `java.util.Arrays.stream(T[] array)` 方法用数组创建流

```java
int[] array = {1, 3, 5, 7, 9};
IntStream stream = Arrays.stream(array);
```

### 使用 Stream 的静态方法

`of()` `iterate()` `generate()`

```java
Stream<Integer> stream = Stream.of(1,2,3,4,5,6); // 1 2 3 4 5 6

Stream<Integer> stream2 = Stream.iterate(0, (x) -> x + 3).limit(4);
stream2.forEach(System.out::println); // 0 3 6 9

Stream<Double> stream3 = Stream.generate(Math::random).limit(3);
stream3.forEach(System.out::println);
```

## Stream 的使用

在使用 `stream` 之前，先理解一个概念： `Optional`

> `Optional` 类是一个可以为 `null` 的容器对象。
>
> 如果值存在则 `isPresent()` 方法会返回 `true`，调用 `get()` 方法会返回该对象。

### 示例

```java
List<User> userList = new ArrayList<User>();
userList.add(new User("A", 10));
userList.add(new User("B", 20));
userList.add(new User("C", 30));

class User {
	private String name;
	private int age;

	public User(String name, int age) {
		this.name = name;
		this.age = age;
	}
}
```

### 遍历 / 匹配 (foreach 、 find 、 match)

`Stream` 也是支持类似集合的遍历和匹配元素的，只是 `Stream` 中的元素是以 `Optional` 类型存在的。

```java
public class StreamTest {
	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(7,6,9,3,8,2,1);
		// 遍历输出符合条件的元素
		list.stream().filter(x -> x > 6).foreach)(System.out::println);
		// 匹配第一个
		Optional<Integer> findFirst = list.stream().filter(x -> x > 6).findFirst();
		// 匹配任意（适用于并行流）
		Optional<Integer> findAny = list.parallelStream().filter(x -> x > 6).findAny();
		// 是否包含符号特定条件的元素
		boolean anyMatch = list.stream().anymatch(x -> x > 6);
		
		System.out.println("匹配第一个值：" + findFirst.get()); // 7
		System.out.println("匹配任意一个值：" + findAny.get());
		System.out.println("是否存在大于6的值：" + anyMatch); // true
	}
}
```

### 筛选 filter

> 筛选，是按照一定的规则校验流中的元素，将符合条件的元素提取到新的流中的操作

`Stream<T> filter(Predicate<? super T> predicate);`

#### 示例1

筛选出 `Integer` 集合中大于7的元素，并打印

```java
public class Test {
	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(6,7,3,8,1,2,9);
		Stream<Integer> stream = list.stream();
		stream.filter(x -> x > 7).forEach(System.out::println); // 8 9
	}
}
```

#### 示例2

筛选User中年龄大于10的人，并形成新的集合。形成新集合依赖 `Collect`

```java
public class Test {
	public static void main(String[] args) {
		List<User> userList = new ArrayList<User>();
		userList.add(new User("A", 8));
		userList.add(new User("B", 10));
		userList.add(new User("C", 11));
		userList.add(new User("D", 20));
		List<String> filterList = userList.strea().filter(x -> x.getAge() > 10).map(User::getAge).collect(Collectors.toList());
		System.out.print(filterList); // [C, D]
	}
}
```

### 聚合 (max 、 min 、 count)

#### 示例1

获取 String 集合中最长的元素

```java
public class Test {
	public static void main(String[] args) {
		List<String> list = Arrays.asList("asrt", "esrantyu", "snt", "sraetinsraient");
		Optional<String> max = list.stream().max(Comparator.comparing(String::length));
	}
}
```

#### 示例2

获取 Integer集合中的最大值

```java
public class Test {
	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(7,6,9,4,11,6);
		// 自然排序
		Optional<Integer> max = list.stream().max(Integer::compareTo);
		// 自定义排序
		Optional<Integer> max2 = list.stream().max(new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o1.compareTo(o2);
			}
		});
		System.out.println("自然排序的最大值："+max.get()); // 11
		System.out.println("自定义排序的最大值:"+max2.get()); // 11
	}
}
```

#### 示例3

计算 `Integer` 集合中大于6的元素的个数

```java
public class Test {
	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(7,6,4,8,2,11,9);
		long count = list.stream().filter(x -> x > 6).count();
	}
}
```

### 映射 (map 、 flatMap)

> 映射，可以将一个流的元素按照一定的映射规则映射到另一个流中。分为 `map` 和 `flatMap`

> 1. `map` ：接收一个函数作为参数，该函数会被应用到每个元素上，并将其映射成一个新的元素。
> 2. `flatMap` ：接收一个函数作为参数，将流中的每个值都换成另一个流，然后把所有流连接成一个流

#### 示例1

英文字符串数组的元素全部改为大写。生疏数组每个元素+3

```java
public class Test {
	public static void main(String[] args) {
		String[] strArr = {"abcd", "bcdd", "defde", "fTr"};
		List<String> strList = Arrays.stream(strArr).map(String::toUpperCase).collect(Collectors.toList());
		List<Integer> intList = Arrays.asList(1,3,5,7,9,11);
		List<Integer> intListNew = intList.stream().map(x -> x + 3).collect(Collectors.toList());
	}
}
```

#### 示例2

将两个字符数组合并成一个新的字符数组

```java
public class Test {
	public static void main(String[] args) {
		List<String> list = Arrays.asList("m,k,l,a", "1,3,5,7");
		List<String> listNew = list.stream().flatMap(s -> {
			String[] split = s.split(",");
			Stream<String> s2 = Arrays.stream(split);
			return s2;
		}).collect(Collectors.toList());
	}
}
```

### 规约 (reduce)

> 规约，也称缩减，就是把一个流缩减成一个值，能实现对集合求和、求乘机和求最值的操作 

```java
Optional<T> reduce(BinaryOperator<T> accumulator);
T reduce(T identity, BinaryOperator<T> accumulator);
<U> U reduce(U identity,BiFunction<U, ? super T, U> accumulator,BinaryOperator<U> combiner);
```

> `Optional reduce (BinaryOperator accumulator)`：第一次执行时，accumulator 函数的第一个参数为流中的第一个元素，第二个参数为流中元素的第二个元素；第二次执行时，第一个参数为第一次函数执行的结果，第二个参数为流中的第三个元素；依次类推。
> 
> `T reduce (T identity, BinaryOperator accumulator)`：流程跟上面一样，只是第一次执行时，accumulator 函数的第一个参数为 identity，而第二个参数为流中的第一个元素。

#### 示例1

求 `Integer` 集合的元素之和、乘积和最大值

```java
public class Test {
	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(1,3,2,8,11,4);
		// 求和方式1
		Optional<Integer> sum = list.stream().reduce((x, y) -> x + y);
		// 求和方式2
		Optional<Integer> sum2 = list.stream().reduce(Integer::sum);
		// 求和方式3
		Integer sum3 = list.stream().reduce(0, Integer::sum);
		// 求乘积
		Optional<Integer> product = list.stream().reduce((x, y) -> x * y);
		// 求最大值方式1
		Optional<Integer> max = list.stream().reduce((x, y) -> x > y ? x : y);
		// 求最大值方式2
		Integer max2 = list.stream().reduce(1, Integer::max);
	}
}
```

### 收集 (collect)

> 收集，就是把一个流收集起来，最终可以是收集成一个值，也可以收集成一个新的集合

> `collect` 主要依赖 `java.util.stream.Collectors` 类内置的静态方法

#### 归集 (toList , toSet , toMap)

因为流不存储数据，那么在流中的数据完成处理后，需要将流中的数据重新归集到新的集合里。

`toList`,`toSet`,`toMap`比较常用，另外也有 `toCollection` `toConcurrentMap`

##### 示例

```java
public class Test {
	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(1,6,3,4,6,7,9,6,20);
		List<Integer> listNew = list.stream().filter(x->x%2 == 0).collect(Collectors.toList());
		Set<Integer> set = list.stream().filter(x->x%2==0).collect(Collectors.toSet());

		List<User> userList = new ArrayList<User>();
		userList.add("A", 10);
		userList.add("B", 20);
		userList.add("C", 30);
		userList.add("D", 40);
		Map<?, User> map = userList.stream().filter(u -> u.getAge() > 10).collect(Collectors.toMap(User::getName, u -> u));
	}
}
```

#### 统计 (count , averaging)

`Collectors` 提供了一系列用于数据统计的静态方法：
- 计数： count 
- 平均值： averagingInt, averagingLong, averagingDouble
- 最值： maxBy, minBy
- 求和： summingInt, summingLong, summingDouble
- 统计以上所有： summarizingInt, summarizingLong, summarizingDouble

##### 示例

求用户人数、平均年龄、年龄总和、最大年龄

```java
public class Test {
	public static void main(String[] args) {
		List<User> userList = new ArrayList<User>();
		userList.add(new User("A", 10));
		userList.add(new User("B", 20));
		userList.add(new User("C", 30));

		// 求总数
		Long count = userList.stream().collect(Collectors.counting);
		// 求平均年龄
		Double average = userList.stream().collect(Collectors.averagingDouble(User::getAge));
		// 求最大年龄
		Optional<Integer> max = userList.stream().map(User::getAge).collect(Collectors.maxBy(Integer::compare));
		// 求年龄之和
		Integer sum = userList.stream().collect(Collectors.summingInt(User::getAge));
		// 一次性统计所有信息
		DoubleSummaryStatistics collect = userList.stream().collect(Collectors.summarizingDouble(User::getAge));
	}
}
```

#### 分组 (partitioningBy, groupingBy)

- 分区：将 `stream` 按条件分为两个 `Map`
- 分组：将集合分为多个 `Map`

##### 示例

将年龄以20为界限分为两组，并按照性别分组

```java
public class Test {
	public static void main(String[] args) {
		List<User> userList = new ArrayList<User>();
		userList.add(new User("A", 12, "male"));
		userList.add(new User("B", 22, "female"));
		userList.add(new User("c", 18, "female"));
		userList.add(new User("d", 28, "male"));

		Map<Boolean, List<User>> part = userList.stream().collect(Collectors.partitioningBy(x -> x.getAge() > 20));
		Map<String, List<User>> group = userList.stream().collect(Collectors.groupingBy(User::getSex));
	}
}
```

#### 接合 (joining)

`joining` 可以将 `stream` 中的元素用特定的连接符（没有的话，则直接连接）连接成一个字符串。

##### 示例

```java
public class Test {
	public static void main(String[] args) {
		List<User> userList = new ArrayList<User>();
		userList.add(new User("A", 10));
		userList.add(new User("B", 20));
		userList.add(new User("C", 30));
		userList.add(new User("D", 40));

		String names = userList.stream().map(u -> u.getName()).collect(Collectors.joining(","));
		// A,B,C,D
	}
}
```

#### 规约 (reducing)

`Collectors` 类提供的 `reducing` 方法，相比于 `stream` 本身的 `reduce` 方法，增加了对自定义规约的支持。

##### 示例

```java
public class Test {
	public static void main(String[] args) {
		List<User> userList = new ArrayList<User>();
		userList.add(new User("A", 10));
		userList.add(new User("B", 20));
		userList.add(new User("C", 30));
		userList.add(new User("D", 40));

		// 每个用户年龄-1之和
		Integer sum = userList.strea().collect(Collectors.reducing(0, User::getAge, (i, j) -> (i + j - 1)));
	}
}
```

### 排序 (sorted)

> `sorted` 中间操作，有两种排序：

1. `sorted()` : 自然排序，流中元素需实现 `Comparable` 接口
2. `sorted(Comparator com)` : Comparator 排序器自定义排序

#### 示例

将员工按工资由高到低（工资一样则按年龄由大到小）排序

```java
public class StreamTest {
	public static void main(String[] args) {
		List<Person> personList = new ArrayList<Person>();

		personList.add(new Person("Sherry", 9000, 24, "female", "New York"));
		personList.add(new Person("Tom", 8900, 22, "male", "Washington"));
		personList.add(new Person("Jack", 9000, 25, "male", "Washington"));
		personList.add(new Person("Lily", 8800, 26, "male", "New York"));
		personList.add(new Person("Alisa", 9000, 26, "female", "New York"));

		// 按工资升序排序（自然排序）
		List<String> newList = personList.stream().sorted(Comparator.comparing(Person::getSalary)).map(Person::getName)
				.collect(Collectors.toList());
		// 按工资倒序排序
		List<String> newList2 = personList.stream().sorted(Comparator.comparing(Person::getSalary).reversed())
				.map(Person::getName).collect(Collectors.toList());
		// 先按工资再按年龄升序排序
		List<String> newList3 = personList.stream()
				.sorted(Comparator.comparing(Person::getSalary).thenComparing(Person::getAge)).map(Person::getName)
				.collect(Collectors.toList());
		// 先按工资再按年龄自定义排序（降序）
		List<String> newList4 = personList.stream().sorted((p1, p2) -> {
			if (p1.getSalary() == p2.getSalary()) {
				return p2.getAge() - p1.getAge();
			} else {
				return p2.getSalary() - p1.getSalary();
			}
		}).map(Person::getName).collect(Collectors.toList());

		System.out.println("按工资升序排序：" + newList);
		System.out.println("按工资降序排序：" + newList2);
		System.out.println("先按工资再按年龄升序排序：" + newList3);
		System.out.println("先按工资再按年龄自定义降序排序：" + newList4);
	}
}
```

### 去重、合并 (distinct , skip, limit)

流也可以进行合并、去重、限制、跳过等操作

#### 示例

```java
public class StreamTest {
	public static void main(String[] args) {
		String[] arr1 = { "a", "b", "c", "d" };
		String[] arr2 = { "d", "e", "f", "g" };

		Stream<String> stream1 = Stream.of(arr1);
		Stream<String> stream2 = Stream.of(arr2);
		// concat:合并两个流 distinct：去重
		List<String> newList = Stream.concat(stream1, stream2).distinct().collect(Collectors.toList());
		// limit：限制从流中获得前n个数据
		List<Integer> collect = Stream.iterate(1, x -> x + 2).limit(10).collect(Collectors.toList());
		// skip：跳过前n个数据  这里的1代表把1代入后边的计算表达式
		List<Integer> collect2 = Stream.iterate(1, x -> x + 2).skip(1).limit(5).collect(Collectors.toList());

		System.out.println("流合并：" + newList);
		System.out.println("limit：" + collect);
		System.out.println("skip：" + collect2);
	}
}
```