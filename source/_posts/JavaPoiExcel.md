---
title: operate excel file in java by poi
date: 2022-07-27 20:51:00
tags: 
 - java
 - advance
 - excel
 - poi
categories:
 - Java
cover: https://raw.githubusercontent.com/baizeyv/PicBed/main/img/java.jpeg
feature: true
---

# Java Poi Excel 处理

## 导入 pom 依赖 (POI)

```xml
<dependency>
  <groupId>org.apache.poi</groupId>
  <artifactId>poi-ooxml-schemas</artifactId>
  <version>4.0.0</version>
</dependency>
<dependency>
  <groupId>org.apache.poi</groupId>
  <artifactId>poi-ooxml</artifactId>
  <version>4.0.0</version>
</dependency>
<dependency>
  <groupId>org.apache.poi</groupId>
  <artifactId>poi</artifactId>
  <version>4.0.0</version>
</dependency>
```

## POI 常用类

1. `Sheet` 工作表
2. `Row` 行
3. `Cell` 单元格
4. `Font` 字体
5. `CellStyle` 单元格样式
6. `CellType` 单元格数据类型

## 导出数据库内容到 Excel

### Excel 工具类

```java
public class ExcelUtils {
	/**
	 * @param list 数据库内容
	 * @param fileName 导出excel文件名称，以 .xls 或 .xlsx 结尾
	 */
	public static String exportExcel(List<? extends Object> list, String fileName, String sheetName) throws IOException, IllegalAccessException {
		/* 如果文件为空则结束，可以使用 try - catch 抛异常来处理此处 */
        if(list == null || list.size() < 1){
            return null;
        }

		/* 根据后缀类型判断 excel 版本，创建对应版本的 workbook */
        Workbook workbook;
        if(fileName.substring(fileName.lastIndexOf(".")) == "xls"){
            workbook = new HSSFWorkbook(); // version 97-2003
        } else {
            workbook = new SXSSFWorkbook(500); // version > 2003
        }

		/* 创建 sheet */
        Sheet sheet = workbook.createSheet();

		/* 设置 sheet 名称 */
        if(!sheetName.equals("") && sheetName != null){
            workbook.setSheetName(0, sheetName);
        } else{
            workbook.setSheetName(0, "sheetTest");
        }

		/* 定位、设置首行表头 */
        int rowNum = 0;
        Row row = sheet.createRow(rowNum);
        Object obj = list.get(0);
        Field[] fields = obj.getClass().getDeclaredFields(); // 根据反射，获取对应数据类的所有字段
        for(int i = 0; i < fields.length; i++){
            fields[i].setAccessible(true); // 暴力反射
            Cell cell = row.createCell(i); // 在对应行的对应位置创建单元格
            cell.setCellValue(fields[i].getName()); // 填充新建单元格内容，根据反射的来的字段设置内容
        }

		/* 遍历从数据库得出的内容 */
        for(Object o : list) {
            rowNum++;
            Field[] listField = o.getClass().getDeclaredFields();
            Row listRow = sheet.createRow(rowNum);
            int index = 0;
            for(Field field : listField) {
                field.setAccessible(true);
                if(field.get(o) == null) {
                    index ++;
                    continue;
                }
                listRow.createCell(index).setCellValue(field.get(o).toString()); // 创建单元格并填充内容
                index ++;
            }
        }

		/* 导出文件路径 */
        String filePath = "F:\\" + fileName;

		/* 创建文件 */
        File file = new File(filePath);
        FileOutputStream output = new FileOutputStream(file);

		/* 将 workbook 写入新文件 */
        workbook.write(output);
        output.close();

		/* 返回文件路径 */
        return filePath;
    }
}
```

## 将 Excel 内容导入数据库

### Excel 工具类

:::tip

- 该工具类仅将 `Excel` 数据转换为二维 `List` 后，将数据存入实体
- 如要导入数据库，需自行在 `Controller` 中调用 `Service` 层，实现对数据库的添加

:::

```java
public class ExcelUtils {
	public static void importExcel() {
		/* 设置上传的文件路径 */
        String filePath = "F:\\test.xlsx";

		/* 获取文件名称 */
        String originalFilename = filePath.substring(filePath.lastIndexOf("\\"));

		/* 获取文件后缀类型 */
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));

		/* 文件输入流 */
        FileInputStream is = null;
        try {
            is = new FileInputStream(new File(filePath));

			/* 根据文件后缀判断 excel 版本，来创建对应版本的 workbook */
            Workbook workbook;
            if(suffix == ".xls") {
                workbook = new HSSFWorkbook(is); // version 97-2003
            } else {
                workbook = new XSSFWorkbook(is); // version > 2003
            }
            if(null == workbook) {
                throw new Exception("It is NULL!");
            }

            List<List<Object>> list = new ArrayList<List<Object>>();

			/* 遍历所有 sheet */
            for(int i = 0; i < workbook.getNumberOfSheets(); i++) {
                Sheet sheet = workbook.getSheetAt(i);
                if(sheet == null){
                    continue;
                }
				/* 遍历所有 row */
                for(int j = sheet.getFirstRowNum(); j <= sheet.getLastRowNum(); j++) {
                    Row row = sheet.getRow(j);
                    if(row == null) {
                        continue;
                    }

                    List<Object> cellList = new ArrayList<>();
	                /* 遍历对应 row 中的单元格 */
                    for(int k = row.getFirstCellNum(); k < row.getLastCellNum(); k++){
                        Cell cell = row.getCell(k);
                        cellList.add(getValue(cell));
                    }
                    list.add(cellList);
                }
            }

			/* 获取表头数据，并添加至 list */
            List<Object> firstRow = null;
            if(list != null && list.size() > 0) {
                firstRow = list.get(0);
            }

			/* 填充实体数据 */
            for(int i = 1; i < list.size(); i++){
                List<Object> rowList = list.get(i);
                Demo demo = new Demo(); // 测试 Demo
                for(int j = 0; j < rowList.size(); j++) {
                    String cellVal = (String)rowList.get(j);
                    setObjectValue(demo, firstRow.get(j).toString().trim(),cellVal);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	/* 设置对象实体的值 */
    public static void setObjectValue(Object object, String fieldName, Object val) {
        try {
            Field[] fields = object.getClass().getDeclaredFields();
            for(Field field : fields){
                if(fieldName.equals(field.getName())) {
                    field.set(object, val.toString());
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	/**
	 * 通过比较单元格格式 获取单元格内容
	 */
    public static Object getValue(Cell cell) {
        Object val = "";
        if(null == cell) {
            return val;
        }

        if (cell.getCellType() == CellType.NUMERIC || cell.getCellType() == CellType.FORMULA) {
            val = cell.getNumericCellValue();
            if (DateUtil.isCellDateFormatted(cell)) {
                val = DateUtil.getJavaDate((Double) val); // POI Excel 日期格式转换
            } else {
                if ((Double) val % 1 > 0) {
                    val = new BigDecimal(val.toString());
                } else {
                    val = new DecimalFormat("0").format(val);
                }
            }
        } else if (cell.getCellType() == CellType.STRING) {
            val = cell.getStringCellValue();
        } else if (cell.getCellType() == CellType.BOOLEAN) {
            val = cell.getBooleanCellValue();
        } else if (cell.getCellType() == CellType.ERROR) {
            val = cell.getErrorCellValue();
        }
        return val;
    }
}
```

### 测试 Demo 类

```java
public class Demo {  
    public String id;  
    public String name;  
    public String createTime;  
  
    public String getId() {  
        return id;  
    }  
  
    public void setId(String id) {  
        this.id = id;  
    }  
  
    public String getName() {  
        return name;  
    }  
  
    public void setName(String name) {  
        this.name = name;  
    }  
  
    public String getCreateTime() {  
        return createTime;  
    }  
  
    public void setCreateTime(String createTime) {  
        this.createTime = createTime;  
    }  
  
    @Override  
    public String toString() {  
        return "Demo{" +  
			"id='" + id + '\'' +  
			", name='" + name + '\'' +  
			", createTime='" + createTime + '\'' +  
			'}';  
    }  
}
```