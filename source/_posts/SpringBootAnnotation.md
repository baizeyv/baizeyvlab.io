---
title: SpringBoot Annotation
date: 2022-08-22 10:11:00
tags: 
 - springboot
 - annotation
categories:
 - SpringBoot
---

# SpringBoot Annotation

## @SpringBootApplication

`@SpringBootApplication` 包括 `@EnableAutoConfiguration` 和 `@ComponentScan` 通常用在主类上

| 注解                     | 说明                                                                                                             |
| ------------------------ | ---------------------------------------------------------------------------------------------------------------- |
| @SpringBootApplication   | 用来声明当前类是一个配置类                                                                                       |
| @EnableAutoConfiguration | 是 `SpringBoot` 实现自动化配置的核心注解，通过这个注解把 `spring` 应用所需的 `bean` 注入容器                     |
| @ComponentScan           | 用来自动扫描注解标识的类，生成 `IOC` 容器里的 `bean` ，默认扫描范围是 `@CoponentScan` 注解所在配置类包及子包的类 |

> `@ComponentScan` 包扫描注解
> 
>> 系统加载的时候扫描 @Component、@Controller、@Service 等这些注解的类，并注册为 Bean 对象供调用，可以自动收集所有的 Spring件，包括 @Configuration 类。 
>> 
>> 如果没有配置的话，Spring Boot 会扫描启动类所在包下以及子包下的使用了 @Service,@Repository 等注解的类。

## @Configuration @Bean

| 注解           | 说明                                                                                      |
| -------------- | ----------------------------------------------------------------------------------------- |
| @Configuration | 作用在类上，配置 `spring` 容器（应用上下文），相当于把该类作为 `spring` 的 `xml` 配置文件 |
| @Bean          | 产生 `bean` 对象加入容器，作用于方法                                                      |

## @Service @Controller @Repository @Component

| 注解        | 说明                                                |
| ----------- | --------------------------------------------------- |
| @Service    | 用于业务层，业务逻辑层 `service` 注入 `spring` 容器 |
| @Controller | 控制层 `controller` 注入 `spring` 容器              |
| @Repository | 持久层 `dao` 注入 `spring` 容器                     |
| @Component  | 普通 `domain` 注入 `spring` 容器                    |


## @ResponseBody

`@ResponseBody` 作用在方法或类上，让该方法的返回结果直接写入 `HTTP response body` 中， 不会经过视图解析器，返回数据直接在页面展示

## @RestController

`@RestController` 是 `@Controller` 和 `@ResponseBody` 的结合体，作用于类，作用等于在类上添加了 `@ResponseBody` 和 `@Controller`

> `@RestController` 的返回值都是经过转换的 `Json`
> 
> `@RestController` 等价于 `@Controller` 加 `@ResponseBody`

## @AutoWired

`@Autowired` 默认按类型装配，常用于业务层实现类和持久层实现类

## @RequestMapping @GetMapping @PostMapping

| 注解            | 说明                                                                  |
| --------------- | --------------------------------------------------------------------- |
| @RequestMapping | 使用该注解可以通过配置的 `url` 进行访问，可以是 `GET` 也可以是 `POST` |
| @GetMapping     | 使用该注解可以通过配置的 `url` 进行访问，限定 `GET` 请求方式          |
| @PostMapping    | 使用该注解可以通过配置的 `url` 进行访问，限定 `POST` 请求方式         |

> `@RequestMapping(value="xxx", method=RequestMethod.POST)` 等价于 `@PostMapping("xxx")`
>
> `@RequestMapping(value="xxx", method=RequestMethod.GET)` 等价于 `@GetMapping("xxx")`

## @RequestParam @RequestBody

| 注解          | 说明                                                                                                    |
| ------------- | ------------------------------------------------------------------------------------------------------- |
| @RequestParam | 主要用于接收 `url?` 后面的参数，包括 `GET` 和 `POST` 请求，只要 `url?` 后有参数，都可以获取到对应的参数 |
| @RequestBody  | 用于获取 `POST` 请求的请求体                                                                            |

## @Qualifier

`@Qualifier` 注解，需要和 `@Autowired` 注解配合使用

> 当有多个同一类型的 Bean 时，可以用 @Qualifier (“name”) 来指定。与 @Autowired 配合使用。
> 
> @Qualifier 限定描述符除了能根据名字进行注入，也能进行更细粒度的控制如何选择候选者

## @TableId

使用 `Mybatis-Plus` 框架时，实现 `id` 的自增，需要在实体类的 `id` 属性上添加 `@TableId(type = IdType.AUTO)` 注解

## @PreAuthorize @PostAuthorize @PreFilter @PostFilter

`Spring Security` 中定义了四个支持使用表达式的注解

| 注解           | 说明                     |
| -------------- | ------------------------ |
| @PreAuthorize  | 在方法调用前进行权限检查 |
| @PostAuthorize | 在方法调用后进行权限检查 |
| @PreFilter     | 对集合类型的参数进行过滤 |
| @PostFilter               | 对返回值进行过滤                         |

要使这些定义能够对我们的方法的调用产生影响，我们需要设置 `global-method-security` 元素的 `pre-post-annotations="enabled"` 默认为 `disabled`

`<global-method-securitypre-post-annotations="enabled"/>`

### 具体使用

- `@PreAuthorize("#id<10")` 限制只能操作 `id` 小于 10 的数据
- `@PostAuthorize("return Object.id%2==0")` 限制返回的数据 `id` 只能是偶数才能通过，对返回数据做检验处理