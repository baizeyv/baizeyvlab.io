---
title: Java实现二叉树
date: 2022-08-22 20:01:00
tags: 
 - java
 - ds
categories:
 - Java
 - DS
---

# Java实现二叉树及四种遍历

```java
package tree;  
  
public class BinaryTreeNode<T> {  
  
    private T data;  
  
    private BinaryTreeNode<T> leftChild;  
  
    private BinaryTreeNode<T> rightChild;  
  
    public T getData() {  
        return data;  
    }  
  
    public void setData(T data) {  
        this.data = data;  
    }  
  
    public BinaryTreeNode<T> getLeftChild() {  
        return leftChild;  
    }  
  
    public void setLeftChild(BinaryTreeNode<T> leftChild) {  
        this.leftChild = leftChild;  
    }  
  
    public BinaryTreeNode<T> getRightChild() {  
        return rightChild;  
    }  
  
    public void setRightChild(BinaryTreeNode<T> rightChild) {  
        this.rightChild = rightChild;  
    }  
}
```

```java
package tree;  
  
import java.util.LinkedList;  
  
public class MyBinaryTree<T> {  
  
    private BinaryTreeNode<T> root;  
  
    /**  
     * init current binary tree     * @param root  
     */  
    public MyBinaryTree(BinaryTreeNode<T> root) {  
        this.root = root;  
    }  
  
    public BinaryTreeNode<T> getRoot() {  
        return root;  
    }  
  
    public void setRoot(BinaryTreeNode<T> root) {  
        this.root = root;  
    }  
  
    public boolean isEmpty() {  
        return  root == null;  
    }  
  
    public void insertLeft(BinaryTreeNode<T> root, BinaryTreeNode<T> node) {  
        root.setLeftChild(node);  
    }  
  
    public void insertRight(BinaryTreeNode<T> root, BinaryTreeNode<T> node) {  
        root.setRightChild(node);  
    }  
  
    /**  
     * 先序遍历 (递归)  
     * @param root  
     */  
    public void preOrder(BinaryTreeNode<T> root) {  
        if(root == null) {  
            return;  
        }  
        System.out.print("| " + root.getData() + " |");  
        preOrder(root.getLeftChild());  
        preOrder(root.getRightChild());  
    }  
  
    /**  
     * 中序遍历 (递归)  
     * @param root  
     */  
    public void inOrder(BinaryTreeNode<T> root){  
        if(root == null) {  
            return;  
        }  
        inOrder(root.getLeftChild());  
        System.out.print("| " + root.getData() + " |");  
        inOrder(root.getRightChild());  
    }  
  
    /**  
     * 后根遍历 (递归)  
     * @param root  
     */  
    public void postOrder(BinaryTreeNode<T> root) {  
        if(root == null) {  
            return;  
        }  
        postOrder(root.getLeftChild());  
        preOrder(root.getRightChild());  
        System.out.print("| " + root.getData() + " |");  
    }  
  
    /**  
     * 层次遍历  
     * @param root  
     */  
    public void levelOrder(BinaryTreeNode<T> root) {  
        LinkedList<BinaryTreeNode<T>> queue = new LinkedList<>();  
        queue.add(root);  
        while (!queue.isEmpty()){  
            root = queue.pop();  
            if(root.getLeftChild() != null){  
                queue.add(root.getLeftChild());  
            }  
            if(root.getRightChild() != null){  
                queue.add(root.getRightChild());  
            }  
            System.out.print("| " + root.getData() + " |");  
        }  
    }  
  
  
}
```

```java
package tree;  
  
public class Main {  
  
    public static void main(String[] args) {  
        BinaryTreeNode<String> nodeA = new BinaryTreeNode<>();  
        nodeA.setData("A");  
  
        BinaryTreeNode<String> nodeB = new BinaryTreeNode<>();  
        nodeB.setData("B");  
        nodeA.setLeftChild(nodeB);  
  
        BinaryTreeNode<String> nodeC = new BinaryTreeNode<>();  
        nodeC.setData("C");  
        nodeA.setRightChild(nodeC);  
  
        BinaryTreeNode<String> nodeD = new BinaryTreeNode<>();  
        nodeD.setData("D");  
        nodeB.setLeftChild(nodeD);  
  
        BinaryTreeNode<String> nodeE = new BinaryTreeNode<>();  
        nodeE.setData("E");  
        nodeB.setRightChild(nodeE);  
  
        BinaryTreeNode<String> nodeF = new BinaryTreeNode<>();  
        nodeF.setData("F");  
        nodeC.setLeftChild(nodeF);  
  
        BinaryTreeNode<String> nodeG = new BinaryTreeNode<>();  
        nodeG.setData("G");  
        nodeC.setRightChild(nodeG);  
  
        MyBinaryTree<String> tree = new MyBinaryTree<>(nodeA);  
  
        System.out.print("先序遍历的结果是: ");  
        tree.preOrder(nodeA);  
  
        System.out.print("\n"+"中序遍历的结果是: ");  
        tree.inOrder(nodeA);  
  
        System.out.print("\n"+"后序遍历的结果是: ");  
        tree.postOrder(nodeA);  
  
        System.out.print("\n" + "层次遍历的结果是: ");  
        tree.levelOrder(nodeA);  
    }  
  
}
```

输出：
![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220822201528.png)
