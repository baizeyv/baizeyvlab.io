---
title: mybatis dynamic sql
date: 2022-07-26 20:17:00
tags: 
- mybatis 
- basic
categories:
- [Mybatis, Basic]
---

# Mybatis 动态 SQL

## 动态 SQL 简述

:::tip

- 动态 `SQL` 即 在不同条件下拼接不同的 `sql` 语句

:::

`Mybatis` 框架的动态 `sql` 技术时一种根据特定条件动态拼接 `sql` 语句的功能

它存在的 *意义* 是为了解决拼接 `sql` 语句字符串时的痛点问题。

> 例如：
> 
>> 使用购物软件选择商品属性的时候，可以选择很多条件，且某些条件也可以不选择
>> 
>> 如果我们使用传统的方式进行查询，反而在拼接 `sql` 的时候会造成一系列问题

## 使用案例

现有用户表 `user`

|Field|Type|PRI|isNull|Ex|Comment|
|---|---|---|---|---|---|
|id|int|yes|no|auto_increase|ID|
|username|varchar(20)|no|no||User Name|
|sex|varchar(10)|no|no||Sex|


### \<if\> 标签

首先使用传统方式，根据 `username` 和 `sex` 查询数据。

如果 `username` 为空，那么将只根据 `sex` 来查询；反之只根据 `username` 查询

```xml
<select id="selectUserByUsernameAndSex" parameterType="com.melody.entity.User" resultType="com.melody.entity.User">
	select * from user where username = #{username} and sex = #{sex}
</select>
```

:::warning
以上查询语句，如果 `#{username}` 为空，那么查询结果为空
:::

解决方法如下：

> 使用动态 `sql`

```xml
<select id="selectUserByUsernameAndSex" parameterType="com.melody.entity.User" resultType="com.melody.entity.User">
	select * from user where
	<if test="username != null and username != ''">
		username = #{username}
	</if>
	<if test="sex != null and sex != ''">
		and sex = #{sex}
	</if>
</select>
```

:::tip
- 从以上案例可以得出：
- 如果 `sex` 为 `null` ，则查询语句为 `select * from user where username = #{username}`
- 如果 `username` 为 `null` ，则查询语句为 `select * from user where and sex = #{sex}` ***这显然是错误的***
:::

如何消除多出来的 `and` 呢？故引出以下的 `where - if` 标签

### \<where\> - \<if\> 标签

```xml
<select id="selectUserByUsernameAndSex" paramterType="com.melody.entity.User" resultType="com.melody.entity.User">
	select * from user
	<where>
		<if test="username != null">
			username = #{username}
		</if>
		<if test="sex != null">
			and sex = #{sex}
		</if>
	</where>
</select>
```

> 在 `<where> - <if>` 中，若条件语句是以 `AND` 或者 `OR` 开头的，那么 `Mybatis` 会自动去除 `AND` 和 `OR`

### \<set\> \<if\> 标签 拼装 update

```xml
<update id="updateInfo" parameterType="com.melody.entity.User">
	update user
	<set>
		<if test="username != null and username != ''">
			username = #{username},
		</if>
		<if test="sex != null and sex != ''">
			sex = #{sex}
		</if>
	</set>
	<where>
		id=#{id}
	</where>
</update>
```

> 在 `<set>` 标签中，会去掉末尾多余的 `,`


### \<choose\> - \<when\> - \<otherwise\> 标签

> `<choose> - <when> - <otherwise>` 类似于 `if - elseif - else`

> `<choose>` 标签是 `<when>` 和 `<otherwise>` 的父标签
> 
> `<when>` 相当于 `if - elseif`
> 
> `<otherwise>` 相当于是 `eles`

```xml
<select id="selectUser" parameterType="com.melody.entity.User" resultType="com.melody.entity.User">
	select * from user
	<where>
		<choose>
			<when test="id != null and id != ''">
				id = #{id}
			</when>
			<when test="username != null and username != ''">
				username = #{username}
			</when>
			<otherwise>
				sex = #{sex}
			</otherwise>
		</choose>
	</where>
</select>
```

### \<trim\> 标签

*`<trim>` 是一个格式化标记，可以完成 `<set>` 或者 `<where>` 标签的功能*

#### 改写 where - if

```xml
<select id="selectUserByUsernameAndSex" parameterType="com.melody.entity.User" resultType="com.melody.entity.User">
	select * from User
	<trim prefix="where" prefixOverrides="and | or">
		<if test="username != null">
			username = #{username}
		</if>
		<if test="sex != null">
			and sex = #{sex}
		</if>
	</trim>
</select>
```

> 其中 `prefix` 代表 **前缀**
> 
> `prefixOverrides` 代表 **去除第一个and或or**

#### 改写 set - if

```xml
<update id="updateInfo" parameterType="com.melody.entity.User">
	update user
	<trim prefix="set" suffixOverrides=",">
		<if test="username != null and username != ''">
			username = #{username}
		</if>
		<if test="sex != null and sex != ''">
			sex = #{sex}
		</if>
	</trim>
	<where>
		id = #{id}
	</where>
</update>
```

### \<foreach\> 标签

> `<foreach>` 标签用于处理传入的参数为数组形式的数据，例如：批量插入、批量更新、批量删除等操作

#### 批量删除

```xml
<delete id="batchDelete">
	delete from User where id in
	<foreach collection="ids" item="id" separator="," open="(" close=")">
		#{id}
	</foreach>
</delete>
```

> `collection` 代表 **当前要循环的数组或者集合**
> `item` 代表 **指定循环的数组或集合的每一个元素**
> `separator` 代表 **各个元素之间的分隔符号**
> `open` 代表 **当前循环开始的前缀**
> `close` 代表 **当前循环结束的后缀**


#### 批量插入

```xml
<insert id="batchInsert">
	insert into User(id, username, sex) values
	<foreach collection="users" item="usr" separator=",">
		(null, #{usr.username}, #{usr.sex})
	</foreach>
</insert>
```