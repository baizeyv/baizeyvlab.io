---
title: Git 基础
date: 2022-08-22 23:01:00
tags: 
 - git
categories:
 - Git
---

# Git基础

## Git 安装

1. 对于和我一样使用 `Arch Linux` 的朋友：

`sudo pacman -S git` Or `yay -S git`

2. 使用 `Windows` 系列的朋友：

`choco install git`

或移步 [Git官网](https://git-scm.com/)进行下载

## Git 配置

在正式使用 `git` 之前，我们需要做一些配置，**配置的主要命令为 `git config` **

如下：

`git config --global user.name "Your name"`

`git config --global user.email "Your Email"`

> *Example:*
> 
>> `git config --global user.name "baizeyv"`
>> 
>> `git config --global user.email "baizeyv@Gmail.com"`

> `name` 以及 `email` 需要全局设置，而 `--global` 正是代表全局， 如果不使用 `--global` 则仅设置当前仓库的 `name` 以及 `email`
>
>> 查看所有已做配置：`git config -l`

## Git 连接 Github/Gitlab/Gitee

> 国内用户请首选 `Gitee` ，除非您正在科学上网
>
> 本文以 `Github` 为例

在使用 `SSH` 协议访问远程仓库之前需要先配置好账户的SSH公钥

### 配置公钥

使用以下命令生成 `sshkey` :

`ssh-keygen -t rsa`

> 此过程中一路 `Enter` 即可

`Linux` 用户可在 `~/.ssh` 目录下找到

复制生成后的 `ssh key`， 通过 `Settings` -> `SSH and GPG keys`

点击 `New SSH key` ， 粘贴，确定即可。

添加后，在 Terminal 中输入：

`ssh -T git@github.com`

首次使用需要确认并添加主机到本机SSH可信列表。

若返回 `Hi YOUR_NAME!You've successfully authenticated, but Github.com does not provide shell access.` ，则证明添加成功

添加成功后，就可以使用SSH协议对仓库进行操作了

## Git 常用命令

### 基础操作

#### 创建版本库

`git init`

#### 将文件添加到版本库

- 添加到暂存区 `git add`
- 提交到仓库 `git commit`

#### 查看仓库状态

`git status`

#### 查看仓库中的具体修改

`git diff`

#### 查看提交历史记录

`git log`

- 显示单行 `git log --pretty=oneline`

#### 查看命令历史

`git reflog`

#### 版本回退

`git reset --hard`

- 回退到上个版本 `git reset --hard HEAD^`
- 回退到上上个版本 `git reset --hard HEAD^^`
- 回退到具体某个提交 `git reset --hard commit_id`

#### 撤销修改

- 丢弃工作区的修改 `git checkout --file`
- 丢弃已经进入暂存区的修改 `git reset HEAD file`

#### 删除文件

`git rm file`

### 分支管理

#### 查看分支

- `git branch`
- `git branch -v`
- `git branch -av`

#### 创建分支

`git branch branch_name`

#### 切换分支

`git checkout branch_name`

#### 创建新分支并切换

`git checkout -b branch_name`

#### 合并分支

`git merge`

- 禁用fast forward(快进)  `git merge --no-ff`

#### 删除分支

`git branch -d branch_name`

### 远程仓库

#### 添加远程仓库

`git remote add`

#### 推送本地内容到远程仓库

`git push`

#### 从远程仓库克隆到本地

`git clone`

#### 修改本地指向的远程仓库地址

`git remote set-url`

#### 获取远程数据的变更

- `git fetch`
- `git pull`

#### 查看远程仓库信息

`git remote [-v]`

#### 建立本地分支和远程分支的关联

`git branch --set-upstream <local_name> origin/<remote_name>`

#### 删除远程分支

`git push origin --delete branch_name`

### 标签管理

#### 创建标签

`git tag -a <tag_name> -m <commit_info> <comment_id>`

#### 查看标签

- 所有标签 `git tag`
- 指定标签 `git show <tagname>`

#### 删除本地标签

`git tag -d <tag_name>`

#### 删除远程标签

- `git push origin :refs/tags/<tag_name>`
- `git push origgin --delete <tag_name>`
- `git push origin :<tag_name>`

#### 推送标签到远程

- 单个标签 `git push origin <tag_name>`
- 所有未推送标签 `git push origin --tags`

---

# Git 进阶

## git merge 和 git rebase 比较

> `git rebase` 让我们的提交记录更加清晰可读

### git rebase 的使用

`rebase` 意思为 *变基*，其作用和 `merge` 类似，用于把一个分支的修改合并到当前分支上。

如下图所示，下图介绍了经过 `rebase` 前后提交历史的变化情况

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220822230000.png)

#### 示例解释

假设我们现在的仓库中有两条分支

- `master`
- `feature`

这两个分支都是基于初始的一个提交 `add readme` 进行检出分支

之后， `master` 分支增加了 `3.js` 和 `4.js` 两个文件，分别进行了两次提交

`feature` 分支增加了 `1.js` 和 `2.js` 文件，分别对应以下两条提交记录。

> `master` 分支：
> 
> ![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220822230235.png)

> `feature` 分支：
> 
> ![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220822230253.png)

结合起来看如下图：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220822230332.png)

此时，切换到 `feature` 分支，执行 `git rebase master` ，成功之后通过 `git log` 查看记录

如下图所示：可以看到先是逐个应用了 `master` 分支的更改，然后以 `master` 分支最后的提交作为基点，再逐个应用 `feature` 分支的每个更改

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220822230532.png)

如此，提交记录非常清晰，没有分叉，上面是比较顺利的情况，但大部分情况下， `rebase` 的过程中会产生冲突的，此时，就需要手动解决冲突

然后使用 `git add` , `git rebase --continue` 的方式来处理冲突，完成 `rebase`

如果不想要某次 `rebase` 的结果，那么需要使用 `git rebase --skip` 来跳过这次 `rebase`

### git merge 与 git base 的区别

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220822230903.png)

> 不同于 `git rebase` 的是， `git merge` 在不是 `fast-forward` (快速合并) 的情况下，
>
> 会产生一条额外的合并记录，类似 `Merge branch 'xxx' into 'xxx'` 的一条提交记录。

> 另外，在解决冲突的时候，用 `merge` 只需要解决一次冲突即可，简单粗暴，
>
> 而用 `rebase` 的时候，需要一次又一次的解决冲突。


## git rebase 交互模式

在开发中，经常会遇到在一个分支上产生了很多的无效的提交，这种情况下使用 `rebase` 的交互式模式可以把已经发生的多次提交压缩成一次提交，得到了一个干净的提交历史，例如某个分支的提交历史情况如下：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220822231505.png)

进入交互式模式的方式命令为：

```bash
git rebase -i <base_commit>
```

参数 `base_commit` 就是指明操作的基点提交对象，基于这个基点进行 `rebase` 的操作，对于上述提交历史的例子，我们就需要把最后一个提交对象（`ac18084`） 之后的提交压缩成一次提交，我们需要执行的命令格式为：

```bash
git rebase -i ac18084
```

此时会进入 `vim` 交互式页面，编辑器列出的信息像下列这样

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220822231832.png)

想要合并这一些更改，我们要使用 `squash` 策略进行合并，即把当前的 `commit` 和它的上一个 `commit` 内容进行合并

大概如下样式：

```txt
pick ... ...
s    ... ...
s    ... ...
s    ... ...
```

> 可根据上图中的注释，根据情况进行编辑

修改文件后 `:wq` 保存退出，此时又会弹出一个编辑页面，这个页面用来编辑提交信息，修改为 `feat:更正`

接着使用 `git branch` 查看提交的 `commit` 信息

`rebase` 后的提交记录如下图所示：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220822232129.png)