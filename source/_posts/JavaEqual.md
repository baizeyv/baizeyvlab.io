---
title: Java Equal
date: 2022-08-30 21:01:00
tags: 
 - java
 - basic 
categories:
 - Java
---

# Java == 和 equals

## 联系

在 `Java` 中 `equals` 和 `==` 都是用来判断变量所代表的对象是否相同、代表的值是否相等的

## 区别

1. `equals` 是方法，而 `==` 是操作符
2. `==` 一般是 *地址* 的绑定
3. `equals` 一般是 *内容(值)* 的绑定；(前提是没有重写 `equals` 方法)
4. 对于基本类型的变量来说(如 `short`, `int`, `long`, `float`, `double`)，**只能** 使用 `==`
	
	因为这些基本类型的变量没有 `equals` 方法。
	
	对于基本类型变量的比较，使用 `==` 比较，***一般比较的是它们的值***
5. 对于引用类型的变量来说(如 `String` 类)，因为 `String` 继承自 `Object` ，而 `equals` 方法来源于 `Object` 类，`equals` 是 `Object` 类的通用方法

	对于引用类型的比较，默认情况下就是使用的 `Object` 原生的(未被重写的) `equals` 方法，使用 `==` 和 `equals` 比较的效果是相同的；

	对于 `equals` 方法被重写的类(如 `String` 类)来说，使用 `equals` 方法会 ***比较它们的值***

### 分析

- 对于 `equals` 方法没有被重写的情况

	> 如果类没有重写该 `equals` 方法，那么默认使用的就是 `Object` 类中的 `equals` 方法，如下为 `Object` 中的 `equals` 方法：
	> ```java
	> public boolean equals(Object obj) {
	> 	return (this == obj);
	> }
	> ```
	> 从源码可以看出，`Object` 的原生 `equals` 方法就是使用的 `==` 进行比较的，所以在这种情况下比较的就是二者在内存中存放的地址
	
- 对于 `equals` 方法被重写的情况

	> 以 `String` 类为例，以下为 `String` 类中的 `equals` 方法：
	> ```java
	> @Override public boolean equals(Object other) {
	> 	if(other == this) {
	> 		return true;
	> 	}
	> 	if(other instanceof String) {
	> 		String s = (String)other;
	> 		int count = this.count;
	> 		if(s.count != count) {
	> 			return false;
	> 		}
	> 		if(hashCode() != s.hashCode()) {
	> 			return false;
	> 		}
	> 		char[] value1 = value;
	> 		int offset1 = offset;
	> 		char[] values2 = s.value;
	> 		int offset2 = s.offset;
	> 		for(int end = offset1 + count; offset1 < end; ) {
	> 			if(value1[offset1] != values2[offset2]) {
	> 				return false;
	> 			}
	> 			offset1++;
	> 			offset2++;
	> 		}
	> 		return true;
	> 	} else {
	> 		return false;
	> 	}
	> }
	> ```
	> 从源码可以看出 `String` 类重新的 `equals` 方法，当使用 `==` 比较的内存地址不相等时，会比较字符串的内容是否相等
	> 
	> 所以 `String` 类的 `equals` 方法会比较二者的字符串内容是否相等	
	
### 示例

```java
public class Test {
	public static void main(String[] args) {
		String str1 = "melody";
		String str2 = new String("melody");
		String str3 = str2;
		System.out.println("str1 == str2 ? " + str1 == str2); // false
		System.out.println("str2 == str3 ? " + str2 == str3); // true
		System.out.println("str1 == str3 ? " + str1 == str3); // false
		System.out.println("str1 equals str2 ? " + str1.equals(str2)); // true
		System.out.println("str2 equals str3 ? " + str2.equals(str3)); // true
		System.out.println("str1 equals str3 ? " + str1.equals(str3)); // true
	}
}
```

输出结果如下：

![](https://raw.githubusercontent.com/baizeyv/PicBed/master/20220830213635.png)

> 因为 `str2` 通过 `new` 的方式在堆中开辟了新的内存空间，而 `str1` 的赋值方式使得 `"melody"` 字符串是存放在常量池中的
> 
> 二者的内存地址不同，故而 `str1 == str2` 为 `false`；`str1.equals(str2)` 为 `true`
> 
> 而 `equals` 在内存地址不同的时候，会比较两个字符串的内容是否相等，故得出此结果

## 重写 equals() 定要重写 hashCode()

因为 `Hash` 比 `equals` 方法的开销小，速度更快，所以在涉及到 `hashCode` 的容器中（如 `HashSet` 等），

判断自己是否持有该对象的时候，会先检查 `hashCode` 是否相等，

如果 `hashCode` 不相等，就会直接认为未持有该对象，并放入容器，不会再调用 `equals` 进行比较

> 这样会导致，即使该对象已经存在 `HashSet` 中，但是因为 `hashCode` 不同，还会再次存入容器

因此 ***如果equals判断是相等的，那么hashCode的值也要相等***

### hashCode

`hashCode` 即散列码。散列码是用一个 `int` 值来代表对象，它是通过将该对象的某些信息进行转换而生成的。

`Object` 类中默认的 `hashCode()` 方法如下：

```java
public native int hashCode();
```

这是一个本地方法，不同的虚拟机有不同的实现。

`Object` 默认的 `hashCode` 是根据对象的内存地址转换来的，是唯一的

***hashCode()方法主要是为了给如HashMap、HashSet这样的哈希表使用的***

> 设计 `hashCode` 最重要的因素是：
> 
>>  对同一个对象调用 `hashCode()` 应该产生同样的值(前提是对象的信息没有被改变)
>
> 设计一个 `HashCode` ，它必须快，而且具有意义(使用有意义的字段来生成 `hashCode`)
> 
> `hashCode` 不需要唯一（默认的 `hashCode` 唯一），因此更应该关注它的速度，而不是唯一性

### equals

`hashCode` 并不需要唯一性，但 `equals` 必须严格判断两个对象是否相同

正确的 `equals` 方法有如下特性：

- 自反性：`x.equals(x)` 一定返回 `true`
- 对称性：若 `x.equals(y)` 为 `true` ，那么 `y.equals(x)` 也为 `true`
- 传递性：若 `x.equals(y)` 为 `true`，且 `y.equals(z)` 为 `true`，那么 `x.equals(z)` 一定为 `true`
- 一致性：若 `x` 和 `y` 中用于等价比较的信息没有改变，那么 `x.equals(y)` 无论调用多少次，结果都一致
- 任何不是 `null` 的 x， `x.equals(null)` 一定返回 `false`

### equals 与 hashCode 相关规定

1. `equals` 相等，`hashCode` 一定相等
2. `equals` 不等，`hashCode` 不一定不等
3. `hashCode` 相等，`equals` 不一定相等
4. `hashCode` 不等，`equals` 一定不等

因此，如果重写了 `equals` 方法，那么必须要重写 `hashCode` 方法，使其满足以上条件。

#### 分析

因为 `HashMap`、 `HashSet` 等哈希表中是使用 `equals` 判断是否相等的。

但是在进行 `equals` 比较之前，会先比较 `hashCode` ，如果 `hashCode` 不等了，那么直接返回不相等

此时为了保证结果的一致性，`equals` 必须保证返回的是不相等

故 ***hashCode不等，equals一定不等***

### HashSet 示例代码分析

```java
public class Test {
	public static void main(String[] args) {
		Melody m1 = new Melody(1, "melody");
		Melody m2 = new Melody(1, "melody");

		HashSet<Melody> melodySet = new HashSet<Melody>();
		melodySet.add(m1);
		melodySet.add(m2);
		// equals 判断是否相等
		System.out.println(m1.equals(m2));
		// 查看HashSet中元素个数
		System.out.println(melodySet.size());
	}
}
```

只重写 `equals` 方法的情况

```java
public class Melody {
	private Integer id;
	private String name;
	public Melody(Integer id, String name) {
		this.id = id;
		this.name = name;
	}
	/* 此处省略 getter 以及 setter */

	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		if(obj == null || getClass() != obj.getClass()) {
			return false;
		}
		Melody melody = (Melody) obj;
		return Objects.equals(id, melody.id);
	}
}
```

此时输出结果如下：

> ```bash
> true
> 2
> ```

由元素个数为2可以看出，Set把 `equals` 比较得相等的对象给放入了容器中

加上重写 `hashCode()` 方法：

```java
@Override
public int hashCode() {
	return Objects.hash(id);
}
```

此时的输出结果如下：

> ```bash
> true
> 1
> ```

此时容器中只有1个元素，则得出 `HashSet` 是通过先判断 `hashCode` 是否相等，再进而通过 `equals` 判断相等的。

若 `hashCode` 不同，则 `HashSet` 会视为不同的对象，以此放入容器

故又回到上文，得出 ***hashCode不等，equals一定不等*** 的规定。